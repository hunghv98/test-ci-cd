# coding: utf-8

"""
    R&S Web Application Firewall / iSuite configuration API

    The Rohde & Schwarz's WAF management API provides a REST/JSON programming interface. It allows automation and scripting of WAF administration tasks, such as management of reverse proxies and tunnels. The API documentation is shipped with the API service itself. Once installed on your management box, you can access the documentation with the following URL (where \"your-waf\" is the IP address or hostname of your management box):<br/><a class=\"custom-link\" href=\"https://your-waf:3001/wafapi/doc\">https://your-waf:3001/wafapi/doc</a>  # noqa: E501

    The version of the OpenAPI document: 1.0.9
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six

from r_s_waf_api_client.configuration import Configuration


class GetAlertingdestinationsDataSuccessArray(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'uid': 'str',
        'name': 'str',
        'type': 'str',
        'smtp_config': 'CreateAlertingDestinationsSuccessDataSMTPConfig',
        'syslog_config': 'CreateAlertingDestinationsSuccessDataSyslogConfig',
        'snmp_config': 'CreateAlertingDestinationsSuccessDataSNMPConfig',
        't_update': 'float',
        't_create': 'float'
    }

    attribute_map = {
        'uid': 'uid',
        'name': 'name',
        'type': 'type',
        'smtp_config': 'SMTPConfig',
        'syslog_config': 'syslogConfig',
        'snmp_config': 'SNMPConfig',
        't_update': 'tUpdate',
        't_create': 'tCreate'
    }

    def __init__(self, uid=None, name=None, type=None, smtp_config=None, syslog_config=None, snmp_config=None, t_update=None, t_create=None, local_vars_configuration=None):  # noqa: E501
        """GetAlertingdestinationsDataSuccessArray - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._uid = None
        self._name = None
        self._type = None
        self._smtp_config = None
        self._syslog_config = None
        self._snmp_config = None
        self._t_update = None
        self._t_create = None
        self.discriminator = None

        if uid is not None:
            self.uid = uid
        if name is not None:
            self.name = name
        if type is not None:
            self.type = type
        if smtp_config is not None:
            self.smtp_config = smtp_config
        if syslog_config is not None:
            self.syslog_config = syslog_config
        if snmp_config is not None:
            self.snmp_config = snmp_config
        if t_update is not None:
            self.t_update = t_update
        if t_create is not None:
            self.t_create = t_create

    @property
    def uid(self):
        """Gets the uid of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501


        :return: The uid of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501
        :rtype: str
        """
        return self._uid

    @uid.setter
    def uid(self, uid):
        """Sets the uid of this GetAlertingdestinationsDataSuccessArray.


        :param uid: The uid of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501
        :type: str
        """

        self._uid = uid

    @property
    def name(self):
        """Gets the name of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501


        :return: The name of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this GetAlertingdestinationsDataSuccessArray.


        :param name: The name of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def type(self):
        """Gets the type of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501


        :return: The type of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this GetAlertingdestinationsDataSuccessArray.


        :param type: The type of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501
        :type: str
        """

        self._type = type

    @property
    def smtp_config(self):
        """Gets the smtp_config of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501


        :return: The smtp_config of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501
        :rtype: CreateAlertingDestinationsSuccessDataSMTPConfig
        """
        return self._smtp_config

    @smtp_config.setter
    def smtp_config(self, smtp_config):
        """Sets the smtp_config of this GetAlertingdestinationsDataSuccessArray.


        :param smtp_config: The smtp_config of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501
        :type: CreateAlertingDestinationsSuccessDataSMTPConfig
        """

        self._smtp_config = smtp_config

    @property
    def syslog_config(self):
        """Gets the syslog_config of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501


        :return: The syslog_config of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501
        :rtype: CreateAlertingDestinationsSuccessDataSyslogConfig
        """
        return self._syslog_config

    @syslog_config.setter
    def syslog_config(self, syslog_config):
        """Sets the syslog_config of this GetAlertingdestinationsDataSuccessArray.


        :param syslog_config: The syslog_config of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501
        :type: CreateAlertingDestinationsSuccessDataSyslogConfig
        """

        self._syslog_config = syslog_config

    @property
    def snmp_config(self):
        """Gets the snmp_config of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501


        :return: The snmp_config of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501
        :rtype: CreateAlertingDestinationsSuccessDataSNMPConfig
        """
        return self._snmp_config

    @snmp_config.setter
    def snmp_config(self, snmp_config):
        """Sets the snmp_config of this GetAlertingdestinationsDataSuccessArray.


        :param snmp_config: The snmp_config of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501
        :type: CreateAlertingDestinationsSuccessDataSNMPConfig
        """

        self._snmp_config = snmp_config

    @property
    def t_update(self):
        """Gets the t_update of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501


        :return: The t_update of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501
        :rtype: float
        """
        return self._t_update

    @t_update.setter
    def t_update(self, t_update):
        """Sets the t_update of this GetAlertingdestinationsDataSuccessArray.


        :param t_update: The t_update of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501
        :type: float
        """

        self._t_update = t_update

    @property
    def t_create(self):
        """Gets the t_create of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501


        :return: The t_create of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501
        :rtype: float
        """
        return self._t_create

    @t_create.setter
    def t_create(self, t_create):
        """Sets the t_create of this GetAlertingdestinationsDataSuccessArray.


        :param t_create: The t_create of this GetAlertingdestinationsDataSuccessArray.  # noqa: E501
        :type: float
        """

        self._t_create = t_create

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, GetAlertingdestinationsDataSuccessArray):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, GetAlertingdestinationsDataSuccessArray):
            return True

        return self.to_dict() != other.to_dict()
