# coding: utf-8

"""
    R&S Web Application Firewall / iSuite configuration API

    The Rohde & Schwarz's WAF management API provides a REST/JSON programming interface. It allows automation and scripting of WAF administration tasks, such as management of reverse proxies and tunnels. The API documentation is shipped with the API service itself. Once installed on your management box, you can access the documentation with the following URL (where \"your-waf\" is the IP address or hostname of your management box):<br/><a class=\"custom-link\" href=\"https://your-waf:3001/wafapi/doc\">https://your-waf:3001/wafapi/doc</a>  # noqa: E501

    The version of the OpenAPI document: 1.0.9
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six

from r_s_waf_api_client.configuration import Configuration


class UpdateCompressionProfileSuccessData(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'uid': 'str',
        'name': 'str',
        'content_types': 'str',
        'exclude_browser': 'str',
        'buffer_size': 'int',
        'compression_level': 'int',
        'memory_level': 'int',
        'window_size': 'int',
        'used_by': 'list[UpdateCompressionProfileUsedBySuccessArray]',
        't_update': 'float',
        't_create': 'float'
    }

    attribute_map = {
        'uid': 'uid',
        'name': 'name',
        'content_types': 'contentTypes',
        'exclude_browser': 'excludeBrowser',
        'buffer_size': 'bufferSize',
        'compression_level': 'compressionLevel',
        'memory_level': 'memoryLevel',
        'window_size': 'windowSize',
        'used_by': 'usedBy',
        't_update': 'tUpdate',
        't_create': 'tCreate'
    }

    def __init__(self, uid=None, name=None, content_types=None, exclude_browser=None, buffer_size=None, compression_level=None, memory_level=None, window_size=None, used_by=None, t_update=None, t_create=None, local_vars_configuration=None):  # noqa: E501
        """UpdateCompressionProfileSuccessData - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._uid = None
        self._name = None
        self._content_types = None
        self._exclude_browser = None
        self._buffer_size = None
        self._compression_level = None
        self._memory_level = None
        self._window_size = None
        self._used_by = None
        self._t_update = None
        self._t_create = None
        self.discriminator = None

        if uid is not None:
            self.uid = uid
        if name is not None:
            self.name = name
        if content_types is not None:
            self.content_types = content_types
        if exclude_browser is not None:
            self.exclude_browser = exclude_browser
        if buffer_size is not None:
            self.buffer_size = buffer_size
        if compression_level is not None:
            self.compression_level = compression_level
        if memory_level is not None:
            self.memory_level = memory_level
        if window_size is not None:
            self.window_size = window_size
        if used_by is not None:
            self.used_by = used_by
        if t_update is not None:
            self.t_update = t_update
        if t_create is not None:
            self.t_create = t_create

    @property
    def uid(self):
        """Gets the uid of this UpdateCompressionProfileSuccessData.  # noqa: E501


        :return: The uid of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :rtype: str
        """
        return self._uid

    @uid.setter
    def uid(self, uid):
        """Sets the uid of this UpdateCompressionProfileSuccessData.


        :param uid: The uid of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :type: str
        """

        self._uid = uid

    @property
    def name(self):
        """Gets the name of this UpdateCompressionProfileSuccessData.  # noqa: E501


        :return: The name of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this UpdateCompressionProfileSuccessData.


        :param name: The name of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def content_types(self):
        """Gets the content_types of this UpdateCompressionProfileSuccessData.  # noqa: E501


        :return: The content_types of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :rtype: str
        """
        return self._content_types

    @content_types.setter
    def content_types(self, content_types):
        """Sets the content_types of this UpdateCompressionProfileSuccessData.


        :param content_types: The content_types of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :type: str
        """

        self._content_types = content_types

    @property
    def exclude_browser(self):
        """Gets the exclude_browser of this UpdateCompressionProfileSuccessData.  # noqa: E501


        :return: The exclude_browser of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :rtype: str
        """
        return self._exclude_browser

    @exclude_browser.setter
    def exclude_browser(self, exclude_browser):
        """Sets the exclude_browser of this UpdateCompressionProfileSuccessData.


        :param exclude_browser: The exclude_browser of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :type: str
        """

        self._exclude_browser = exclude_browser

    @property
    def buffer_size(self):
        """Gets the buffer_size of this UpdateCompressionProfileSuccessData.  # noqa: E501


        :return: The buffer_size of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :rtype: int
        """
        return self._buffer_size

    @buffer_size.setter
    def buffer_size(self, buffer_size):
        """Sets the buffer_size of this UpdateCompressionProfileSuccessData.


        :param buffer_size: The buffer_size of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :type: int
        """

        self._buffer_size = buffer_size

    @property
    def compression_level(self):
        """Gets the compression_level of this UpdateCompressionProfileSuccessData.  # noqa: E501


        :return: The compression_level of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :rtype: int
        """
        return self._compression_level

    @compression_level.setter
    def compression_level(self, compression_level):
        """Sets the compression_level of this UpdateCompressionProfileSuccessData.


        :param compression_level: The compression_level of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :type: int
        """

        self._compression_level = compression_level

    @property
    def memory_level(self):
        """Gets the memory_level of this UpdateCompressionProfileSuccessData.  # noqa: E501


        :return: The memory_level of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :rtype: int
        """
        return self._memory_level

    @memory_level.setter
    def memory_level(self, memory_level):
        """Sets the memory_level of this UpdateCompressionProfileSuccessData.


        :param memory_level: The memory_level of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :type: int
        """

        self._memory_level = memory_level

    @property
    def window_size(self):
        """Gets the window_size of this UpdateCompressionProfileSuccessData.  # noqa: E501


        :return: The window_size of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :rtype: int
        """
        return self._window_size

    @window_size.setter
    def window_size(self, window_size):
        """Sets the window_size of this UpdateCompressionProfileSuccessData.


        :param window_size: The window_size of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :type: int
        """

        self._window_size = window_size

    @property
    def used_by(self):
        """Gets the used_by of this UpdateCompressionProfileSuccessData.  # noqa: E501


        :return: The used_by of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :rtype: list[UpdateCompressionProfileUsedBySuccessArray]
        """
        return self._used_by

    @used_by.setter
    def used_by(self, used_by):
        """Sets the used_by of this UpdateCompressionProfileSuccessData.


        :param used_by: The used_by of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :type: list[UpdateCompressionProfileUsedBySuccessArray]
        """

        self._used_by = used_by

    @property
    def t_update(self):
        """Gets the t_update of this UpdateCompressionProfileSuccessData.  # noqa: E501


        :return: The t_update of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :rtype: float
        """
        return self._t_update

    @t_update.setter
    def t_update(self, t_update):
        """Sets the t_update of this UpdateCompressionProfileSuccessData.


        :param t_update: The t_update of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :type: float
        """

        self._t_update = t_update

    @property
    def t_create(self):
        """Gets the t_create of this UpdateCompressionProfileSuccessData.  # noqa: E501


        :return: The t_create of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :rtype: float
        """
        return self._t_create

    @t_create.setter
    def t_create(self, t_create):
        """Sets the t_create of this UpdateCompressionProfileSuccessData.


        :param t_create: The t_create of this UpdateCompressionProfileSuccessData.  # noqa: E501
        :type: float
        """

        self._t_create = t_create

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, UpdateCompressionProfileSuccessData):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, UpdateCompressionProfileSuccessData):
            return True

        return self.to_dict() != other.to_dict()
