# coding: utf-8

"""
    R&S Web Application Firewall / iSuite configuration API

    The Rohde & Schwarz's WAF management API provides a REST/JSON programming interface. It allows automation and scripting of WAF administration tasks, such as management of reverse proxies and tunnels. The API documentation is shipped with the API service itself. Once installed on your management box, you can access the documentation with the following URL (where \"your-waf\" is the IP address or hostname of your management box):<br/><a class=\"custom-link\" href=\"https://your-waf:3001/wafapi/doc\">https://your-waf:3001/wafapi/doc</a>  # noqa: E501

    The version of the OpenAPI document: 1.0.9
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six

from r_s_waf_api_client.configuration import Configuration


class CreateApplianceSuccessDataDns(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'enable': 'bool',
        'dns1': 'str',
        'dns2': 'str',
        'dns_ttl_min': 'str',
        'dns_ttl_max': 'str'
    }

    attribute_map = {
        'enable': 'enable',
        'dns1': 'dns1',
        'dns2': 'dns2',
        'dns_ttl_min': 'dnsTtlMin',
        'dns_ttl_max': 'dnsTtlMax'
    }

    def __init__(self, enable=None, dns1=None, dns2=None, dns_ttl_min=None, dns_ttl_max=None, local_vars_configuration=None):  # noqa: E501
        """CreateApplianceSuccessDataDns - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._enable = None
        self._dns1 = None
        self._dns2 = None
        self._dns_ttl_min = None
        self._dns_ttl_max = None
        self.discriminator = None

        if enable is not None:
            self.enable = enable
        if dns1 is not None:
            self.dns1 = dns1
        if dns2 is not None:
            self.dns2 = dns2
        if dns_ttl_min is not None:
            self.dns_ttl_min = dns_ttl_min
        if dns_ttl_max is not None:
            self.dns_ttl_max = dns_ttl_max

    @property
    def enable(self):
        """Gets the enable of this CreateApplianceSuccessDataDns.  # noqa: E501


        :return: The enable of this CreateApplianceSuccessDataDns.  # noqa: E501
        :rtype: bool
        """
        return self._enable

    @enable.setter
    def enable(self, enable):
        """Sets the enable of this CreateApplianceSuccessDataDns.


        :param enable: The enable of this CreateApplianceSuccessDataDns.  # noqa: E501
        :type: bool
        """

        self._enable = enable

    @property
    def dns1(self):
        """Gets the dns1 of this CreateApplianceSuccessDataDns.  # noqa: E501


        :return: The dns1 of this CreateApplianceSuccessDataDns.  # noqa: E501
        :rtype: str
        """
        return self._dns1

    @dns1.setter
    def dns1(self, dns1):
        """Sets the dns1 of this CreateApplianceSuccessDataDns.


        :param dns1: The dns1 of this CreateApplianceSuccessDataDns.  # noqa: E501
        :type: str
        """

        self._dns1 = dns1

    @property
    def dns2(self):
        """Gets the dns2 of this CreateApplianceSuccessDataDns.  # noqa: E501


        :return: The dns2 of this CreateApplianceSuccessDataDns.  # noqa: E501
        :rtype: str
        """
        return self._dns2

    @dns2.setter
    def dns2(self, dns2):
        """Sets the dns2 of this CreateApplianceSuccessDataDns.


        :param dns2: The dns2 of this CreateApplianceSuccessDataDns.  # noqa: E501
        :type: str
        """

        self._dns2 = dns2

    @property
    def dns_ttl_min(self):
        """Gets the dns_ttl_min of this CreateApplianceSuccessDataDns.  # noqa: E501


        :return: The dns_ttl_min of this CreateApplianceSuccessDataDns.  # noqa: E501
        :rtype: str
        """
        return self._dns_ttl_min

    @dns_ttl_min.setter
    def dns_ttl_min(self, dns_ttl_min):
        """Sets the dns_ttl_min of this CreateApplianceSuccessDataDns.


        :param dns_ttl_min: The dns_ttl_min of this CreateApplianceSuccessDataDns.  # noqa: E501
        :type: str
        """

        self._dns_ttl_min = dns_ttl_min

    @property
    def dns_ttl_max(self):
        """Gets the dns_ttl_max of this CreateApplianceSuccessDataDns.  # noqa: E501


        :return: The dns_ttl_max of this CreateApplianceSuccessDataDns.  # noqa: E501
        :rtype: str
        """
        return self._dns_ttl_max

    @dns_ttl_max.setter
    def dns_ttl_max(self, dns_ttl_max):
        """Sets the dns_ttl_max of this CreateApplianceSuccessDataDns.


        :param dns_ttl_max: The dns_ttl_max of this CreateApplianceSuccessDataDns.  # noqa: E501
        :type: str
        """

        self._dns_ttl_max = dns_ttl_max

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, CreateApplianceSuccessDataDns):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, CreateApplianceSuccessDataDns):
            return True

        return self.to_dict() != other.to_dict()
