# coding: utf-8

"""
    R&S Web Application Firewall / iSuite configuration API

    The Rohde & Schwarz's WAF management API provides a REST/JSON programming interface. It allows automation and scripting of WAF administration tasks, such as management of reverse proxies and tunnels. The API documentation is shipped with the API service itself. Once installed on your management box, you can access the documentation with the following URL (where \"your-waf\" is the IP address or hostname of your management box):<br/><a class=\"custom-link\" href=\"https://your-waf:3001/wafapi/doc\">https://your-waf:3001/wafapi/doc</a>  # noqa: E501

    The version of the OpenAPI document: 1.0.9
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six

from r_s_waf_api_client.configuration import Configuration


class GetLabelsDataSuccessArray(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'uid': 'str',
        'name': 'str',
        'description': 'str',
        'color': 'str',
        't_update': 'float',
        't_create': 'float'
    }

    attribute_map = {
        'uid': 'uid',
        'name': 'name',
        'description': 'description',
        'color': 'color',
        't_update': 'tUpdate',
        't_create': 'tCreate'
    }

    def __init__(self, uid=None, name=None, description=None, color=None, t_update=None, t_create=None, local_vars_configuration=None):  # noqa: E501
        """GetLabelsDataSuccessArray - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._uid = None
        self._name = None
        self._description = None
        self._color = None
        self._t_update = None
        self._t_create = None
        self.discriminator = None

        if uid is not None:
            self.uid = uid
        if name is not None:
            self.name = name
        if description is not None:
            self.description = description
        if color is not None:
            self.color = color
        if t_update is not None:
            self.t_update = t_update
        if t_create is not None:
            self.t_create = t_create

    @property
    def uid(self):
        """Gets the uid of this GetLabelsDataSuccessArray.  # noqa: E501


        :return: The uid of this GetLabelsDataSuccessArray.  # noqa: E501
        :rtype: str
        """
        return self._uid

    @uid.setter
    def uid(self, uid):
        """Sets the uid of this GetLabelsDataSuccessArray.


        :param uid: The uid of this GetLabelsDataSuccessArray.  # noqa: E501
        :type: str
        """

        self._uid = uid

    @property
    def name(self):
        """Gets the name of this GetLabelsDataSuccessArray.  # noqa: E501


        :return: The name of this GetLabelsDataSuccessArray.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this GetLabelsDataSuccessArray.


        :param name: The name of this GetLabelsDataSuccessArray.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def description(self):
        """Gets the description of this GetLabelsDataSuccessArray.  # noqa: E501


        :return: The description of this GetLabelsDataSuccessArray.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this GetLabelsDataSuccessArray.


        :param description: The description of this GetLabelsDataSuccessArray.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def color(self):
        """Gets the color of this GetLabelsDataSuccessArray.  # noqa: E501


        :return: The color of this GetLabelsDataSuccessArray.  # noqa: E501
        :rtype: str
        """
        return self._color

    @color.setter
    def color(self, color):
        """Sets the color of this GetLabelsDataSuccessArray.


        :param color: The color of this GetLabelsDataSuccessArray.  # noqa: E501
        :type: str
        """

        self._color = color

    @property
    def t_update(self):
        """Gets the t_update of this GetLabelsDataSuccessArray.  # noqa: E501


        :return: The t_update of this GetLabelsDataSuccessArray.  # noqa: E501
        :rtype: float
        """
        return self._t_update

    @t_update.setter
    def t_update(self, t_update):
        """Sets the t_update of this GetLabelsDataSuccessArray.


        :param t_update: The t_update of this GetLabelsDataSuccessArray.  # noqa: E501
        :type: float
        """

        self._t_update = t_update

    @property
    def t_create(self):
        """Gets the t_create of this GetLabelsDataSuccessArray.  # noqa: E501


        :return: The t_create of this GetLabelsDataSuccessArray.  # noqa: E501
        :rtype: float
        """
        return self._t_create

    @t_create.setter
    def t_create(self, t_create):
        """Sets the t_create of this GetLabelsDataSuccessArray.


        :param t_create: The t_create of this GetLabelsDataSuccessArray.  # noqa: E501
        :type: float
        """

        self._t_create = t_create

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, GetLabelsDataSuccessArray):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, GetLabelsDataSuccessArray):
            return True

        return self.to_dict() != other.to_dict()
