# coding: utf-8

"""
    R&S Web Application Firewall / iSuite configuration API

    The Rohde & Schwarz's WAF management API provides a REST/JSON programming interface. It allows automation and scripting of WAF administration tasks, such as management of reverse proxies and tunnels. The API documentation is shipped with the API service itself. Once installed on your management box, you can access the documentation with the following URL (where \"your-waf\" is the IP address or hostname of your management box):<br/><a class=\"custom-link\" href=\"https://your-waf:3001/wafapi/doc\">https://your-waf:3001/wafapi/doc</a>  # noqa: E501

    The version of the OpenAPI document: 1.0.9
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six

from r_s_waf_api_client.configuration import Configuration


class CreateTunnelNetworkOutgoingSsl(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'profile': 'ApplySSLKeyUid',
        'certificate': 'object',
        'verify_backend_certificate': 'CreateTunnelNetworkOutgoingSslVerifyBackendCertificate',
        'ajp_enable': 'bool'
    }

    attribute_map = {
        'profile': 'profile',
        'certificate': 'certificate',
        'verify_backend_certificate': 'verifyBackendCertificate',
        'ajp_enable': 'ajpEnable'
    }

    def __init__(self, profile=None, certificate=None, verify_backend_certificate=None, ajp_enable=None, local_vars_configuration=None):  # noqa: E501
        """CreateTunnelNetworkOutgoingSsl - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._profile = None
        self._certificate = None
        self._verify_backend_certificate = None
        self._ajp_enable = None
        self.discriminator = None

        if profile is not None:
            self.profile = profile
        if certificate is not None:
            self.certificate = certificate
        if verify_backend_certificate is not None:
            self.verify_backend_certificate = verify_backend_certificate
        if ajp_enable is not None:
            self.ajp_enable = ajp_enable

    @property
    def profile(self):
        """Gets the profile of this CreateTunnelNetworkOutgoingSsl.  # noqa: E501


        :return: The profile of this CreateTunnelNetworkOutgoingSsl.  # noqa: E501
        :rtype: ApplySSLKeyUid
        """
        return self._profile

    @profile.setter
    def profile(self, profile):
        """Sets the profile of this CreateTunnelNetworkOutgoingSsl.


        :param profile: The profile of this CreateTunnelNetworkOutgoingSsl.  # noqa: E501
        :type: ApplySSLKeyUid
        """

        self._profile = profile

    @property
    def certificate(self):
        """Gets the certificate of this CreateTunnelNetworkOutgoingSsl.  # noqa: E501


        :return: The certificate of this CreateTunnelNetworkOutgoingSsl.  # noqa: E501
        :rtype: object
        """
        return self._certificate

    @certificate.setter
    def certificate(self, certificate):
        """Sets the certificate of this CreateTunnelNetworkOutgoingSsl.


        :param certificate: The certificate of this CreateTunnelNetworkOutgoingSsl.  # noqa: E501
        :type: object
        """

        self._certificate = certificate

    @property
    def verify_backend_certificate(self):
        """Gets the verify_backend_certificate of this CreateTunnelNetworkOutgoingSsl.  # noqa: E501


        :return: The verify_backend_certificate of this CreateTunnelNetworkOutgoingSsl.  # noqa: E501
        :rtype: CreateTunnelNetworkOutgoingSslVerifyBackendCertificate
        """
        return self._verify_backend_certificate

    @verify_backend_certificate.setter
    def verify_backend_certificate(self, verify_backend_certificate):
        """Sets the verify_backend_certificate of this CreateTunnelNetworkOutgoingSsl.


        :param verify_backend_certificate: The verify_backend_certificate of this CreateTunnelNetworkOutgoingSsl.  # noqa: E501
        :type: CreateTunnelNetworkOutgoingSslVerifyBackendCertificate
        """

        self._verify_backend_certificate = verify_backend_certificate

    @property
    def ajp_enable(self):
        """Gets the ajp_enable of this CreateTunnelNetworkOutgoingSsl.  # noqa: E501


        :return: The ajp_enable of this CreateTunnelNetworkOutgoingSsl.  # noqa: E501
        :rtype: bool
        """
        return self._ajp_enable

    @ajp_enable.setter
    def ajp_enable(self, ajp_enable):
        """Sets the ajp_enable of this CreateTunnelNetworkOutgoingSsl.


        :param ajp_enable: The ajp_enable of this CreateTunnelNetworkOutgoingSsl.  # noqa: E501
        :type: bool
        """

        self._ajp_enable = ajp_enable

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, CreateTunnelNetworkOutgoingSsl):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, CreateTunnelNetworkOutgoingSsl):
            return True

        return self.to_dict() != other.to_dict()
