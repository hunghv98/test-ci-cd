# coding: utf-8

"""
    R&S Web Application Firewall / iSuite configuration API

    The Rohde & Schwarz's WAF management API provides a REST/JSON programming interface. It allows automation and scripting of WAF administration tasks, such as management of reverse proxies and tunnels. The API documentation is shipped with the API service itself. Once installed on your management box, you can access the documentation with the following URL (where \"your-waf\" is the IP address or hostname of your management box):<br/><a class=\"custom-link\" href=\"https://your-waf:3001/wafapi/doc\">https://your-waf:3001/wafapi/doc</a>  # noqa: E501

    The version of the OpenAPI document: 1.0.9
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six

from r_s_waf_api_client.configuration import Configuration


class GetLicensesDataSuccessArrayNumberOf(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'reverse_proxies': 'int',
        'tunnels': 'int',
        'max_client': 'int',
        'wam_session': 'int',
        'cpu': 'int',
        'ram': 'int'
    }

    attribute_map = {
        'reverse_proxies': 'reverseProxies',
        'tunnels': 'tunnels',
        'max_client': 'maxClient',
        'wam_session': 'wamSession',
        'cpu': 'cpu',
        'ram': 'ram'
    }

    def __init__(self, reverse_proxies=None, tunnels=None, max_client=None, wam_session=None, cpu=None, ram=None, local_vars_configuration=None):  # noqa: E501
        """GetLicensesDataSuccessArrayNumberOf - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._reverse_proxies = None
        self._tunnels = None
        self._max_client = None
        self._wam_session = None
        self._cpu = None
        self._ram = None
        self.discriminator = None

        if reverse_proxies is not None:
            self.reverse_proxies = reverse_proxies
        if tunnels is not None:
            self.tunnels = tunnels
        if max_client is not None:
            self.max_client = max_client
        if wam_session is not None:
            self.wam_session = wam_session
        if cpu is not None:
            self.cpu = cpu
        if ram is not None:
            self.ram = ram

    @property
    def reverse_proxies(self):
        """Gets the reverse_proxies of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501


        :return: The reverse_proxies of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501
        :rtype: int
        """
        return self._reverse_proxies

    @reverse_proxies.setter
    def reverse_proxies(self, reverse_proxies):
        """Sets the reverse_proxies of this GetLicensesDataSuccessArrayNumberOf.


        :param reverse_proxies: The reverse_proxies of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501
        :type: int
        """

        self._reverse_proxies = reverse_proxies

    @property
    def tunnels(self):
        """Gets the tunnels of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501


        :return: The tunnels of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501
        :rtype: int
        """
        return self._tunnels

    @tunnels.setter
    def tunnels(self, tunnels):
        """Sets the tunnels of this GetLicensesDataSuccessArrayNumberOf.


        :param tunnels: The tunnels of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501
        :type: int
        """

        self._tunnels = tunnels

    @property
    def max_client(self):
        """Gets the max_client of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501


        :return: The max_client of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501
        :rtype: int
        """
        return self._max_client

    @max_client.setter
    def max_client(self, max_client):
        """Sets the max_client of this GetLicensesDataSuccessArrayNumberOf.


        :param max_client: The max_client of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501
        :type: int
        """

        self._max_client = max_client

    @property
    def wam_session(self):
        """Gets the wam_session of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501


        :return: The wam_session of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501
        :rtype: int
        """
        return self._wam_session

    @wam_session.setter
    def wam_session(self, wam_session):
        """Sets the wam_session of this GetLicensesDataSuccessArrayNumberOf.


        :param wam_session: The wam_session of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501
        :type: int
        """

        self._wam_session = wam_session

    @property
    def cpu(self):
        """Gets the cpu of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501


        :return: The cpu of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501
        :rtype: int
        """
        return self._cpu

    @cpu.setter
    def cpu(self, cpu):
        """Sets the cpu of this GetLicensesDataSuccessArrayNumberOf.


        :param cpu: The cpu of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501
        :type: int
        """

        self._cpu = cpu

    @property
    def ram(self):
        """Gets the ram of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501


        :return: The ram of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501
        :rtype: int
        """
        return self._ram

    @ram.setter
    def ram(self, ram):
        """Sets the ram of this GetLicensesDataSuccessArrayNumberOf.


        :param ram: The ram of this GetLicensesDataSuccessArrayNumberOf.  # noqa: E501
        :type: int
        """

        self._ram = ram

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, GetLicensesDataSuccessArrayNumberOf):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, GetLicensesDataSuccessArrayNumberOf):
            return True

        return self.to_dict() != other.to_dict()
