# coding: utf-8

"""
    R&S Web Application Firewall / iSuite configuration API

    The Rohde & Schwarz's WAF management API provides a REST/JSON programming interface. It allows automation and scripting of WAF administration tasks, such as management of reverse proxies and tunnels. The API documentation is shipped with the API service itself. Once installed on your management box, you can access the documentation with the following URL (where \"your-waf\" is the IP address or hostname of your management box):<br/><a class=\"custom-link\" href=\"https://your-waf:3001/wafapi/doc\">https://your-waf:3001/wafapi/doc</a>  # noqa: E501

    The version of the OpenAPI document: 1.0.9
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import r_s_waf_api_client
from r_s_waf_api_client.models.get_security_exception_profiles_success import GetSecurityExceptionProfilesSuccess  # noqa: E501
from r_s_waf_api_client.rest import ApiException

class TestGetSecurityExceptionProfilesSuccess(unittest.TestCase):
    """GetSecurityExceptionProfilesSuccess unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test GetSecurityExceptionProfilesSuccess
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # model = r_s_waf_api_client.models.get_security_exception_profiles_success.GetSecurityExceptionProfilesSuccess()  # noqa: E501
        if include_optional :
            return GetSecurityExceptionProfilesSuccess(
                data = [
                    r_s_waf_api_client.models.get_security_exception_profiles_data_success_array.GetSecurityExceptionProfilesDataSuccessArray(
                        uid = '0', 
                        name = '0', 
                        read_only = True, 
                        description = '0', 
                        rules = [
                            r_s_waf_api_client.models.get_security_exception_profiles_rules_success_array.GetSecurityExceptionProfilesRulesSuccessArray(
                                uid = '0', 
                                name = '0', 
                                enabled = True, 
                                description = '0', 
                                conditions = [
                                    r_s_waf_api_client.models.get_security_exception_profiles_conditions_success_array.GetSecurityExceptionProfilesConditionsSuccessArray(
                                        part = '0', 
                                        key_operator = '0', 
                                        key = '0', 
                                        value_operator = '0', 
                                        value = '0', )
                                    ], 
                                matching_parts = [
                                    r_s_waf_api_client.models.get_security_exception_profiles_matching_parts_success_array.GetSecurityExceptionProfilesMatchingPartsSuccessArray(
                                        part = '0', 
                                        key_operator = '0', 
                                        key = '0', 
                                        value_type = '0', 
                                        value_operator = '0', 
                                        value = '0', )
                                    ], )
                            ], 
                        t_update = 1.337, 
                        t_create = 1.337, )
                    ], 
                data_rules = [
                    r_s_waf_api_client.models.get_security_exception_profiles_rules_success_array.GetSecurityExceptionProfilesRulesSuccessArray(
                        uid = '0', 
                        name = '0', 
                        enabled = True, 
                        description = '0', 
                        conditions = [
                            r_s_waf_api_client.models.get_security_exception_profiles_conditions_success_array.GetSecurityExceptionProfilesConditionsSuccessArray(
                                part = '0', 
                                key_operator = '0', 
                                key = '0', 
                                value_operator = '0', 
                                value = '0', )
                            ], 
                        matching_parts = [
                            r_s_waf_api_client.models.get_security_exception_profiles_matching_parts_success_array.GetSecurityExceptionProfilesMatchingPartsSuccessArray(
                                part = '0', 
                                key_operator = '0', 
                                key = '0', 
                                value_type = '0', 
                                value_operator = '0', 
                                value = '0', )
                            ], )
                    ], 
                data_rules_conditions = [
                    r_s_waf_api_client.models.get_security_exception_profiles_conditions_success_array.GetSecurityExceptionProfilesConditionsSuccessArray(
                        part = '0', 
                        key_operator = '0', 
                        key = '0', 
                        value_operator = '0', 
                        value = '0', )
                    ], 
                data_rules_matching_parts = [
                    r_s_waf_api_client.models.get_security_exception_profiles_matching_parts_success_array.GetSecurityExceptionProfilesMatchingPartsSuccessArray(
                        part = '0', 
                        key_operator = '0', 
                        key = '0', 
                        value_type = '0', 
                        value_operator = '0', 
                        value = '0', )
                    ]
            )
        else :
            return GetSecurityExceptionProfilesSuccess(
        )

    def testGetSecurityExceptionProfilesSuccess(self):
        """Test GetSecurityExceptionProfilesSuccess"""
        inst_req_only = self.make_instance(include_optional=False)
        inst_req_and_optional = self.make_instance(include_optional=True)


if __name__ == '__main__':
    unittest.main()
