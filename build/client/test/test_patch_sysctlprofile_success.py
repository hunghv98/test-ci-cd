# coding: utf-8

"""
    R&S Web Application Firewall / iSuite configuration API

    The Rohde & Schwarz's WAF management API provides a REST/JSON programming interface. It allows automation and scripting of WAF administration tasks, such as management of reverse proxies and tunnels. The API documentation is shipped with the API service itself. Once installed on your management box, you can access the documentation with the following URL (where \"your-waf\" is the IP address or hostname of your management box):<br/><a class=\"custom-link\" href=\"https://your-waf:3001/wafapi/doc\">https://your-waf:3001/wafapi/doc</a>  # noqa: E501

    The version of the OpenAPI document: 1.0.9
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import r_s_waf_api_client
from r_s_waf_api_client.models.patch_sysctlprofile_success import PatchSysctlprofileSuccess  # noqa: E501
from r_s_waf_api_client.rest import ApiException

class TestPatchSysctlprofileSuccess(unittest.TestCase):
    """PatchSysctlprofileSuccess unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test PatchSysctlprofileSuccess
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # model = r_s_waf_api_client.models.patch_sysctlprofile_success.PatchSysctlprofileSuccess()  # noqa: E501
        if include_optional :
            return PatchSysctlprofileSuccess(
                data = r_s_waf_api_client.models.create_sysctlprofile_success_data.CreateSysctlprofileSuccess_data(
                    uid = '0', 
                    name = '0', 
                    kernel_shm_max = 56, 
                    net_ipv4_conf_all_rp_filter = 56, 
                    net_core_optmem_max = 56, 
                    net_ipv4_tcp_ecn = 56, 
                    net_ipv4_tcp_congestion_control = '0', 
                    net_ipv4_tcp_fack = 56, 
                    net_ipv4_tcp_sack = 56, 
                    net_ipv4_tcp_dsack = 56, 
                    net_ipv4_tcp_timestamps = 56, 
                    net_ipv4_tcp_windows_scaling = 56, 
                    net_ipv4_tcp_adv_win_scale = 56, 
                    net_ipv4_tcp_workaround_signed_windows = 56, 
                    net_ipv4_tcp_syncookies = 56, 
                    net_ipv4_tcp_fin_timeout = 56, 
                    net_ipv4_tcp_keepalive_time = 56, 
                    net_ipv4_tcp_keepalive_intvl = 56, 
                    net_ipv4_tcp_keepalive_probes = 56, 
                    net_ipv4_tcp_tw_reuse = 56, 
                    net_ipv4_tcp_max_tw_buckets = 56, 
                    net_ipv4_tcp_max_orphans = 56, 
                    net_ipv4_tcp_max_syn_backlog = 56, 
                    net_core_netdev_max_backlog = 56, 
                    net_ipv4_tcp_moderate_rcvbuf = 56, 
                    net_ipv4_tcp_rmem_min = 56, 
                    net_ipv4_tcp_rmem_default = 56, 
                    net_ipv4_tcp_rmem_max = 56, 
                    net_ipv4_tcp_wmem_min = 56, 
                    net_ipv4_tcp_wmem_default = 56, 
                    net_ipv4_tcp_wmem_max = 56, 
                    net_core_rmem_default = 56, 
                    net_core_wmem_default = 56, 
                    net_core_somaxconn = 56, 
                    net_ipv4_tcp_abort_on_overflow = 56, 
                    net_ipv4_tcp_syn_retries = 56, 
                    net_ipv4_tcp_syn_ack_retries = 56, 
                    net_ipv4_tcp_tw_recycle = 56, 
                    net_ipv4_tcp_rfc1337 = 56, )
            )
        else :
            return PatchSysctlprofileSuccess(
        )

    def testPatchSysctlprofileSuccess(self):
        """Test PatchSysctlprofileSuccess"""
        inst_req_only = self.make_instance(include_optional=False)
        inst_req_and_optional = self.make_instance(include_optional=True)


if __name__ == '__main__':
    unittest.main()
