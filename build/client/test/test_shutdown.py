# coding: utf-8

"""
    R&S Web Application Firewall / iSuite configuration API

    The Rohde & Schwarz's WAF management API provides a REST/JSON programming interface. It allows automation and scripting of WAF administration tasks, such as management of reverse proxies and tunnels. The API documentation is shipped with the API service itself. Once installed on your management box, you can access the documentation with the following URL (where \"your-waf\" is the IP address or hostname of your management box):<br/><a class=\"custom-link\" href=\"https://your-waf:3001/wafapi/doc\">https://your-waf:3001/wafapi/doc</a>  # noqa: E501

    The version of the OpenAPI document: 1.0.9
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import r_s_waf_api_client
from r_s_waf_api_client.models.shutdown import Shutdown  # noqa: E501
from r_s_waf_api_client.rest import ApiException

class TestShutdown(unittest.TestCase):
    """Shutdown unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test Shutdown
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # model = r_s_waf_api_client.models.shutdown.Shutdown()  # noqa: E501
        if include_optional :
            return Shutdown(
                appliance_uid = '0', 
                mode = '0', 
                force_file_check = True
            )
        else :
            return Shutdown(
        )

    def testShutdown(self):
        """Test Shutdown"""
        inst_req_only = self.make_instance(include_optional=False)
        inst_req_and_optional = self.make_instance(include_optional=True)


if __name__ == '__main__':
    unittest.main()
