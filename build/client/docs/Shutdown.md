# Shutdown

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appliance_uid** | **str** |  | [optional] 
**mode** | **str** |  | [optional] 
**force_file_check** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


