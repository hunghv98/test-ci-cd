# PatchApplianceSuccessDataSsh

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enable** | **bool** |  | [optional] 
**interfaces** | [**list[PatchApplianceInterfacesSuccessArray]**](PatchApplianceInterfacesSuccessArray.md) |  | [optional] 
**port** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


