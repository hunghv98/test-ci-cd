# PatchSecurityExceptionProfileRulesSuccessArray

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**description** | **str** |  | [optional] 
**conditions** | [**list[PatchSecurityExceptionProfileConditionsSuccessArray]**](PatchSecurityExceptionProfileConditionsSuccessArray.md) |  | [optional] 
**matching_parts** | [**list[PatchSecurityExceptionProfileMatchingPartsSuccessArray]**](PatchSecurityExceptionProfileMatchingPartsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


