# GetScoringlistsSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list[GetScoringlistsDataSuccessArray]**](GetScoringlistsDataSuccessArray.md) |  | [optional] 
**data_exceptions** | [**list[GetScoringlistsExceptionsSuccessArray]**](GetScoringlistsExceptionsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


