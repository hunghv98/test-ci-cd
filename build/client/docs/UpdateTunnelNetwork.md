# UpdateTunnelNetwork

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**incoming** | [**UpdateTunnelNetworkIncoming**](UpdateTunnelNetworkIncoming.md) |  | 
**outgoing** | [**PatchTunnelNetworkOutgoing**](PatchTunnelNetworkOutgoing.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


