# UpdateSysctlprofile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**kernel_shm_max** | **int** |  | [optional] 
**net_ipv4_conf_all_rp_filter** | **int** |  | [optional] 
**net_core_optmem_max** | **int** |  | [optional] 
**net_ipv4_tcp_ecn** | **int** |  | [optional] 
**net_ipv4_tcp_congestion_control** | **str** |  | [optional] 
**net_ipv4_tcp_fack** | **int** |  | [optional] 
**net_ipv4_tcp_sack** | **int** |  | [optional] 
**net_ipv4_tcp_dsack** | **int** |  | [optional] 
**net_ipv4_tcp_timestamps** | **int** |  | [optional] 
**net_ipv4_tcp_windows_scaling** | **int** |  | [optional] 
**net_ipv4_tcp_adv_win_scale** | **int** |  | [optional] 
**net_ipv4_tcp_workaround_signed_windows** | **int** |  | [optional] 
**net_ipv4_tcp_syncookies** | **int** |  | [optional] 
**net_ipv4_tcp_fin_timeout** | **int** |  | [optional] 
**net_ipv4_tcp_keepalive_time** | **int** |  | [optional] 
**net_ipv4_tcp_keepalive_intvl** | **int** |  | [optional] 
**net_ipv4_tcp_keepalive_probes** | **int** |  | [optional] 
**net_ipv4_tcp_tw_reuse** | **int** |  | [optional] 
**net_ipv4_tcp_max_tw_buckets** | **int** |  | [optional] 
**net_ipv4_tcp_max_orphans** | **int** |  | [optional] 
**net_ipv4_tcp_max_syn_backlog** | **int** |  | [optional] 
**net_core_netdev_max_backlog** | **int** |  | [optional] 
**net_ipv4_tcp_moderate_rcvbuf** | **int** |  | [optional] 
**net_ipv4_tcp_rmem_min** | **int** |  | [optional] 
**net_ipv4_tcp_rmem_default** | **int** |  | [optional] 
**net_ipv4_tcp_rmem_max** | **int** |  | [optional] 
**net_ipv4_tcp_wmem_min** | **int** |  | [optional] 
**net_ipv4_tcp_wmem_default** | **int** |  | [optional] 
**net_ipv4_tcp_wmem_max** | **int** |  | [optional] 
**net_core_rmem_default** | **int** |  | [optional] 
**net_core_wmem_default** | **int** |  | [optional] 
**net_core_somaxconn** | **int** |  | [optional] 
**net_ipv4_tcp_abort_on_overflow** | **int** |  | [optional] 
**net_ipv4_tcp_syn_retries** | **int** |  | [optional] 
**net_ipv4_tcp_syn_ack_retries** | **int** |  | [optional] 
**net_ipv4_tcp_tw_recycle** | **int** |  | [optional] 
**net_ipv4_tcp_rfc1337** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


