# CreateAlertingDestinations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**smtp_config** | [**CreateAlertingDestinationsSMTPConfig**](CreateAlertingDestinationsSMTPConfig.md) |  | [optional] 
**syslog_config** | [**CreateAlertingDestinationsSyslogConfig**](CreateAlertingDestinationsSyslogConfig.md) |  | [optional] 
**snmp_config** | [**CreateAlertingDestinationsSNMPConfig**](CreateAlertingDestinationsSNMPConfig.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


