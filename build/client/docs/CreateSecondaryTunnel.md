# CreateSecondaryTunnel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reverse_proxy** | [**UpdateNetworkInterfaceDevice**](UpdateNetworkInterfaceDevice.md) |  | [optional] 
**parent_tunnel** | [**UpdateNetworkInterfaceDevice**](UpdateNetworkInterfaceDevice.md) |  | [optional] 
**in_network_interface** | [**UpdateNetworkInterfaceDevice**](UpdateNetworkInterfaceDevice.md) |  | [optional] 
**active** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


