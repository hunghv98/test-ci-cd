# GetSecondaryTunnelsDataSuccessArray

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**active** | **str** |  | [optional] 
**reverse_proxy** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**parent_tunnel** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**in_network_interface** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**t_update** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


