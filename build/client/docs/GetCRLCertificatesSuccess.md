# GetCRLCertificatesSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list[GetCRLCertificatesDataSuccessArray]**](GetCRLCertificatesDataSuccessArray.md) |  | [optional] 
**data_crl** | [**list[GetCRLCertificatesCrlSuccessArray]**](GetCRLCertificatesCrlSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


