# CreateAlertingDestinationsSMTPConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ip** | **str** |  | 
**port** | **int** |  | 
**login** | **str** |  | [optional] 
**_pass** | **str** |  | [optional] 
**mail_from** | **str** |  | 
**mail_to** | **str** |  | 
**mail_crypt_protocol** | **str** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


