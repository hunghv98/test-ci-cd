# UpdateNetworkInterfaceSuccessData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**device** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**appliance** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**ip** | **str** |  | [optional] 
**netmask** | **str** |  | [optional] 
**active** | **str** |  | [optional] 
**used_by** | [**list[UpdateNetworkInterfaceUsedBySuccessArray]**](UpdateNetworkInterfaceUsedBySuccessArray.md) |  | [optional] 
**t_update** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


