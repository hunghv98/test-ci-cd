# PatchReverseProxyProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**start_server** | **int** |  | [optional] 
**server_limit** | **int** |  | [optional] 
**max_clients** | **int** |  | [optional] 
**max_spare_threads** | **int** |  | [optional] 
**min_spare_threads** | **int** |  | [optional] 
**thread_per_child** | **int** |  | [optional] 
**max_requests_per_child** | **int** |  | [optional] 
**limit_request_field_size** | **int** |  | [optional] 
**timeout** | **int** |  | [optional] 
**proxy_timeout** | **int** |  | [optional] 
**keep_alive** | **bool** |  | [optional] 
**keep_alive_timeout** | **int** |  | [optional] 
**max_keep_alive_requests** | **int** |  | [optional] 
**ssl_session_cache_size** | **int** |  | [optional] 
**ssl_session_cache_timeout** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


