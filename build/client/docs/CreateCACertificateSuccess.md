# CreateCACertificateSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**CreateCACertificateSuccessData**](CreateCACertificateSuccessData.md) |  | [optional] 
**data_ca** | [**list[CreateCACertificateCaSuccessArray]**](CreateCACertificateCaSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


