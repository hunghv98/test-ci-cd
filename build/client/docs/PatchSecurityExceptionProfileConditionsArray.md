# PatchSecurityExceptionProfileConditionsArray

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**part** | **str** |  | [optional] 
**key_operator** | **str** |  | [optional] 
**key** | **str** |  | [optional] 
**value_operator** | **str** |  | [optional] 
**value** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


