# AddReverseProxy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appliance** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**name** | **str** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**healthcheck_uri** | **str** |  | [optional] 
**access_log** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**advanced_parameters** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**block_default_tunnel** | [**AddReverseProxyBlockDefaultTunnel**](AddReverseProxyBlockDefaultTunnel.md) |  | [optional] 
**request_timeout** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**no_log_on** | [**AddReverseProxyNoLogOn**](AddReverseProxyNoLogOn.md) |  | [optional] 
**labels** | [**list[AddReverseProxyLabelsArray]**](AddReverseProxyLabelsArray.md) |  | [optional] 
**syslog_destinations** | [**AddReverseProxySyslogDestinations**](AddReverseProxySyslogDestinations.md) |  | [optional] 
**logs** | [**AddReverseProxyLogs**](AddReverseProxyLogs.md) |  | [optional] 
**monitor** | [**AddReverseProxyMonitor**](AddReverseProxyMonitor.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


