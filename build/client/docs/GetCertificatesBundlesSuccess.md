# GetCertificatesBundlesSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list[GetCertificatesBundlesDataSuccessArray]**](GetCertificatesBundlesDataSuccessArray.md) |  | [optional] 
**data_ca** | [**list[GetCertificatesBundlesCaSuccessArray]**](GetCertificatesBundlesCaSuccessArray.md) |  | [optional] 
**data_ca_ocsp** | [**list[GetCertificatesBundlesCaOCSPSuccessArray]**](GetCertificatesBundlesCaOCSPSuccessArray.md) |  | [optional] 
**data_crl** | [**list[GetCertificatesBundlesCrlSuccessArray]**](GetCertificatesBundlesCrlSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


