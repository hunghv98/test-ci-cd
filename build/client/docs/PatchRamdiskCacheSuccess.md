# PatchRamdiskCacheSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**PatchRamdiskCacheSuccessData**](PatchRamdiskCacheSuccessData.md) |  | [optional] 
**data_used_by** | [**list[PatchRamdiskCacheUsedBySuccessArray]**](PatchRamdiskCacheUsedBySuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


