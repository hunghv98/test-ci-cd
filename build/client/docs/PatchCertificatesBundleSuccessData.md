# PatchCertificatesBundleSuccessData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**ca** | [**list[PatchCertificatesBundleCaSuccessArray]**](PatchCertificatesBundleCaSuccessArray.md) |  | [optional] 
**ca_ocsp** | [**list[PatchCertificatesBundleCaOCSPSuccessArray]**](PatchCertificatesBundleCaOCSPSuccessArray.md) |  | [optional] 
**crl** | [**list[PatchCertificatesBundleCrlSuccessArray]**](PatchCertificatesBundleCrlSuccessArray.md) |  | [optional] 
**t_update** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


