# UpdateSecurityExceptionProfileRulesSuccessArray

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**description** | **str** |  | [optional] 
**conditions** | [**list[UpdateSecurityExceptionProfileConditionsSuccessArray]**](UpdateSecurityExceptionProfileConditionsSuccessArray.md) |  | [optional] 
**matching_parts** | [**list[UpdateSecurityExceptionProfileMatchingPartsSuccessArray]**](UpdateSecurityExceptionProfileMatchingPartsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


