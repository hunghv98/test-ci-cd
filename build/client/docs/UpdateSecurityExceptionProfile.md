# UpdateSecurityExceptionProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**rules** | [**list[UpdateSecurityExceptionProfileRulesArray]**](UpdateSecurityExceptionProfileRulesArray.md) |  | [optional] 
**rules_conditions** | [**list[UpdateSecurityExceptionProfileConditionsArray]**](UpdateSecurityExceptionProfileConditionsArray.md) |  | [optional] 
**rules_matching_parts** | [**list[UpdateSecurityExceptionProfileMatchingPartsArray]**](UpdateSecurityExceptionProfileMatchingPartsArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


