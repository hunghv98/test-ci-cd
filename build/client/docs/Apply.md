# Apply

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reverse_proxies** | [**list[ApplyReverseProxiesArray]**](ApplyReverseProxiesArray.md) |  | [optional] 
**cold_restart** | **bool** |  | [optional] 
**tunnels** | [**list[ApplyTunnelsArray]**](ApplyTunnelsArray.md) |  | [optional] 
**secondary_tunnels** | [**list[ApplySecondaryTunnelsArray]**](ApplySecondaryTunnelsArray.md) |  | [optional] 
**appliances** | [**list[ApplyAppliancesArray]**](ApplyAppliancesArray.md) |  | [optional] 
**network_devices** | [**list[ApplyNetworkDevicesArray]**](ApplyNetworkDevicesArray.md) |  | [optional] 
**ssl_key_uid** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


