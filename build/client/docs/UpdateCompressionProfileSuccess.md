# UpdateCompressionProfileSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**UpdateCompressionProfileSuccessData**](UpdateCompressionProfileSuccessData.md) |  | [optional] 
**data_used_by** | [**list[UpdateCompressionProfileUsedBySuccessArray]**](UpdateCompressionProfileUsedBySuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


