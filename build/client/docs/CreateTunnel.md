# CreateTunnel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**reverse_proxy** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 
**workflow** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**workflow_parameters** | [**list[CreateTunnelWorkflowParametersArray]**](CreateTunnelWorkflowParametersArray.md) |  | [optional] 
**application_template** | **str** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**labels** | [**list[CreateTunnelLabelsArray]**](CreateTunnelLabelsArray.md) |  | [optional] 
**network** | [**CreateTunnelNetwork**](CreateTunnelNetwork.md) |  | [optional] 
**network_incoming_server_alias** | **list[object]** |  | [optional] 
**performance** | [**CreateTunnelPerformance**](CreateTunnelPerformance.md) |  | [optional] 
**logs** | [**CreateTunnelLogs**](CreateTunnelLogs.md) |  | [optional] 
**logs_realtime_syslog_destination_profiles** | [**list[CreateTunnelSyslogDestinationProfilesArray]**](CreateTunnelSyslogDestinationProfilesArray.md) |  | [optional] 
**monitor** | [**CreateTunnelMonitor**](CreateTunnelMonitor.md) |  | [optional] 
**advanced** | [**CreateTunnelAdvanced**](CreateTunnelAdvanced.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


