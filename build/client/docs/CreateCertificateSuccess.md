# CreateCertificateSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**CreateCertificateSuccessData**](CreateCertificateSuccessData.md) |  | [optional] 
**data_labels** | [**list[CreateCertificateLabelsSuccessArray]**](CreateCertificateLabelsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


