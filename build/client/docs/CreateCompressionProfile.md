# CreateCompressionProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**content_types** | **str** |  | [optional] 
**exclude_browser** | **str** |  | [optional] 
**buffer_size** | **int** |  | [optional] 
**compression_level** | **int** |  | [optional] 
**memory_level** | **int** |  | [optional] 
**window_size** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


