# PatchScoringlistSuccessData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**editable** | **bool** |  | [optional] 
**static_scoringlist** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**xss** | **str** |  | [optional] 
**sqli** | **str** |  | [optional] 
**fi** | **str** |  | [optional] 
**cmdi** | **str** |  | [optional] 
**otheri** | **str** |  | [optional] 
**exceptions** | [**list[PatchScoringlistExceptionsSuccessArray]**](PatchScoringlistExceptionsSuccessArray.md) |  | [optional] 
**t_update** | **float** |  | [optional] 
**t_create** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


