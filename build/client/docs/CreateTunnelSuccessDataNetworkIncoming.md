# CreateTunnelSuccessDataNetworkIncoming

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**incoming_type** | **str** |  | [optional] 
**ip** | **str** |  | [optional] 
**port** | **int** |  | [optional] 
**server_name** | **str** |  | [optional] 
**interface** | [**CreateTunnelSuccessDataNetworkIncomingInterface**](CreateTunnelSuccessDataNetworkIncomingInterface.md) |  | [optional] 
**pooler_tunnel** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**http2_enabled** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


