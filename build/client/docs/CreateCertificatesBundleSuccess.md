# CreateCertificatesBundleSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**CreateCertificatesBundleSuccessData**](CreateCertificatesBundleSuccessData.md) |  | [optional] 
**data_ca** | [**list[CreateCertificatesBundleCaSuccessArray]**](CreateCertificatesBundleCaSuccessArray.md) |  | [optional] 
**data_ca_ocsp** | [**list[CreateCertificatesBundleCaOCSPSuccessArray]**](CreateCertificatesBundleCaOCSPSuccessArray.md) |  | [optional] 
**data_crl** | [**list[CreateCertificatesBundleCrlSuccessArray]**](CreateCertificatesBundleCrlSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


