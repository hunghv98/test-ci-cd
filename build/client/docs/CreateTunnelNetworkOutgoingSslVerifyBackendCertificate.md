# CreateTunnelNetworkOutgoingSslVerifyBackendCertificate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bundle** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 
**type** | **str** |  | [optional] 
**depth** | **int** |  | [optional] 
**proxy_check_peer_cn** | **bool** |  | [optional] 
**proxy_check_peer_name** | **bool** |  | [optional] 
**proxy_check_peer_expire** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


