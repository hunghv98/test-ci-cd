# CreateApplianceSuccessDataDns

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enable** | **bool** |  | [optional] 
**dns1** | **str** |  | [optional] 
**dns2** | **str** |  | [optional] 
**dns_ttl_min** | **str** |  | [optional] 
**dns_ttl_max** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


