# GetCACertificatesSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list[GetCACertificatesDataSuccessArray]**](GetCACertificatesDataSuccessArray.md) |  | [optional] 
**data_ca** | [**list[GetCACertificatesCaSuccessArray]**](GetCACertificatesCaSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


