# PatchSecurityExceptionProfileRulesArray

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**description** | **str** |  | [optional] 
**conditions** | [**list[PatchSecurityExceptionProfileConditionsArray]**](PatchSecurityExceptionProfileConditionsArray.md) |  | [optional] 
**matching_parts** | [**list[PatchSecurityExceptionProfileMatchingPartsArray]**](PatchSecurityExceptionProfileMatchingPartsArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


