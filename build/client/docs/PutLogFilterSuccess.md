# PutLogFilterSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**PutLogFilterSuccessData**](PutLogFilterSuccessData.md) |  | [optional] 
**data_used_by** | [**list[PutLogFilterUsedBySuccessArray]**](PutLogFilterUsedBySuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


