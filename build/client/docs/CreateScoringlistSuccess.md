# CreateScoringlistSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**CreateScoringlistSuccessData**](CreateScoringlistSuccessData.md) |  | [optional] 
**data_exceptions** | [**list[CreateScoringlistExceptionsSuccessArray]**](CreateScoringlistExceptionsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


