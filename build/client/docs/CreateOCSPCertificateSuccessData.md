# CreateOCSPCertificateSuccessData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**ca_ocsp** | [**list[CreateOCSPCertificateCaOCSPSuccessArray]**](CreateOCSPCertificateCaOCSPSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


