# CreateLogfilterSuccessData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**read_only** | **bool** |  | [optional] 
**used_by** | [**list[CreateLogfilterUsedBySuccessArray]**](CreateLogfilterUsedBySuccessArray.md) |  | [optional] 
**t_update** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


