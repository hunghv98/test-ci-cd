# CreateSecurityExceptionProfileSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**CreateSecurityExceptionProfileSuccessData**](CreateSecurityExceptionProfileSuccessData.md) |  | [optional] 
**data_rules** | [**list[CreateSecurityExceptionProfileRulesSuccessArray]**](CreateSecurityExceptionProfileRulesSuccessArray.md) |  | [optional] 
**data_rules_conditions** | [**list[CreateSecurityExceptionProfileConditionsSuccessArray]**](CreateSecurityExceptionProfileConditionsSuccessArray.md) |  | [optional] 
**data_rules_matching_parts** | [**list[CreateSecurityExceptionProfileMatchingPartsSuccessArray]**](CreateSecurityExceptionProfileMatchingPartsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


