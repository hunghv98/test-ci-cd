# PatchApplianceSuccessDataSnmp

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enable** | **bool** |  | [optional] 
**interfaces** | [**list[PatchApplianceInterfacesSuccessArray]**](PatchApplianceInterfacesSuccessArray.md) |  | [optional] 
**port** | **str** |  | [optional] 
**sys_location** | **str** |  | [optional] 
**sys_contact** | **str** |  | [optional] 
**community** | **str** |  | [optional] 
**network_filter** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


