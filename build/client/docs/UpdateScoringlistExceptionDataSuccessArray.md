# UpdateScoringlistExceptionDataSuccessArray

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**rule_uid** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**path** | **str** |  | [optional] 
**case_sensitive** | **bool** |  | [optional] 
**negative** | **bool** |  | [optional] 
**t_update** | **float** |  | [optional] 
**t_create** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


