# CreateRequesttimeoutprofile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**header_timeout** | **str** |  | [optional] 
**body_timeout** | **str** |  | [optional] 
**header_rate** | **int** |  | [optional] 
**body_rate** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


