# CreateTunnelNetworkIncomingSsl

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profile** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**certificate** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**sni_vhost_check** | **bool** |  | [optional] 
**sslhsts_enable** | **bool** |  | [optional] 
**verify_client_certificate** | [**CreateTunnelNetworkIncomingSslVerifyClientCertificate**](CreateTunnelNetworkIncomingSslVerifyClientCertificate.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


