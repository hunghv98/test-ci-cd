# CreateCertificate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**password** | **str** |  | [optional] 
**pkcs12** | **file** |  | [optional] 
**key** | **file** |  | [optional] 
**crt** | **file** |  | [optional] 
**chain** | **file** |  | [optional] 
**type** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


