# UpdateNetworkInterfaceUsedBySuccessArray

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tunnel_uid** | **str** |  | [optional] 
**tunnel_name** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


