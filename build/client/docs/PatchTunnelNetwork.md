# PatchTunnelNetwork

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**incoming** | [**PatchTunnelNetworkIncoming**](PatchTunnelNetworkIncoming.md) |  | [optional] 
**outgoing** | [**PatchTunnelNetworkOutgoing**](PatchTunnelNetworkOutgoing.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


