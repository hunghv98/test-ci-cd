# GetAppliancesSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list[GetAppliancesDataSuccessArray]**](GetAppliancesDataSuccessArray.md) |  | [optional] 
**data_ssh_interfaces** | [**list[GetAppliancesInterfacesSuccessArray]**](GetAppliancesInterfacesSuccessArray.md) |  | [optional] 
**data_snmp_interfaces** | [**list[GetAppliancesInterfacesSuccessArray]**](GetAppliancesInterfacesSuccessArray.md) |  | [optional] 
**data_network_devices** | [**list[GetAppliancesNetworkDevicesSuccessArray]**](GetAppliancesNetworkDevicesSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


