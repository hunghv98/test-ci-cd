# GetLogFilterSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list[GetLogFilterDataSuccessArray]**](GetLogFilterDataSuccessArray.md) |  | [optional] 
**data_used_by** | [**list[GetLogFilterUsedBySuccessArray]**](GetLogFilterUsedBySuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


