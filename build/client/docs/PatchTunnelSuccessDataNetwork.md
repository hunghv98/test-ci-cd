# PatchTunnelSuccessDataNetwork

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**incoming** | [**CreateTunnelSuccessDataNetworkIncoming**](CreateTunnelSuccessDataNetworkIncoming.md) |  | [optional] 
**outgoing** | [**PatchTunnelSuccessDataNetworkOutgoing**](PatchTunnelSuccessDataNetworkOutgoing.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


