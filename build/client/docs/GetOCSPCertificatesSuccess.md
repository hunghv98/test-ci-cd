# GetOCSPCertificatesSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list[GetOCSPCertificatesDataSuccessArray]**](GetOCSPCertificatesDataSuccessArray.md) |  | [optional] 
**data_ca_ocsp** | [**list[GetOCSPCertificatesCaOCSPSuccessArray]**](GetOCSPCertificatesCaOCSPSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


