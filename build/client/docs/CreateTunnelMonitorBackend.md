# CreateTunnelMonitorBackend

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **bool** |  | [optional] 
**method** | **str** |  | [optional] 
**url** | **str** |  | [optional] 
**http_host** | **str** |  | [optional] 
**frequency** | **int** |  | [optional] 
**timeout** | **int** |  | [optional] 
**return_code** | **str** |  | [optional] 
**match** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


