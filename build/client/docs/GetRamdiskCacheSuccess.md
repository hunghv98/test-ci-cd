# GetRamdiskCacheSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list[GetRamdiskCacheDataSuccessArray]**](GetRamdiskCacheDataSuccessArray.md) |  | [optional] 
**data_used_by** | [**list[GetRamdiskCacheUsedBySuccessArray]**](GetRamdiskCacheUsedBySuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


