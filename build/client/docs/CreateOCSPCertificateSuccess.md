# CreateOCSPCertificateSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**CreateOCSPCertificateSuccessData**](CreateOCSPCertificateSuccessData.md) |  | [optional] 
**data_ca_ocsp** | [**list[CreateOCSPCertificateCaOCSPSuccessArray]**](CreateOCSPCertificateCaOCSPSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


