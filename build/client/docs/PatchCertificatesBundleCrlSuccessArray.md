# PatchCertificatesBundleCrlSuccessArray

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**expiration** | **str** |  | [optional] 
**issuer** | **str** |  | [optional] 
**next_update** | **str** |  | [optional] 
**enable_auto_update** | **bool** |  | [optional] 
**download_url** | **str** |  | [optional] 
**download_method** | **str** |  | [optional] 
**refresh_cron_time** | **str** |  | [optional] 
**enable** | **bool** |  | [optional] 
**enable_proxy** | **bool** |  | [optional] 
**enable_proxy_authentication** | **bool** |  | [optional] 
**proxy_profile_uid** | **str** |  | [optional] 
**header_host** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


