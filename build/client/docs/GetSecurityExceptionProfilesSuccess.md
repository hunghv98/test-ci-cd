# GetSecurityExceptionProfilesSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list[GetSecurityExceptionProfilesDataSuccessArray]**](GetSecurityExceptionProfilesDataSuccessArray.md) |  | [optional] 
**data_rules** | [**list[GetSecurityExceptionProfilesRulesSuccessArray]**](GetSecurityExceptionProfilesRulesSuccessArray.md) |  | [optional] 
**data_rules_conditions** | [**list[GetSecurityExceptionProfilesConditionsSuccessArray]**](GetSecurityExceptionProfilesConditionsSuccessArray.md) |  | [optional] 
**data_rules_matching_parts** | [**list[GetSecurityExceptionProfilesMatchingPartsSuccessArray]**](GetSecurityExceptionProfilesMatchingPartsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


