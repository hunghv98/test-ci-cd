# CreateTunnelNetworkIncomingSslVerifyClientCertificate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bundle** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**ca** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**type** | **str** |  | [optional] 
**depth** | **int** |  | [optional] 
**ocsp** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**catch_errors** | **bool** |  | [optional] 
**legacy_dn_string_format** | **bool** |  | [optional] 
**ssl_redirect_enable** | **bool** |  | [optional] 
**ssl_redirect_port_in** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


