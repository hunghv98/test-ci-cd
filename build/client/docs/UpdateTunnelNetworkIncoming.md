# UpdateTunnelNetworkIncoming

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**incoming_type** | **str** |  | [optional] 
**ip** | **str** |  | [optional] 
**vip** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 
**interface** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 
**pooler_tunnel** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 
**port** | **int** |  | [optional] 
**server_name** | **str** |  | [optional] 
**server_alias** | **list[object]** |  | [optional] 
**ssl** | [**CreateTunnelNetworkIncomingSsl**](CreateTunnelNetworkIncomingSsl.md) |  | [optional] 
**http2_enabled** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


