# r_s_waf_api_client.DefaultApi

All URIs are relative to *https://ManagementWAF:3001/wafapi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_network_interface**](DefaultApi.md#add_network_interface) | **POST** /networkdevices/interfaces | Create a network interface
[**add_reverse_proxy**](DefaultApi.md#add_reverse_proxy) | **POST** /reverseproxies | Create a reverse proxy
[**add_reverse_proxy_profile**](DefaultApi.md#add_reverse_proxy_profile) | **POST** /reverseproxyprofiles | Create a reverse proxy profile
[**addsslprofiles**](DefaultApi.md#addsslprofiles) | **POST** /sslprofiles | Create SSL Profile
[**apply**](DefaultApi.md#apply) | **POST** /apply | Apply items
[**create_accesslogprofile**](DefaultApi.md#create_accesslogprofile) | **POST** /accesslogprofiles | Create an access log profile
[**create_alerting_destinations**](DefaultApi.md#create_alerting_destinations) | **POST** /alertingdestinations | Create an alerting destination
[**create_appliance**](DefaultApi.md#create_appliance) | **POST** /appliances | Create an appliance
[**create_ca_certificate**](DefaultApi.md#create_ca_certificate) | **POST** /certificatesbundles/ca | Create a CA certificate of the specific bundle
[**create_certificate**](DefaultApi.md#create_certificate) | **POST** /certificates/import | Create a certificate
[**create_certificates_bundle**](DefaultApi.md#create_certificates_bundle) | **POST** /certificatesbundles | Create a certificates bundle
[**create_compression_profile**](DefaultApi.md#create_compression_profile) | **POST** /compressionprofiles | Create a compression profile
[**create_crl_certificate**](DefaultApi.md#create_crl_certificate) | **POST** /certificatesbundles/crl | Create a CRL certificate of the specific bundle
[**create_icx**](DefaultApi.md#create_icx) | **POST** /icx | Create an ICX configuration
[**create_label**](DefaultApi.md#create_label) | **POST** /labels | Create a label
[**create_logfilter**](DefaultApi.md#create_logfilter) | **POST** /logfilters | Create a new Logfilter
[**create_normalization**](DefaultApi.md#create_normalization) | **POST** /normalization | Create a normalization configuration
[**create_ntp**](DefaultApi.md#create_ntp) | **POST** /ntp | Create a ntp
[**create_ocsp_certificate**](DefaultApi.md#create_ocsp_certificate) | **POST** /certificatesbundles/ocsp | Create a OCSP certificate of the specific bundle
[**create_ramdisk_cache**](DefaultApi.md#create_ramdisk_cache) | **POST** /ramdiskcacheprofiles | Create a Ramdisk cache profile
[**create_requesttimeoutprofile**](DefaultApi.md#create_requesttimeoutprofile) | **POST** /requesttimeoutprofiles | Create an request timeout profile
[**create_scoringlist**](DefaultApi.md#create_scoringlist) | **POST** /scoringlist | Create a scoringlist configuration
[**create_scoringlist_exception**](DefaultApi.md#create_scoringlist_exception) | **POST** /scoringlist/exceptions | Create a scoringlist exception
[**create_secondary_tunnel**](DefaultApi.md#create_secondary_tunnel) | **POST** /secondarytunnels | Create a secondary tunnel
[**create_security_exception_profile**](DefaultApi.md#create_security_exception_profile) | **POST** /securityexceptions | Create a Security Exception Profile
[**create_sysctlprofile**](DefaultApi.md#create_sysctlprofile) | **POST** /sysctl | Create a Sysctl profile
[**create_tunnel**](DefaultApi.md#create_tunnel) | **POST** /tunnels | Create a tunnel
[**del_accesslogprofile**](DefaultApi.md#del_accesslogprofile) | **DELETE** /accesslogprofiles | Delete an access log profile
[**del_alerting_destination**](DefaultApi.md#del_alerting_destination) | **DELETE** /alertingdestinations | Delete an alerting destination
[**del_appliance**](DefaultApi.md#del_appliance) | **DELETE** /appliances | Delete an appliance
[**del_ca_certificate_bundle**](DefaultApi.md#del_ca_certificate_bundle) | **DELETE** /certificatesbundles/ca | Delete a CA certificates
[**del_certificates_bundle**](DefaultApi.md#del_certificates_bundle) | **DELETE** /certificatesbundles | Delete a certificates bundle
[**del_compression_profile**](DefaultApi.md#del_compression_profile) | **DELETE** /compressionprofiles | Delete a compression profile
[**del_crl_certificate_bundle**](DefaultApi.md#del_crl_certificate_bundle) | **DELETE** /certificatesbundles/crl | Delete a CRL certificates
[**del_icx**](DefaultApi.md#del_icx) | **DELETE** /icx | Delete an ICX configuration
[**del_label**](DefaultApi.md#del_label) | **DELETE** /labels | Delete a label
[**del_normalization**](DefaultApi.md#del_normalization) | **DELETE** /normalization | Delete a normalization configuration
[**del_ntp**](DefaultApi.md#del_ntp) | **DELETE** /ntp | Delete a ntp
[**del_ocsp_certificate_bundle**](DefaultApi.md#del_ocsp_certificate_bundle) | **DELETE** /certificatesbundles/ocsp | Delete a OCSP certificates
[**del_ramdisk_cache**](DefaultApi.md#del_ramdisk_cache) | **DELETE** /ramdiskcacheprofiles | Delete a Ramdisk cache
[**del_requesttimeoutprofile**](DefaultApi.md#del_requesttimeoutprofile) | **DELETE** /requesttimeoutprofiles | Delete an request timeout profile
[**del_scoringlist**](DefaultApi.md#del_scoringlist) | **DELETE** /scoringlist | Delete a scoringlist configuration
[**del_scoringlist_exception**](DefaultApi.md#del_scoringlist_exception) | **DELETE** /scoringlist/exceptions | Delete a scoringlist exception
[**del_secondadry_tunnel**](DefaultApi.md#del_secondadry_tunnel) | **DELETE** /secondarytunnels | Delete a secondary tunnel
[**del_security_exception_profile**](DefaultApi.md#del_security_exception_profile) | **DELETE** /securityexceptions | Delete a security exception profile
[**del_sysctlprofile**](DefaultApi.md#del_sysctlprofile) | **DELETE** /sysctl | Delete a Sysctl profile
[**del_tunnel**](DefaultApi.md#del_tunnel) | **DELETE** /tunnels | Delete a tunnel
[**delete_certificate**](DefaultApi.md#delete_certificate) | **DELETE** /certificates | Delete a certificate
[**delete_networ_interface**](DefaultApi.md#delete_networ_interface) | **DELETE** /networkdevices/interfaces | Delete a network interface
[**delete_reverse_proxies**](DefaultApi.md#delete_reverse_proxies) | **DELETE** /reverseproxies | Delete a reverse proxy
[**delete_reverse_proxy_profile**](DefaultApi.md#delete_reverse_proxy_profile) | **DELETE** /reverseproxyprofiles | Delete a reverse proxy profile
[**deletesslprofiles**](DefaultApi.md#deletesslprofiles) | **DELETE** /sslprofiles | Delete a SSL Profile
[**download_ca_certificate**](DefaultApi.md#download_ca_certificate) | **GET** /certificatesbundles/ca/export | Download CA Certificates File
[**download_caocsp_certificate**](DefaultApi.md#download_caocsp_certificate) | **GET** /certificatesbundles/ocsp/export | Download CAOCSP Certificates File
[**download_certificate**](DefaultApi.md#download_certificate) | **GET** /certificates/export | Download certificate File
[**download_crl_certificate**](DefaultApi.md#download_crl_certificate) | **GET** /certificatesbundles/crl/export | Download CRL Certificates File
[**download_log_filter**](DefaultApi.md#download_log_filter) | **GET** /logfilters/export | Download LogFilter File
[**file_upload**](DefaultApi.md#file_upload) | **POST** /files | Send a file
[**get_access_log_profiles**](DefaultApi.md#get_access_log_profiles) | **GET** /accesslogprofiles | Get access log profiles
[**get_alertingdestinations**](DefaultApi.md#get_alertingdestinations) | **GET** /alertingdestinations | Get alerting destinations
[**get_appliances**](DefaultApi.md#get_appliances) | **GET** /appliances | Get appliances
[**get_ca_certificates**](DefaultApi.md#get_ca_certificates) | **GET** /certificatesbundles/ca | Get CA certificates of bundle
[**get_certificates**](DefaultApi.md#get_certificates) | **GET** /certificates | Get certificates
[**get_certificates_bundles**](DefaultApi.md#get_certificates_bundles) | **GET** /certificatesbundles | Get certificates bundles
[**get_compression_profiles**](DefaultApi.md#get_compression_profiles) | **GET** /compressionprofiles | Get compression profiles
[**get_crl_certificates**](DefaultApi.md#get_crl_certificates) | **GET** /certificatesbundles/crl | Get CRL certificates of bundle
[**get_icx**](DefaultApi.md#get_icx) | **GET** /icx | Get ICX configurations
[**get_icx_templates**](DefaultApi.md#get_icx_templates) | **GET** /icxtemplates | Get ICX templates
[**get_labels**](DefaultApi.md#get_labels) | **GET** /labels | Get labels
[**get_licenses**](DefaultApi.md#get_licenses) | **GET** /licenses | Get licenses
[**get_log_filter**](DefaultApi.md#get_log_filter) | **GET** /logfilters | Get LogFilters
[**get_network_devices**](DefaultApi.md#get_network_devices) | **GET** /networkdevices | Get network devices
[**get_network_interfaces**](DefaultApi.md#get_network_interfaces) | **GET** /networkdevices/interfaces | Get network interfaces
[**get_normalization**](DefaultApi.md#get_normalization) | **GET** /normalization | Get normalization configurations
[**get_ntps**](DefaultApi.md#get_ntps) | **GET** /ntp | Get ntps
[**get_ocsp_certificates**](DefaultApi.md#get_ocsp_certificates) | **GET** /certificatesbundles/ocsp | Get OCSP certificates of bundle
[**get_ramdisk_cache**](DefaultApi.md#get_ramdisk_cache) | **GET** /ramdiskcacheprofiles | Get Ramdisk cache profile
[**get_requesttimeoutprofiles**](DefaultApi.md#get_requesttimeoutprofiles) | **GET** /requesttimeoutprofiles | Get request timeout profiles
[**get_reverse_proxies**](DefaultApi.md#get_reverse_proxies) | **GET** /reverseproxies | Get reverse proxies
[**get_reverse_proxy_profiles**](DefaultApi.md#get_reverse_proxy_profiles) | **GET** /reverseproxyprofiles | Get reverse proxy profiles
[**get_scoringlist_exceptions**](DefaultApi.md#get_scoringlist_exceptions) | **GET** /scoringlist/exceptions | Get scoringlist exceptions
[**get_scoringlist_templates**](DefaultApi.md#get_scoringlist_templates) | **GET** /scoringlist/templates | Get scoringlist templates
[**get_scoringlists**](DefaultApi.md#get_scoringlists) | **GET** /scoringlist | Get scoringlist configurations
[**get_secondary_tunnels**](DefaultApi.md#get_secondary_tunnels) | **GET** /secondarytunnels | Get secondary tunnels
[**get_security_exception_profiles**](DefaultApi.md#get_security_exception_profiles) | **GET** /securityexceptions | Get Security Exception Profiles
[**get_sslprofiles**](DefaultApi.md#get_sslprofiles) | **GET** /sslprofiles | Get SSL Profiles
[**get_static_scoringlists**](DefaultApi.md#get_static_scoringlists) | **GET** /scoringlist/statics | Get static scoringlists
[**get_sysctlprofiles**](DefaultApi.md#get_sysctlprofiles) | **GET** /sysctl | Get Sysctl profiles
[**get_tunnels**](DefaultApi.md#get_tunnels) | **GET** /tunnels | Get tunnels
[**getnormalizationtemplate**](DefaultApi.md#getnormalizationtemplate) | **GET** /normalization/templates | Get normalization templates
[**patch_accesslogprofile**](DefaultApi.md#patch_accesslogprofile) | **PATCH** /accesslogprofiles | Patch an access log profile
[**patch_alerting_destination**](DefaultApi.md#patch_alerting_destination) | **PATCH** /alertingdestinations | Patch an alerting destination
[**patch_appliance**](DefaultApi.md#patch_appliance) | **PATCH** /appliances | Patch an appliance
[**patch_certificate**](DefaultApi.md#patch_certificate) | **PATCH** /certificates/import | Patch a certificate
[**patch_certificates_bundle**](DefaultApi.md#patch_certificates_bundle) | **PATCH** /certificatesbundles | Patch a certificates bundle
[**patch_compression_profile**](DefaultApi.md#patch_compression_profile) | **PATCH** /compressionprofiles | Patch a compression profile
[**patch_icx**](DefaultApi.md#patch_icx) | **PATCH** /icx | Patch an ICX configurattion
[**patch_label**](DefaultApi.md#patch_label) | **PATCH** /labels | Patch a label
[**patch_network_interface**](DefaultApi.md#patch_network_interface) | **PATCH** /networkdevices/interfaces | Patch a network interface
[**patch_normalization**](DefaultApi.md#patch_normalization) | **PATCH** /normalization | Patch a normalization configurattion
[**patch_ntp**](DefaultApi.md#patch_ntp) | **PATCH** /ntp | Patch a ntp
[**patch_ramdisk_cache**](DefaultApi.md#patch_ramdisk_cache) | **PATCH** /ramdiskcacheprofiles | Patch a Ramdisk cache profile
[**patch_requesttimeoutprofile**](DefaultApi.md#patch_requesttimeoutprofile) | **PATCH** /requesttimeoutprofiles | Patch an request timeout profile
[**patch_reverse_proxy**](DefaultApi.md#patch_reverse_proxy) | **PATCH** /reverseproxies | Patch a reverse proxy
[**patch_reverse_proxy_profile**](DefaultApi.md#patch_reverse_proxy_profile) | **PATCH** /reverseproxyprofiles | Patch a reverse proxy profile
[**patch_scoringlist**](DefaultApi.md#patch_scoringlist) | **PATCH** /scoringlist | Patch a scoringlist configuration
[**patch_scoringlist_exception**](DefaultApi.md#patch_scoringlist_exception) | **PATCH** /scoringlist/exceptions | Patch a scoringlist exception
[**patch_secondary_tunnel**](DefaultApi.md#patch_secondary_tunnel) | **PATCH** /secondarytunnels | Patch a secondary tunnel
[**patch_security_exception_profile**](DefaultApi.md#patch_security_exception_profile) | **PATCH** /securityexceptions | Patch a Security Exception Profile
[**patch_sysctlprofile**](DefaultApi.md#patch_sysctlprofile) | **PATCH** /sysctl | Patch a Sysctl profile
[**patch_tunnel**](DefaultApi.md#patch_tunnel) | **PATCH** /tunnels | Patch a tunnel
[**put_log_filter**](DefaultApi.md#put_log_filter) | **PUT** /logfilters/import | Upload LogFilter File
[**setsslprofiles**](DefaultApi.md#setsslprofiles) | **PUT** /sslprofiles | Update a SSL Profile
[**shutdown**](DefaultApi.md#shutdown) | **POST** /appliances/shutdown | Shutdown/Halt the system
[**tunnel_backend_status**](DefaultApi.md#tunnel_backend_status) | **GET** /status/tunnels/backend | Get backend status
[**tunnel_listening_status**](DefaultApi.md#tunnel_listening_status) | **GET** /status/tunnels/listening | Get listening status
[**tunnel_runtime_information**](DefaultApi.md#tunnel_runtime_information) | **GET** /status/tunnels/runtime | Get runtime information
[**update_accesslogprofile**](DefaultApi.md#update_accesslogprofile) | **PUT** /accesslogprofiles | Update an access log profile
[**update_certificate**](DefaultApi.md#update_certificate) | **PUT** /certificates/import | Update a certificate
[**update_certificates_bundle**](DefaultApi.md#update_certificates_bundle) | **PUT** /certificatesbundles | Update a certificates bundle
[**update_compression_profile**](DefaultApi.md#update_compression_profile) | **PUT** /compressionprofiles | Update a compression profile
[**update_icx**](DefaultApi.md#update_icx) | **PUT** /icx | Update an ICX configuration
[**update_label**](DefaultApi.md#update_label) | **PUT** /labels | Update a label
[**update_network_interface**](DefaultApi.md#update_network_interface) | **PUT** /networkdevices/interfaces | Update a network interface
[**update_ntp**](DefaultApi.md#update_ntp) | **PUT** /ntp | Update a ntp
[**update_ramdisk_cache**](DefaultApi.md#update_ramdisk_cache) | **PUT** /ramdiskcacheprofiles | Update a Ramdisk cache profile
[**update_requesttimeoutprofile**](DefaultApi.md#update_requesttimeoutprofile) | **PUT** /requesttimeoutprofiles | Update an request timeout profile
[**update_reverse_proxy**](DefaultApi.md#update_reverse_proxy) | **PUT** /reverseproxies | Update a reverse proxy
[**update_reverse_proxy_profile**](DefaultApi.md#update_reverse_proxy_profile) | **PUT** /reverseproxyprofiles | Update a reverse proxy profile
[**update_scoringlist**](DefaultApi.md#update_scoringlist) | **PUT** /scoringlist | Update a scoringlist configuration
[**update_scoringlist_exception**](DefaultApi.md#update_scoringlist_exception) | **PUT** /scoringlist/exceptions | Update a scoringlist exception
[**update_security_exception_profile**](DefaultApi.md#update_security_exception_profile) | **PUT** /securityexceptions | Update a Security Exception Profile
[**update_sysctlprofile**](DefaultApi.md#update_sysctlprofile) | **PUT** /sysctl | Update a Sysctl profile
[**update_tunnel**](DefaultApi.md#update_tunnel) | **PUT** /tunnels | Update a tunnel
[**upload_license**](DefaultApi.md#upload_license) | **POST** /licenses | Upload a license file


# **add_network_interface**
> AddNetworkInterfaceSuccess add_network_interface(add_network_interface)

Create a network interface

Create a new network interface

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    add_network_interface = r_s_waf_api_client.AddNetworkInterface() # AddNetworkInterface | 

    try:
        # Create a network interface
        api_response = api_instance.add_network_interface(add_network_interface)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->add_network_interface: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **add_network_interface** | [**AddNetworkInterface**](AddNetworkInterface.md)|  | 

### Return type

[**AddNetworkInterfaceSuccess**](AddNetworkInterfaceSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object the matching network interface. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_reverse_proxy**
> AddReverseProxySuccess add_reverse_proxy(add_reverse_proxy)

Create a reverse proxy

Create a new reverse proxy

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    add_reverse_proxy = r_s_waf_api_client.AddReverseProxy() # AddReverseProxy | 

    try:
        # Create a reverse proxy
        api_response = api_instance.add_reverse_proxy(add_reverse_proxy)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->add_reverse_proxy: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **add_reverse_proxy** | [**AddReverseProxy**](AddReverseProxy.md)|  | 

### Return type

[**AddReverseProxySuccess**](AddReverseProxySuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_reverse_proxy_profile**
> AddReverseProxyProfileSuccess add_reverse_proxy_profile(add_reverse_proxy_profile)

Create a reverse proxy profile

Create a new reverse proxy profile

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    add_reverse_proxy_profile = r_s_waf_api_client.AddReverseProxyProfile() # AddReverseProxyProfile | 

    try:
        # Create a reverse proxy profile
        api_response = api_instance.add_reverse_proxy_profile(add_reverse_proxy_profile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->add_reverse_proxy_profile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **add_reverse_proxy_profile** | [**AddReverseProxyProfile**](AddReverseProxyProfile.md)|  | 

### Return type

[**AddReverseProxyProfileSuccess**](AddReverseProxyProfileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing matching reverse proxy profiles. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **addsslprofiles**
> AddsslprofilesSuccess addsslprofiles(addsslprofiles)

Create SSL Profile

Create a new SSL Profile

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    addsslprofiles = r_s_waf_api_client.Addsslprofiles() # Addsslprofiles | 

    try:
        # Create SSL Profile
        api_response = api_instance.addsslprofiles(addsslprofiles)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->addsslprofiles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **addsslprofiles** | [**Addsslprofiles**](Addsslprofiles.md)|  | 

### Return type

[**AddsslprofilesSuccess**](AddsslprofilesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing matching sslprofiles. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **apply**
> ApplySuccess apply(apply)

Apply items

While applying a tunnel, parent reverse proxy is automatically applied (but it don't apply other child tunnels of parent reverseproxy).

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    apply = r_s_waf_api_client.Apply() # Apply | 

    try:
        # Apply items
        api_response = api_instance.apply(apply)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->apply: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apply** | [**Apply**](Apply.md)|  | 

### Return type

[**ApplySuccess**](ApplySuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_accesslogprofile**
> CreateAccesslogprofileSuccess create_accesslogprofile(create_accesslogprofile)

Create an access log profile

Create a new access log profile

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_accesslogprofile = r_s_waf_api_client.CreateAccesslogprofile() # CreateAccesslogprofile | 

    try:
        # Create an access log profile
        api_response = api_instance.create_accesslogprofile(create_accesslogprofile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_accesslogprofile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_accesslogprofile** | [**CreateAccesslogprofile**](CreateAccesslogprofile.md)|  | 

### Return type

[**CreateAccesslogprofileSuccess**](CreateAccesslogprofileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching access log profiles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_alerting_destinations**
> CreateAlertingDestinationsSuccess create_alerting_destinations(create_alerting_destinations)

Create an alerting destination

Create a new alerting destination

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_alerting_destinations = r_s_waf_api_client.CreateAlertingDestinations() # CreateAlertingDestinations | 

    try:
        # Create an alerting destination
        api_response = api_instance.create_alerting_destinations(create_alerting_destinations)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_alerting_destinations: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_alerting_destinations** | [**CreateAlertingDestinations**](CreateAlertingDestinations.md)|  | 

### Return type

[**CreateAlertingDestinationsSuccess**](CreateAlertingDestinationsSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching alerting destinations |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_appliance**
> CreateApplianceSuccess create_appliance(create_appliance)

Create an appliance

Create a new appliance

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_appliance = r_s_waf_api_client.CreateAppliance() # CreateAppliance | 

    try:
        # Create an appliance
        api_response = api_instance.create_appliance(create_appliance)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_appliance: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_appliance** | [**CreateAppliance**](CreateAppliance.md)|  | 

### Return type

[**CreateApplianceSuccess**](CreateApplianceSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching appliances. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_ca_certificate**
> CreateCACertificateSuccess create_ca_certificate(content_md5=content_md5, name=name, bundle_uid=bundle_uid, upload=upload)

Create a CA certificate of the specific bundle

Create the CA certificate.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    content_md5 = 'content_md5_example' # str | The md5sum of uploaded file. To ensure that data is not corrupted traversing the network, user may use the header.                          <br />If you use cURL, specify <code>-H \"Content-MD5: b063ea6ddd4af4e20ab27e7fb8e62079\"</code>. (optional)
name = 'name_example' # str |  (optional)
bundle_uid = 'bundle_uid_example' # str |  (optional)
upload = '/path/to/file' # file |  (optional)

    try:
        # Create a CA certificate of the specific bundle
        api_response = api_instance.create_ca_certificate(content_md5=content_md5, name=name, bundle_uid=bundle_uid, upload=upload)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_ca_certificate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **content_md5** | **str**| The md5sum of uploaded file. To ensure that data is not corrupted traversing the network, user may use the header.                          &lt;br /&gt;If you use cURL, specify &lt;code&gt;-H \&quot;Content-MD5: b063ea6ddd4af4e20ab27e7fb8e62079\&quot;&lt;/code&gt;. | [optional] 
 **name** | **str**|  | [optional] 
 **bundle_uid** | **str**|  | [optional] 
 **upload** | **file**|  | [optional] 

### Return type

[**CreateCACertificateSuccess**](CreateCACertificateSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching certificates bundles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_certificate**
> CreateCertificateSuccess create_certificate(content_md5=content_md5, name=name, password=password, pkcs12=pkcs12, key=key, crt=crt, chain=chain, type=type)

Create a certificate

Create the certificate.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    content_md5 = 'content_md5_example' # str | The md5sum of uploaded file. To ensure that data is not corrupted traversing the network, user may use the header.                          <br />If you use cURL, specify <code>-H \"Content-MD5: b063ea6ddd4af4e20ab27e7fb8e62079\"</code>. (optional)
name = 'name_example' # str |  (optional)
password = 'password_example' # str |  (optional)
pkcs12 = '/path/to/file' # file |  (optional)
key = '/path/to/file' # file |  (optional)
crt = '/path/to/file' # file |  (optional)
chain = '/path/to/file' # file |  (optional)
type = 'type_example' # str |  (optional)

    try:
        # Create a certificate
        api_response = api_instance.create_certificate(content_md5=content_md5, name=name, password=password, pkcs12=pkcs12, key=key, crt=crt, chain=chain, type=type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_certificate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **content_md5** | **str**| The md5sum of uploaded file. To ensure that data is not corrupted traversing the network, user may use the header.                          &lt;br /&gt;If you use cURL, specify &lt;code&gt;-H \&quot;Content-MD5: b063ea6ddd4af4e20ab27e7fb8e62079\&quot;&lt;/code&gt;. | [optional] 
 **name** | **str**|  | [optional] 
 **password** | **str**|  | [optional] 
 **pkcs12** | **file**|  | [optional] 
 **key** | **file**|  | [optional] 
 **crt** | **file**|  | [optional] 
 **chain** | **file**|  | [optional] 
 **type** | **str**|  | [optional] 

### Return type

[**CreateCertificateSuccess**](CreateCertificateSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing new certificates. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_certificates_bundle**
> CreateCertificatesBundleSuccess create_certificates_bundle(create_certificates_bundle)

Create a certificates bundle

Create a new certificates bundle

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_certificates_bundle = r_s_waf_api_client.CreateCertificatesBundle() # CreateCertificatesBundle | 

    try:
        # Create a certificates bundle
        api_response = api_instance.create_certificates_bundle(create_certificates_bundle)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_certificates_bundle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_certificates_bundle** | [**CreateCertificatesBundle**](CreateCertificatesBundle.md)|  | 

### Return type

[**CreateCertificatesBundleSuccess**](CreateCertificatesBundleSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching certificates bundles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_compression_profile**
> CreateCompressionProfileSuccess create_compression_profile(create_compression_profile)

Create a compression profile

Create a new compression profile

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_compression_profile = r_s_waf_api_client.CreateCompressionProfile() # CreateCompressionProfile | 

    try:
        # Create a compression profile
        api_response = api_instance.create_compression_profile(create_compression_profile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_compression_profile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_compression_profile** | [**CreateCompressionProfile**](CreateCompressionProfile.md)|  | 

### Return type

[**CreateCompressionProfileSuccess**](CreateCompressionProfileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching compression profiles. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_crl_certificate**
> CreateCRLCertificateSuccess create_crl_certificate(content_md5=content_md5, name=name, bundle_uid=bundle_uid, upload=upload)

Create a CRL certificate of the specific bundle

Create the CRL certificate.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    content_md5 = 'content_md5_example' # str | The md5sum of uploaded file. To ensure that data is not corrupted traversing the network, user may use the header.                          <br />If you use cURL, specify <code>-H \"Content-MD5: b063ea6ddd4af4e20ab27e7fb8e62079\"</code>. (optional)
name = 'name_example' # str |  (optional)
bundle_uid = 'bundle_uid_example' # str |  (optional)
upload = '/path/to/file' # file |  (optional)

    try:
        # Create a CRL certificate of the specific bundle
        api_response = api_instance.create_crl_certificate(content_md5=content_md5, name=name, bundle_uid=bundle_uid, upload=upload)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_crl_certificate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **content_md5** | **str**| The md5sum of uploaded file. To ensure that data is not corrupted traversing the network, user may use the header.                          &lt;br /&gt;If you use cURL, specify &lt;code&gt;-H \&quot;Content-MD5: b063ea6ddd4af4e20ab27e7fb8e62079\&quot;&lt;/code&gt;. | [optional] 
 **name** | **str**|  | [optional] 
 **bundle_uid** | **str**|  | [optional] 
 **upload** | **file**|  | [optional] 

### Return type

[**CreateCRLCertificateSuccess**](CreateCRLCertificateSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching certificates bundles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_icx**
> CreateIcxSuccess create_icx(create_icx)

Create an ICX configuration

Create a new ICX configuration

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_icx = r_s_waf_api_client.CreateIcx() # CreateIcx | 

    try:
        # Create an ICX configuration
        api_response = api_instance.create_icx(create_icx)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_icx: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_icx** | [**CreateIcx**](CreateIcx.md)|  | 

### Return type

[**CreateIcxSuccess**](CreateIcxSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching ICX configurations |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_label**
> CreateLabelSuccess create_label(create_label)

Create a label

Create a new label

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_label = r_s_waf_api_client.CreateLabel() # CreateLabel | 

    try:
        # Create a label
        api_response = api_instance.create_label(create_label)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_label: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_label** | [**CreateLabel**](CreateLabel.md)|  | 

### Return type

[**CreateLabelSuccess**](CreateLabelSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching labels |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_logfilter**
> CreateLogfilterSuccess create_logfilter(content_md5=content_md5, name=name, description=description, upload=upload)

Create a new Logfilter

Create the logfilter.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    content_md5 = 'content_md5_example' # str | The md5sum of uploaded file. To ensure that data is not corrupted traversing the network, user may use the header.                          <br />If you use cURL, specify <code>-H \"Content-MD5: b063ea6ddd4af4e20ab27e7fb8e62079\"</code>. (optional)
name = 'name_example' # str |  (optional)
description = 'description_example' # str |  (optional)
upload = '/path/to/file' # file |  (optional)

    try:
        # Create a new Logfilter
        api_response = api_instance.create_logfilter(content_md5=content_md5, name=name, description=description, upload=upload)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_logfilter: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **content_md5** | **str**| The md5sum of uploaded file. To ensure that data is not corrupted traversing the network, user may use the header.                          &lt;br /&gt;If you use cURL, specify &lt;code&gt;-H \&quot;Content-MD5: b063ea6ddd4af4e20ab27e7fb8e62079\&quot;&lt;/code&gt;. | [optional] 
 **name** | **str**|  | [optional] 
 **description** | **str**|  | [optional] 
 **upload** | **file**|  | [optional] 

### Return type

[**CreateLogfilterSuccess**](CreateLogfilterSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The created logfilter |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_normalization**
> CreateNormalizationSuccess create_normalization(create_normalization)

Create a normalization configuration

Create a new normalization configuration

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_normalization = r_s_waf_api_client.CreateNormalization() # CreateNormalization | 

    try:
        # Create a normalization configuration
        api_response = api_instance.create_normalization(create_normalization)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_normalization: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_normalization** | [**CreateNormalization**](CreateNormalization.md)|  | 

### Return type

[**CreateNormalizationSuccess**](CreateNormalizationSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching normalization configurations |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_ntp**
> CreateNtpSuccess create_ntp(create_ntp)

Create a ntp

Create a new ntp

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_ntp = r_s_waf_api_client.CreateNtp() # CreateNtp | 

    try:
        # Create a ntp
        api_response = api_instance.create_ntp(create_ntp)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_ntp: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_ntp** | [**CreateNtp**](CreateNtp.md)|  | 

### Return type

[**CreateNtpSuccess**](CreateNtpSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching ntps |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_ocsp_certificate**
> CreateOCSPCertificateSuccess create_ocsp_certificate(content_md5=content_md5, name=name, bundle_uid=bundle_uid, upload=upload)

Create a OCSP certificate of the specific bundle

Create the OCSP certificate.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    content_md5 = 'content_md5_example' # str | The md5sum of uploaded file. To ensure that data is not corrupted traversing the network, user may use the header.                          <br />If you use cURL, specify <code>-H \"Content-MD5: b063ea6ddd4af4e20ab27e7fb8e62079\"</code>. (optional)
name = 'name_example' # str |  (optional)
bundle_uid = 'bundle_uid_example' # str |  (optional)
upload = '/path/to/file' # file |  (optional)

    try:
        # Create a OCSP certificate of the specific bundle
        api_response = api_instance.create_ocsp_certificate(content_md5=content_md5, name=name, bundle_uid=bundle_uid, upload=upload)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_ocsp_certificate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **content_md5** | **str**| The md5sum of uploaded file. To ensure that data is not corrupted traversing the network, user may use the header.                          &lt;br /&gt;If you use cURL, specify &lt;code&gt;-H \&quot;Content-MD5: b063ea6ddd4af4e20ab27e7fb8e62079\&quot;&lt;/code&gt;. | [optional] 
 **name** | **str**|  | [optional] 
 **bundle_uid** | **str**|  | [optional] 
 **upload** | **file**|  | [optional] 

### Return type

[**CreateOCSPCertificateSuccess**](CreateOCSPCertificateSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching certificates bundles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_ramdisk_cache**
> CreateRamdiskCacheSuccess create_ramdisk_cache(create_ramdisk_cache)

Create a Ramdisk cache profile

Create a Ramdisk cache profile

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_ramdisk_cache = r_s_waf_api_client.CreateRamdiskCache() # CreateRamdiskCache | 

    try:
        # Create a Ramdisk cache profile
        api_response = api_instance.create_ramdisk_cache(create_ramdisk_cache)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_ramdisk_cache: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_ramdisk_cache** | [**CreateRamdiskCache**](CreateRamdiskCache.md)|  | 

### Return type

[**CreateRamdiskCacheSuccess**](CreateRamdiskCacheSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching Ramdisk cache profile. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_requesttimeoutprofile**
> CreateRequesttimeoutprofileSuccess create_requesttimeoutprofile(create_requesttimeoutprofile)

Create an request timeout profile

Create a new request timeout profile

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_requesttimeoutprofile = r_s_waf_api_client.CreateRequesttimeoutprofile() # CreateRequesttimeoutprofile | 

    try:
        # Create an request timeout profile
        api_response = api_instance.create_requesttimeoutprofile(create_requesttimeoutprofile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_requesttimeoutprofile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_requesttimeoutprofile** | [**CreateRequesttimeoutprofile**](CreateRequesttimeoutprofile.md)|  | 

### Return type

[**CreateRequesttimeoutprofileSuccess**](CreateRequesttimeoutprofileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching request timeout profiles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_scoringlist**
> CreateScoringlistSuccess create_scoringlist(create_scoringlist)

Create a scoringlist configuration

Create a new scoringlist configuration

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_scoringlist = r_s_waf_api_client.CreateScoringlist() # CreateScoringlist | 

    try:
        # Create a scoringlist configuration
        api_response = api_instance.create_scoringlist(create_scoringlist)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_scoringlist: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_scoringlist** | [**CreateScoringlist**](CreateScoringlist.md)|  | 

### Return type

[**CreateScoringlistSuccess**](CreateScoringlistSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching scoringlist configuration |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_scoringlist_exception**
> CreateScoringlistExceptionSuccess create_scoringlist_exception(create_scoringlist_exception)

Create a scoringlist exception

Create a new scoringlist exception

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_scoringlist_exception = r_s_waf_api_client.CreateScoringlistException() # CreateScoringlistException | 

    try:
        # Create a scoringlist exception
        api_response = api_instance.create_scoringlist_exception(create_scoringlist_exception)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_scoringlist_exception: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_scoringlist_exception** | [**CreateScoringlistException**](CreateScoringlistException.md)|  | 

### Return type

[**CreateScoringlistExceptionSuccess**](CreateScoringlistExceptionSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The matching scoringlist exceptions |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_secondary_tunnel**
> CreateSecondaryTunnelSuccess create_secondary_tunnel(create_secondary_tunnel)

Create a secondary tunnel

Create a new secondary tunnel

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_secondary_tunnel = r_s_waf_api_client.CreateSecondaryTunnel() # CreateSecondaryTunnel | 

    try:
        # Create a secondary tunnel
        api_response = api_instance.create_secondary_tunnel(create_secondary_tunnel)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_secondary_tunnel: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_secondary_tunnel** | [**CreateSecondaryTunnel**](CreateSecondaryTunnel.md)|  | 

### Return type

[**CreateSecondaryTunnelSuccess**](CreateSecondaryTunnelSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching secondary tunnels |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_security_exception_profile**
> CreateSecurityExceptionProfileSuccess create_security_exception_profile(create_security_exception_profile)

Create a Security Exception Profile

Create a security exception profile.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_security_exception_profile = r_s_waf_api_client.CreateSecurityExceptionProfile() # CreateSecurityExceptionProfile | 

    try:
        # Create a Security Exception Profile
        api_response = api_instance.create_security_exception_profile(create_security_exception_profile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_security_exception_profile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_security_exception_profile** | [**CreateSecurityExceptionProfile**](CreateSecurityExceptionProfile.md)|  | 

### Return type

[**CreateSecurityExceptionProfileSuccess**](CreateSecurityExceptionProfileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The created security exception profile data |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_sysctlprofile**
> CreateSysctlprofileSuccess create_sysctlprofile(create_sysctlprofile)

Create a Sysctl profile

Create a new Sysctl profile<br> This item lets you manage profiles for configuring the Linux kernel TCP/IP stack. More details on sysctl parameters can be found on the Linux kernel documentation.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_sysctlprofile = r_s_waf_api_client.CreateSysctlprofile() # CreateSysctlprofile | 

    try:
        # Create a Sysctl profile
        api_response = api_instance.create_sysctlprofile(create_sysctlprofile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_sysctlprofile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_sysctlprofile** | [**CreateSysctlprofile**](CreateSysctlprofile.md)|  | 

### Return type

[**CreateSysctlprofileSuccess**](CreateSysctlprofileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching Sysctl profiles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_tunnel**
> CreateTunnelSuccess create_tunnel(create_tunnel)

Create a tunnel

Create a new tunnel

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    create_tunnel = r_s_waf_api_client.CreateTunnel() # CreateTunnel | 

    try:
        # Create a tunnel
        api_response = api_instance.create_tunnel(create_tunnel)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->create_tunnel: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_tunnel** | [**CreateTunnel**](CreateTunnel.md)|  | 

### Return type

[**CreateTunnelSuccess**](CreateTunnelSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching tunnels |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_accesslogprofile**
> DelAccesslogprofileSuccess del_accesslogprofile(uid=uid, name=name)

Delete an access log profile

Delete an access log profile. You must specify an access log profile uid or an profile name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Accesslog Profile uid. (optional)
name = 'name_example' # str | Accesslog Profile name. (optional)

    try:
        # Delete an access log profile
        api_response = api_instance.del_accesslogprofile(uid=uid, name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_accesslogprofile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Accesslog Profile uid. | [optional] 
 **name** | **str**| Accesslog Profile name. | [optional] 

### Return type

[**DelAccesslogprofileSuccess**](DelAccesslogprofileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_alerting_destination**
> DelAlertingDestinationSuccess del_alerting_destination(uid=uid, name=name)

Delete an alerting destination

Delete an alerting destination. You must specify an alerting destination uid or an alerting destination name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Alerting destination uid. (optional)
name = 'name_example' # str | Alerting destination name. (optional)

    try:
        # Delete an alerting destination
        api_response = api_instance.del_alerting_destination(uid=uid, name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_alerting_destination: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Alerting destination uid. | [optional] 
 **name** | **str**| Alerting destination name. | [optional] 

### Return type

[**DelAlertingDestinationSuccess**](DelAlertingDestinationSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_appliance**
> DelApplianceSuccess del_appliance(uid)

Delete an appliance

Delete an appliance. You must specify an appliance uid or an appliance name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Appliance uid.

    try:
        # Delete an appliance
        api_response = api_instance.del_appliance(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_appliance: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Appliance uid. | 

### Return type

[**DelApplianceSuccess**](DelApplianceSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_ca_certificate_bundle**
> DelCACertificateBundleSuccess del_ca_certificate_bundle(uid)

Delete a CA certificates

Delete a CA certificates. You must specify a CA certificate uid.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | CA Certificate uid.

    try:
        # Delete a CA certificates
        api_response = api_instance.del_ca_certificate_bundle(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_ca_certificate_bundle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| CA Certificate uid. | 

### Return type

[**DelCACertificateBundleSuccess**](DelCACertificateBundleSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_certificates_bundle**
> DelCertificatesBundleSuccess del_certificates_bundle(uid=uid, name=name)

Delete a certificates bundle

Delete a certificates bundle. You must specify a certificates bundle uid or a certificates bundle name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Identifier of the certificates bundle. (optional)
name = 'name_example' # str | Name of the certificates bundle. (optional)

    try:
        # Delete a certificates bundle
        api_response = api_instance.del_certificates_bundle(uid=uid, name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_certificates_bundle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Identifier of the certificates bundle. | [optional] 
 **name** | **str**| Name of the certificates bundle. | [optional] 

### Return type

[**DelCertificatesBundleSuccess**](DelCertificatesBundleSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_compression_profile**
> DelCompressionProfileSuccess del_compression_profile(uid)

Delete a compression profile

Delete a compression profile. You must specify a compression profile uid or a compression profile name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Compression profile uid or name.

    try:
        # Delete a compression profile
        api_response = api_instance.del_compression_profile(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_compression_profile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Compression profile uid or name. | 

### Return type

[**DelCompressionProfileSuccess**](DelCompressionProfileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_crl_certificate_bundle**
> DelCRLCertificateBundleSuccess del_crl_certificate_bundle(uid)

Delete a CRL certificates

Delete a CRL certificates. You must specify a CRL certificate uid.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | CRL Certificate uid.

    try:
        # Delete a CRL certificates
        api_response = api_instance.del_crl_certificate_bundle(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_crl_certificate_bundle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| CRL Certificate uid. | 

### Return type

[**DelCRLCertificateBundleSuccess**](DelCRLCertificateBundleSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_icx**
> DelIcxSuccess del_icx(uid)

Delete an ICX configuration

Delete a ICX configuration. You must specify an ICX configuration uid or a ICX configuration name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | ICX configuration uid or name.

    try:
        # Delete an ICX configuration
        api_response = api_instance.del_icx(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_icx: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| ICX configuration uid or name. | 

### Return type

[**DelIcxSuccess**](DelIcxSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_label**
> DelLabelSuccess del_label(uid)

Delete a label

Delete a label. You must specify a label uid or a label name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Label uid or name.

    try:
        # Delete a label
        api_response = api_instance.del_label(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_label: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Label uid or name. | 

### Return type

[**DelLabelSuccess**](DelLabelSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_normalization**
> DelNormalizationSuccess del_normalization(uid)

Delete a normalization configuration

Delete a normalization configuration. You must specify a normalization configuration uid or a normalization configuration name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Normalization configuration uid or name.

    try:
        # Delete a normalization configuration
        api_response = api_instance.del_normalization(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_normalization: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Normalization configuration uid or name. | 

### Return type

[**DelNormalizationSuccess**](DelNormalizationSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_ntp**
> DelNtpSuccess del_ntp(uid)

Delete a ntp

Delete a ntp. You must specify a ntp uid or a ntp name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Ntp uid or name.

    try:
        # Delete a ntp
        api_response = api_instance.del_ntp(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_ntp: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Ntp uid or name. | 

### Return type

[**DelNtpSuccess**](DelNtpSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_ocsp_certificate_bundle**
> DelOCSPCertificateBundleSuccess del_ocsp_certificate_bundle(uid)

Delete a OCSP certificates

Delete a OCSP certificates. You must specify a OCSP certificate uid.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | OCSP Certificate uid.

    try:
        # Delete a OCSP certificates
        api_response = api_instance.del_ocsp_certificate_bundle(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_ocsp_certificate_bundle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| OCSP Certificate uid. | 

### Return type

[**DelOCSPCertificateBundleSuccess**](DelOCSPCertificateBundleSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_ramdisk_cache**
> DelRamdiskCacheSuccess del_ramdisk_cache(uid)

Delete a Ramdisk cache

Delete a label. You must specify a Ramdisk cache uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Ramdisk cache uid or name.

    try:
        # Delete a Ramdisk cache
        api_response = api_instance.del_ramdisk_cache(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_ramdisk_cache: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Ramdisk cache uid or name. | 

### Return type

[**DelRamdiskCacheSuccess**](DelRamdiskCacheSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_requesttimeoutprofile**
> DelRequesttimeoutprofileSuccess del_requesttimeoutprofile(uid)

Delete an request timeout profile

Delete an request timeout profile. You must specify an request timeout profile uid or an profile name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Accesslog Profile uid or name.

    try:
        # Delete an request timeout profile
        api_response = api_instance.del_requesttimeoutprofile(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_requesttimeoutprofile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Accesslog Profile uid or name. | 

### Return type

[**DelRequesttimeoutprofileSuccess**](DelRequesttimeoutprofileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_scoringlist**
> DelScoringlistSuccess del_scoringlist(uid)

Delete a scoringlist configuration

Delete a scoringlist. You must specify a scoringlist configuration uid or a scoringlist configuration name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Scoringlist configuration uid or name.

    try:
        # Delete a scoringlist configuration
        api_response = api_instance.del_scoringlist(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_scoringlist: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Scoringlist configuration uid or name. | 

### Return type

[**DelScoringlistSuccess**](DelScoringlistSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_scoringlist_exception**
> DelScoringlistExceptionSuccess del_scoringlist_exception(uid, scoringlist_uid=scoringlist_uid)

Delete a scoringlist exception

Delete a scoringlist exception. You must specify a scoringlist uid and a scoringlist exception uid.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Uid of the new scoringlist exception.
scoringlist_uid = 'scoringlist_uid_example' # str | Scoringlist uid. (optional)

    try:
        # Delete a scoringlist exception
        api_response = api_instance.del_scoringlist_exception(uid, scoringlist_uid=scoringlist_uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_scoringlist_exception: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Uid of the new scoringlist exception. | 
 **scoringlist_uid** | **str**| Scoringlist uid. | [optional] 

### Return type

[**DelScoringlistExceptionSuccess**](DelScoringlistExceptionSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_secondadry_tunnel**
> DelSecondadryTunnelSuccess del_secondadry_tunnel(uid)

Delete a secondary tunnel

Delete a secondary tunnel. You must specify a secondary tunnel uid or a secondary tunnel name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Secondary Tunnel uid.

    try:
        # Delete a secondary tunnel
        api_response = api_instance.del_secondadry_tunnel(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_secondadry_tunnel: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Secondary Tunnel uid. | 

### Return type

[**DelSecondadryTunnelSuccess**](DelSecondadryTunnelSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_security_exception_profile**
> DelSecurityExceptionProfileSuccess del_security_exception_profile(uid)

Delete a security exception profile

Delete a security exception profile. You must specify a security exception profile uid or an profile name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | security exception profile uid or name.

    try:
        # Delete a security exception profile
        api_response = api_instance.del_security_exception_profile(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_security_exception_profile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| security exception profile uid or name. | 

### Return type

[**DelSecurityExceptionProfileSuccess**](DelSecurityExceptionProfileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_sysctlprofile**
> DelSysctlprofileSuccess del_sysctlprofile(uid)

Delete a Sysctl profile

Delete a Sysctl profile. You must specify a Sysctl profile uid or a Sysctl profile name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Sysctl profile uid or name.

    try:
        # Delete a Sysctl profile
        api_response = api_instance.del_sysctlprofile(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_sysctlprofile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Sysctl profile uid or name. | 

### Return type

[**DelSysctlprofileSuccess**](DelSysctlprofileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **del_tunnel**
> DelTunnelSuccess del_tunnel(uid)

Delete a tunnel

Delete a tunnel. You must specify a tunnel uid or a tunnel name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Tunnel uid.

    try:
        # Delete a tunnel
        api_response = api_instance.del_tunnel(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->del_tunnel: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Tunnel uid. | 

### Return type

[**DelTunnelSuccess**](DelTunnelSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_certificate**
> DeleteCertificateSuccess delete_certificate(uid)

Delete a certificate

Delete a certificate. You must specify a certificate uid or a certificate name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Select certificate to delete

    try:
        # Delete a certificate
        api_response = api_instance.delete_certificate(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->delete_certificate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Select certificate to delete | 

### Return type

[**DeleteCertificateSuccess**](DeleteCertificateSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_networ_interface**
> DeleteNetworInterfaceSuccess delete_networ_interface(uid)

Delete a network interface

Delete a network interface. You must specify a network interface uid or a network interface name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | The network interface's UID we want to delete.

    try:
        # Delete a network interface
        api_response = api_instance.delete_networ_interface(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->delete_networ_interface: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| The network interface&#39;s UID we want to delete. | 

### Return type

[**DeleteNetworInterfaceSuccess**](DeleteNetworInterfaceSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (null if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_reverse_proxies**
> DeleteReverseProxiesSuccess delete_reverse_proxies(uid)

Delete a reverse proxy

Delete a reverse proxy. You must specify a reverse proxy uid.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | The reverse proxy's UID we want to delete.

    try:
        # Delete a reverse proxy
        api_response = api_instance.delete_reverse_proxies(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->delete_reverse_proxies: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| The reverse proxy&#39;s UID we want to delete. | 

### Return type

[**DeleteReverseProxiesSuccess**](DeleteReverseProxiesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (null if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_reverse_proxy_profile**
> DeleteReverseProxyProfileSuccess delete_reverse_proxy_profile(uid)

Delete a reverse proxy profile

Delete a reverse proxy profile. You must specify a reverse proxy profile uid or a reverse proxy profile name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Select Reverse proxy profile to delete

    try:
        # Delete a reverse proxy profile
        api_response = api_instance.delete_reverse_proxy_profile(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->delete_reverse_proxy_profile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Select Reverse proxy profile to delete | 

### Return type

[**DeleteReverseProxyProfileSuccess**](DeleteReverseProxyProfileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deletesslprofiles**
> DeletesslprofilesSuccess deletesslprofiles(uid)

Delete a SSL Profile

Delete a SSL Profile. You must specify a SSL Profile uid or a SSL Profile name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Select SSL Profile to delete

    try:
        # Delete a SSL Profile
        api_response = api_instance.deletesslprofiles(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->deletesslprofiles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Select SSL Profile to delete | 

### Return type

[**DeletesslprofilesSuccess**](DeletesslprofilesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result (empty array [] if everything goes well) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **download_ca_certificate**
> DownloadCACertificateSuccess download_ca_certificate(uid)

Download CA Certificates File

Download the special CA certificate file.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | CA certificate uid.

    try:
        # Download CA Certificates File
        api_response = api_instance.download_ca_certificate(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->download_ca_certificate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| CA certificate uid. | 

### Return type

[**DownloadCACertificateSuccess**](DownloadCACertificateSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The CA certificate content file |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **download_caocsp_certificate**
> DownloadCAOCSPCertificateSuccess download_caocsp_certificate(uid)

Download CAOCSP Certificates File

Download the special CAOCSP certificate file.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | CAOCSP certificate uid.

    try:
        # Download CAOCSP Certificates File
        api_response = api_instance.download_caocsp_certificate(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->download_caocsp_certificate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| CAOCSP certificate uid. | 

### Return type

[**DownloadCAOCSPCertificateSuccess**](DownloadCAOCSPCertificateSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The CAOCSP certificate content file |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **download_certificate**
> DownloadCertificateSuccess download_certificate(uid)

Download certificate File

Download the special certificate file.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Certificate uid.

    try:
        # Download certificate File
        api_response = api_instance.download_certificate(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->download_certificate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Certificate uid. | 

### Return type

[**DownloadCertificateSuccess**](DownloadCertificateSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The certificate files |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **download_crl_certificate**
> DownloadCRLCertificateSuccess download_crl_certificate(uid)

Download CRL Certificates File

Download the special CRL certificate file.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | CRL certificate uid.

    try:
        # Download CRL Certificates File
        api_response = api_instance.download_crl_certificate(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->download_crl_certificate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| CRL certificate uid. | 

### Return type

[**DownloadCRLCertificateSuccess**](DownloadCRLCertificateSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The CRL certificate content file |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **download_log_filter**
> DownloadLogFilterSuccess download_log_filter(uid)

Download LogFilter File

Download the special logfilter file.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | logfilter's uid.

    try:
        # Download LogFilter File
        api_response = api_instance.download_log_filter(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->download_log_filter: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| logfilter&#39;s uid. | 

### Return type

[**DownloadLogFilterSuccess**](DownloadLogFilterSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The content of logfilter file. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **file_upload**
> FileUploadSuccess file_upload(file=file)

Send a file

Create a temporary file to be used later by the api, The file expire after 30 secondes

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    file = '/path/to/file' # file |  (optional)

    try:
        # Send a file
        api_response = api_instance.file_upload(file=file)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->file_upload: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **file**|  | [optional] 

### Return type

[**FileUploadSuccess**](FileUploadSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_access_log_profiles**
> GetAccessLogProfilesSuccess get_access_log_profiles(uid=uid, name=name)

Get access log profiles

Get access log profiles. You can specify an access log profile uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Access log Profile uid. (optional)
name = 'name_example' # str | Access log Profile name. (optional)

    try:
        # Get access log profiles
        api_response = api_instance.get_access_log_profiles(uid=uid, name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_access_log_profiles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Access log Profile uid. | [optional] 
 **name** | **str**| Access log Profile name. | [optional] 

### Return type

[**GetAccessLogProfilesSuccess**](GetAccessLogProfilesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching access log profiles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_alertingdestinations**
> GetAlertingdestinationsSuccess get_alertingdestinations(uid=uid, name=name)

Get alerting destinations

Get alerting destinations. You can specify an alerting destination uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Alerting destination uid. (optional)
name = 'name_example' # str | Alerting destination name. (optional)

    try:
        # Get alerting destinations
        api_response = api_instance.get_alertingdestinations(uid=uid, name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_alertingdestinations: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Alerting destination uid. | [optional] 
 **name** | **str**| Alerting destination name. | [optional] 

### Return type

[**GetAlertingdestinationsSuccess**](GetAlertingdestinationsSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching alerting destinations |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_appliances**
> GetAppliancesSuccess get_appliances(name=name)

Get appliances

Get appliances. You can specify an appliance uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Appliance name. (optional)

    try:
        # Get appliances
        api_response = api_instance.get_appliances(name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_appliances: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Appliance name. | [optional] 

### Return type

[**GetAppliancesSuccess**](GetAppliancesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching appliances. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_ca_certificates**
> GetCACertificatesSuccess get_ca_certificates(bundle_uid=bundle_uid)

Get CA certificates of bundle

Get CA certificates. You can specify a certificate bundle uid.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    bundle_uid = 'bundle_uid_example' # str | Certificate Bundle uid or CA Certificates uid. The bundleUid is required when filtering with uid. (optional)

    try:
        # Get CA certificates of bundle
        api_response = api_instance.get_ca_certificates(bundle_uid=bundle_uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_ca_certificates: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bundle_uid** | **str**| Certificate Bundle uid or CA Certificates uid. The bundleUid is required when filtering with uid. | [optional] 

### Return type

[**GetCACertificatesSuccess**](GetCACertificatesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching certificates bundles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_certificates**
> GetCertificatesSuccess get_certificates(name=name, label_name=label_name)

Get certificates

Get certificates. You can specify a certificates uid or name. You can also filter by label uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Return a filtered list (optional)
label_name = 'label_name_example' # str | Return a list that is filtered by label (optional)

    try:
        # Get certificates
        api_response = api_instance.get_certificates(name=name, label_name=label_name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_certificates: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Return a filtered list | [optional] 
 **label_name** | **str**| Return a list that is filtered by label | [optional] 

### Return type

[**GetCertificatesSuccess**](GetCertificatesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing matching certificates. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_certificates_bundles**
> GetCertificatesBundlesSuccess get_certificates_bundles(uid=uid, name=name)

Get certificates bundles

Get certificates bundles. You can specify a certificates bundle uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Certificates Bundle uid. (optional)
name = 'name_example' # str | Certificates Bundle name. (optional)

    try:
        # Get certificates bundles
        api_response = api_instance.get_certificates_bundles(uid=uid, name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_certificates_bundles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Certificates Bundle uid. | [optional] 
 **name** | **str**| Certificates Bundle name. | [optional] 

### Return type

[**GetCertificatesBundlesSuccess**](GetCertificatesBundlesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching certificates bundles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_compression_profiles**
> GetCompressionProfilesSuccess get_compression_profiles(name=name)

Get compression profiles

Get compression profiles. You can specify a compression profile uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Compression profile uid or name. (optional)

    try:
        # Get compression profiles
        api_response = api_instance.get_compression_profiles(name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_compression_profiles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Compression profile uid or name. | [optional] 

### Return type

[**GetCompressionProfilesSuccess**](GetCompressionProfilesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching compression profiles. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_crl_certificates**
> GetCRLCertificatesSuccess get_crl_certificates(bundle_uid=bundle_uid)

Get CRL certificates of bundle

Get CRL certificates. You can specify a certificate bundle uid.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    bundle_uid = 'bundle_uid_example' # str | Certificate Bundle uid or CRL Certificates uid. The bundleUid is required when filtering with uid. (optional)

    try:
        # Get CRL certificates of bundle
        api_response = api_instance.get_crl_certificates(bundle_uid=bundle_uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_crl_certificates: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bundle_uid** | **str**| Certificate Bundle uid or CRL Certificates uid. The bundleUid is required when filtering with uid. | [optional] 

### Return type

[**GetCRLCertificatesSuccess**](GetCRLCertificatesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching certificates bundles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_icx**
> GetIcxSuccess get_icx(name=name)

Get ICX configurations

Get ICX. You can specify an ICX configuration uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | ICX configuration uid or name. (optional)

    try:
        # Get ICX configurations
        api_response = api_instance.get_icx(name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_icx: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| ICX configuration uid or name. | [optional] 

### Return type

[**GetIcxSuccess**](GetIcxSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching ICX configurations |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_icx_templates**
> GetIcxTemplatesSuccess get_icx_templates(name=name)

Get ICX templates

Get ICX templates. You can specify an ICX template uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | ICX template uid or name. (optional)

    try:
        # Get ICX templates
        api_response = api_instance.get_icx_templates(name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_icx_templates: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| ICX template uid or name. | [optional] 

### Return type

[**GetIcxTemplatesSuccess**](GetIcxTemplatesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the ICX templates |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_labels**
> GetLabelsSuccess get_labels(name=name)

Get labels

Get labels. You can specify a label uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Label uid or name. (optional)

    try:
        # Get labels
        api_response = api_instance.get_labels(name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_labels: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Label uid or name. | [optional] 

### Return type

[**GetLabelsSuccess**](GetLabelsSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching labels |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_licenses**
> GetLicensesSuccess get_licenses()

Get licenses

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    
    try:
        # Get licenses
        api_response = api_instance.get_licenses()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_licenses: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**GetLicensesSuccess**](GetLicensesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Array containing licenses. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_log_filter**
> GetLogFilterSuccess get_log_filter(uid=uid, name=name)

Get LogFilters

Get logfilters. You can specify a LogFilter uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | logfilter uid. (optional)
name = 'name_example' # str | logfilter name. (optional)

    try:
        # Get LogFilters
        api_response = api_instance.get_log_filter(uid=uid, name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_log_filter: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| logfilter uid. | [optional] 
 **name** | **str**| logfilter name. | [optional] 

### Return type

[**GetLogFilterSuccess**](GetLogFilterSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching logfilters |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_network_devices**
> GetNetworkDevicesSuccess get_network_devices(uid=uid, appliance_uid=appliance_uid)

Get network devices

Get network devices. You can specify a network devices uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Identifier of the network devices. (optional)
appliance_uid = 'appliance_uid_example' # str | Appliance uid of the network devices. (optional)

    try:
        # Get network devices
        api_response = api_instance.get_network_devices(uid=uid, appliance_uid=appliance_uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_network_devices: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Identifier of the network devices. | [optional] 
 **appliance_uid** | **str**| Appliance uid of the network devices. | [optional] 

### Return type

[**GetNetworkDevicesSuccess**](GetNetworkDevicesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing network devices. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_network_interfaces**
> GetNetworkInterfacesSuccess get_network_interfaces(name=name, appliance_uid=appliance_uid, ip=ip)

Get network interfaces

Get network interfaces. You can specify a network interfaces uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Identifier of the network interfaces. (optional)
appliance_uid = 'appliance_uid_example' # str | Appliance uid of the network interface. (optional)
ip = 'ip_example' # str | Filter on a specific IP. We can use wildcard like 192.168.0.* (optional)

    try:
        # Get network interfaces
        api_response = api_instance.get_network_interfaces(name=name, appliance_uid=appliance_uid, ip=ip)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_network_interfaces: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Identifier of the network interfaces. | [optional] 
 **appliance_uid** | **str**| Appliance uid of the network interface. | [optional] 
 **ip** | **str**| Filter on a specific IP. We can use wildcard like 192.168.0.* | [optional] 

### Return type

[**GetNetworkInterfacesSuccess**](GetNetworkInterfacesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing network interfaces. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_normalization**
> GetNormalizationSuccess get_normalization(name=name)

Get normalization configurations

Get Normalization. You can specify an normalization configuration uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Normalization configuration uid or name. (optional)

    try:
        # Get normalization configurations
        api_response = api_instance.get_normalization(name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_normalization: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Normalization configuration uid or name. | [optional] 

### Return type

[**GetNormalizationSuccess**](GetNormalizationSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching normalization configurations |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_ntps**
> GetNtpsSuccess get_ntps(name=name)

Get ntps

Get ntps. You can specify a ntp uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Ntp name. (optional)

    try:
        # Get ntps
        api_response = api_instance.get_ntps(name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_ntps: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Ntp name. | [optional] 

### Return type

[**GetNtpsSuccess**](GetNtpsSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching ntps |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_ocsp_certificates**
> GetOCSPCertificatesSuccess get_ocsp_certificates(bundle_uid=bundle_uid)

Get OCSP certificates of bundle

Get OCSP certificates. You can specify a certificate bundle.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    bundle_uid = 'bundle_uid_example' # str | Certificate Bundle uid or OCSP Certificates uid. The bundleUid is required when filtering with uid. (optional)

    try:
        # Get OCSP certificates of bundle
        api_response = api_instance.get_ocsp_certificates(bundle_uid=bundle_uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_ocsp_certificates: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bundle_uid** | **str**| Certificate Bundle uid or OCSP Certificates uid. The bundleUid is required when filtering with uid. | [optional] 

### Return type

[**GetOCSPCertificatesSuccess**](GetOCSPCertificatesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching certificates bundles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_ramdisk_cache**
> GetRamdiskCacheSuccess get_ramdisk_cache(name=name)

Get Ramdisk cache profile

Get Ramdisk cache profiles. You can specify a Ramdisk cache profile uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Ramdisk cache profile uid or name. (optional)

    try:
        # Get Ramdisk cache profile
        api_response = api_instance.get_ramdisk_cache(name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_ramdisk_cache: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Ramdisk cache profile uid or name. | [optional] 

### Return type

[**GetRamdiskCacheSuccess**](GetRamdiskCacheSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching Ramdisk cache profile. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_requesttimeoutprofiles**
> GetRequesttimeoutprofilesSuccess get_requesttimeoutprofiles(name=name)

Get request timeout profiles

Get request timeout profiles. You can specify an request timeout profile uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | request timeout Profile uid or name. (optional)

    try:
        # Get request timeout profiles
        api_response = api_instance.get_requesttimeoutprofiles(name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_requesttimeoutprofiles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| request timeout Profile uid or name. | [optional] 

### Return type

[**GetRequesttimeoutprofilesSuccess**](GetRequesttimeoutprofilesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching request timeout profiles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_reverse_proxies**
> GetReverseProxiesSuccess get_reverse_proxies(uid=uid, name=name, label_uid=label_uid, label_name=label_name, appliance_uid=appliance_uid)

Get reverse proxies

Get reverse proxies. You can specify a reverse proxy uid or name. You can also filter by label uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Identifier of the reverse proxies. (optional)
name = 'name_example' # str | Name identifier of the reverse proxies. (optional)
label_uid = 'label_uid_example' # str | Label uid. (optional)
label_name = 'label_name_example' # str | Label name. (optional)
appliance_uid = 'appliance_uid_example' # str | Appliance uid of the reverse proxies (optional)

    try:
        # Get reverse proxies
        api_response = api_instance.get_reverse_proxies(uid=uid, name=name, label_uid=label_uid, label_name=label_name, appliance_uid=appliance_uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_reverse_proxies: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Identifier of the reverse proxies. | [optional] 
 **name** | **str**| Name identifier of the reverse proxies. | [optional] 
 **label_uid** | **str**| Label uid. | [optional] 
 **label_name** | **str**| Label name. | [optional] 
 **appliance_uid** | **str**| Appliance uid of the reverse proxies | [optional] 

### Return type

[**GetReverseProxiesSuccess**](GetReverseProxiesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_reverse_proxy_profiles**
> GetReverseProxyProfilesSuccess get_reverse_proxy_profiles(name=name)

Get reverse proxy profiles

Get reverse proxy profiles. You can specify a reverse proxy profiles uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Reverse proxy profile uid or name. (optional)

    try:
        # Get reverse proxy profiles
        api_response = api_instance.get_reverse_proxy_profiles(name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_reverse_proxy_profiles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Reverse proxy profile uid or name. | [optional] 

### Return type

[**GetReverseProxyProfilesSuccess**](GetReverseProxyProfilesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Array containing matching reverse proxy profiles. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_scoringlist_exceptions**
> GetScoringlistExceptionsSuccess get_scoringlist_exceptions(scoringlist_uid=scoringlist_uid, uid=uid)

Get scoringlist exceptions

Get scoringlist exceptions. You have to set a specify scoringlist uid.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    scoringlist_uid = 'scoringlist_uid_example' # str | Scoringlist uid. (optional)
uid = 'uid_example' # str | Scoringlist exception uid (optional)

    try:
        # Get scoringlist exceptions
        api_response = api_instance.get_scoringlist_exceptions(scoringlist_uid=scoringlist_uid, uid=uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_scoringlist_exceptions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scoringlist_uid** | **str**| Scoringlist uid. | [optional] 
 **uid** | **str**| Scoringlist exception uid | [optional] 

### Return type

[**GetScoringlistExceptionsSuccess**](GetScoringlistExceptionsSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The list of scoringlist exceptions |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_scoringlist_templates**
> GetScoringlistTemplatesSuccess get_scoringlist_templates(uid=uid)

Get scoringlist templates

Get scoringlist templates. You can specify a scoringlist template uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Scoringlist template uid or name. (optional)

    try:
        # Get scoringlist templates
        api_response = api_instance.get_scoringlist_templates(uid=uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_scoringlist_templates: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Scoringlist template uid or name. | [optional] 

### Return type

[**GetScoringlistTemplatesSuccess**](GetScoringlistTemplatesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the scoringlist templates |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_scoringlists**
> GetScoringlistsSuccess get_scoringlists(name=name)

Get scoringlist configurations

Get scoringlist configurations. You can specify a scoringlist configuration uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Scoringlist configuration uid or name. (optional)

    try:
        # Get scoringlist configurations
        api_response = api_instance.get_scoringlists(name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_scoringlists: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Scoringlist configuration uid or name. | [optional] 

### Return type

[**GetScoringlistsSuccess**](GetScoringlistsSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching scoringlist configurations |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_secondary_tunnels**
> GetSecondaryTunnelsSuccess get_secondary_tunnels(name=name)

Get secondary tunnels

Get secondary tunnels. You can specify a secondary tunnel uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Secondary Tunnel uid or name. (optional)

    try:
        # Get secondary tunnels
        api_response = api_instance.get_secondary_tunnels(name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_secondary_tunnels: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Secondary Tunnel uid or name. | [optional] 

### Return type

[**GetSecondaryTunnelsSuccess**](GetSecondaryTunnelsSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching secondary tunnels |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_security_exception_profiles**
> GetSecurityExceptionProfilesSuccess get_security_exception_profiles(name=name)

Get Security Exception Profiles

Get security exception profiles. You can specify a security exception profile uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | security exception profile uid or name. (optional)

    try:
        # Get Security Exception Profiles
        api_response = api_instance.get_security_exception_profiles(name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_security_exception_profiles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| security exception profile uid or name. | [optional] 

### Return type

[**GetSecurityExceptionProfilesSuccess**](GetSecurityExceptionProfilesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching security exception profiles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_sslprofiles**
> GetSslprofilesSuccess get_sslprofiles(name)

Get SSL Profiles

Get SSL Profiles. You can specify a SSL Profiles uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Return a filtered list

    try:
        # Get SSL Profiles
        api_response = api_instance.get_sslprofiles(name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_sslprofiles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Return a filtered list | 

### Return type

[**GetSslprofilesSuccess**](GetSslprofilesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing matching SSL Profile. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_static_scoringlists**
> GetStaticScoringlistsSuccess get_static_scoringlists(uid=uid)

Get static scoringlists

Get static scoringlists. You can specify a static scoringlist uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Static scoringlist uid or name. (optional)

    try:
        # Get static scoringlists
        api_response = api_instance.get_static_scoringlists(uid=uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_static_scoringlists: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Static scoringlist uid or name. | [optional] 

### Return type

[**GetStaticScoringlistsSuccess**](GetStaticScoringlistsSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the static scoringlists |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_sysctlprofiles**
> GetSysctlprofilesSuccess get_sysctlprofiles(name=name)

Get Sysctl profiles

Get Sysctl profiles. You can specify a Sysctl profile uid or name.<br> This item lets you manage profiles for configuring the Linux kernel TCP/IP stack. More details on sysctl parameters can be found on the Linux kernel documentation.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Sysctl profile uid or name. (optional)

    try:
        # Get Sysctl profiles
        api_response = api_instance.get_sysctlprofiles(name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_sysctlprofiles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Sysctl profile uid or name. | [optional] 

### Return type

[**GetSysctlprofilesSuccess**](GetSysctlprofilesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching Sysctl profiles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_tunnels**
> GetTunnelsSuccess get_tunnels(name=name, uid=uid, label_uid=label_uid, label_name=label_name)

Get tunnels

Get tunnels. You can specify a tunnel uid or name. You can also filter by label uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Tunnel name. (optional)
uid = 'uid_example' # str | Tunnel uid. (optional)
label_uid = 'label_uid_example' # str | Label uid. (optional)
label_name = 'label_name_example' # str | Label name. (optional)

    try:
        # Get tunnels
        api_response = api_instance.get_tunnels(name=name, uid=uid, label_uid=label_uid, label_name=label_name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_tunnels: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Tunnel name. | [optional] 
 **uid** | **str**| Tunnel uid. | [optional] 
 **label_uid** | **str**| Label uid. | [optional] 
 **label_name** | **str**| Label name. | [optional] 

### Return type

[**GetTunnelsSuccess**](GetTunnelsSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching tunnels |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getnormalizationtemplate**
> GetnormalizationtemplateSuccess getnormalizationtemplate(uid=uid)

Get normalization templates

Get normalization templates. You can specify a normalization template uid or name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Normalization template uid or name. (optional)

    try:
        # Get normalization templates
        api_response = api_instance.getnormalizationtemplate(uid=uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->getnormalizationtemplate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Normalization template uid or name. | [optional] 

### Return type

[**GetnormalizationtemplateSuccess**](GetnormalizationtemplateSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the normalization templates |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_accesslogprofile**
> PatchAccesslogprofileSuccess patch_accesslogprofile(patch_accesslogprofile, uid=uid, name=name)

Patch an access log profile

Patch an existing access log profile. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    patch_accesslogprofile = r_s_waf_api_client.PatchAccesslogprofile() # PatchAccesslogprofile | 
uid = 'uid_example' # str | Accesslog Profile uid. (optional)
name = 'name_example' # str | Accesslog Profile name. (optional)

    try:
        # Patch an access log profile
        api_response = api_instance.patch_accesslogprofile(patch_accesslogprofile, uid=uid, name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_accesslogprofile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patch_accesslogprofile** | [**PatchAccesslogprofile**](PatchAccesslogprofile.md)|  | 
 **uid** | **str**| Accesslog Profile uid. | [optional] 
 **name** | **str**| Accesslog Profile name. | [optional] 

### Return type

[**PatchAccesslogprofileSuccess**](PatchAccesslogprofileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching access log profiles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_alerting_destination**
> PatchAlertingDestinationSuccess patch_alerting_destination(patch_alerting_destination, uid=uid, name=name)

Patch an alerting destination

Patch an alerting destination. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    patch_alerting_destination = r_s_waf_api_client.PatchAlertingDestination() # PatchAlertingDestination | 
uid = 'uid_example' # str | Alerting destination uid. (optional)
name = 'name_example' # str | Alerting destination name. (optional)

    try:
        # Patch an alerting destination
        api_response = api_instance.patch_alerting_destination(patch_alerting_destination, uid=uid, name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_alerting_destination: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patch_alerting_destination** | [**PatchAlertingDestination**](PatchAlertingDestination.md)|  | 
 **uid** | **str**| Alerting destination uid. | [optional] 
 **name** | **str**| Alerting destination name. | [optional] 

### Return type

[**PatchAlertingDestinationSuccess**](PatchAlertingDestinationSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching alerting destinations |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_appliance**
> PatchApplianceSuccess patch_appliance(uid, patch_appliance)

Patch an appliance

Patch an existing appliance. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Appliance uid.
patch_appliance = r_s_waf_api_client.PatchAppliance() # PatchAppliance | 

    try:
        # Patch an appliance
        api_response = api_instance.patch_appliance(uid, patch_appliance)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_appliance: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Appliance uid. | 
 **patch_appliance** | [**PatchAppliance**](PatchAppliance.md)|  | 

### Return type

[**PatchApplianceSuccess**](PatchApplianceSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching appliances. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_certificate**
> PatchCertificateSuccess patch_certificate(uid, patch_certificate)

Patch a certificate

Patch a certificate.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Certificate's uid.
patch_certificate = r_s_waf_api_client.PatchCertificate() # PatchCertificate | 

    try:
        # Patch a certificate
        api_response = api_instance.patch_certificate(uid, patch_certificate)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_certificate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Certificate&#39;s uid. | 
 **patch_certificate** | [**PatchCertificate**](PatchCertificate.md)|  | 

### Return type

[**PatchCertificateSuccess**](PatchCertificateSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing new certificates. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_certificates_bundle**
> PatchCertificatesBundleSuccess patch_certificates_bundle(patch_certificates_bundle, uid=uid, name=name)

Patch a certificates bundle

Patch an existing certificates bundle. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    patch_certificates_bundle = r_s_waf_api_client.PatchCertificatesBundle() # PatchCertificatesBundle | 
uid = 'uid_example' # str | Identifier of the certificates bundle. (optional)
name = 'name_example' # str | Name of the certificates bundle. (optional)

    try:
        # Patch a certificates bundle
        api_response = api_instance.patch_certificates_bundle(patch_certificates_bundle, uid=uid, name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_certificates_bundle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patch_certificates_bundle** | [**PatchCertificatesBundle**](PatchCertificatesBundle.md)|  | 
 **uid** | **str**| Identifier of the certificates bundle. | [optional] 
 **name** | **str**| Name of the certificates bundle. | [optional] 

### Return type

[**PatchCertificatesBundleSuccess**](PatchCertificatesBundleSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching certificates bundles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_compression_profile**
> PatchCompressionProfileSuccess patch_compression_profile(uid, patch_compression_profile)

Patch a compression profile

Patch a existing compression profile. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Compression profile uid or name.
patch_compression_profile = r_s_waf_api_client.PatchCompressionProfile() # PatchCompressionProfile | 

    try:
        # Patch a compression profile
        api_response = api_instance.patch_compression_profile(uid, patch_compression_profile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_compression_profile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Compression profile uid or name. | 
 **patch_compression_profile** | [**PatchCompressionProfile**](PatchCompressionProfile.md)|  | 

### Return type

[**PatchCompressionProfileSuccess**](PatchCompressionProfileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching compression profiles. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_icx**
> PatchIcxSuccess patch_icx(uid, patch_icx)

Patch an ICX configurattion

Patch an existing ICX configuration. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | ICX configuration uid or name.
patch_icx = r_s_waf_api_client.PatchIcx() # PatchIcx | 

    try:
        # Patch an ICX configurattion
        api_response = api_instance.patch_icx(uid, patch_icx)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_icx: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| ICX configuration uid or name. | 
 **patch_icx** | [**PatchIcx**](PatchIcx.md)|  | 

### Return type

[**PatchIcxSuccess**](PatchIcxSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching ICX configurations |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_label**
> PatchLabelSuccess patch_label(uid, patch_label)

Patch a label

Patch an existing label. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Label uid or name.
patch_label = r_s_waf_api_client.PatchLabel() # PatchLabel | 

    try:
        # Patch a label
        api_response = api_instance.patch_label(uid, patch_label)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_label: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Label uid or name. | 
 **patch_label** | [**PatchLabel**](PatchLabel.md)|  | 

### Return type

[**PatchLabelSuccess**](PatchLabelSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching labels |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_network_interface**
> PatchNetworkInterfaceSuccess patch_network_interface(name, patch_network_interface)

Patch a network interface

Patch a existing network interface. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Identifier of the network interface.
patch_network_interface = r_s_waf_api_client.PatchNetworkInterface() # PatchNetworkInterface | 

    try:
        # Patch a network interface
        api_response = api_instance.patch_network_interface(name, patch_network_interface)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_network_interface: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Identifier of the network interface. | 
 **patch_network_interface** | [**PatchNetworkInterface**](PatchNetworkInterface.md)|  | 

### Return type

[**PatchNetworkInterfaceSuccess**](PatchNetworkInterfaceSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object the matching network interface. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_normalization**
> PatchNormalizationSuccess patch_normalization(uid, patch_normalization)

Patch a normalization configurattion

Patch an existing normalization configuration. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Normalization configuration uid or name.
patch_normalization = r_s_waf_api_client.PatchNormalization() # PatchNormalization | 

    try:
        # Patch a normalization configurattion
        api_response = api_instance.patch_normalization(uid, patch_normalization)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_normalization: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Normalization configuration uid or name. | 
 **patch_normalization** | [**PatchNormalization**](PatchNormalization.md)|  | 

### Return type

[**PatchNormalizationSuccess**](PatchNormalizationSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching normalization configurations |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_ntp**
> PatchNtpSuccess patch_ntp(uid, patch_ntp)

Patch a ntp

Patch an existing ntp. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Ntp uid or name.
patch_ntp = r_s_waf_api_client.PatchNtp() # PatchNtp | 

    try:
        # Patch a ntp
        api_response = api_instance.patch_ntp(uid, patch_ntp)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_ntp: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Ntp uid or name. | 
 **patch_ntp** | [**PatchNtp**](PatchNtp.md)|  | 

### Return type

[**PatchNtpSuccess**](PatchNtpSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching ntps |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_ramdisk_cache**
> PatchRamdiskCacheSuccess patch_ramdisk_cache(uid, patch_ramdisk_cache)

Patch a Ramdisk cache profile

Patch an existing Ramdisk cache profile. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Ramdisk cache uid or name.
patch_ramdisk_cache = r_s_waf_api_client.PatchRamdiskCache() # PatchRamdiskCache | 

    try:
        # Patch a Ramdisk cache profile
        api_response = api_instance.patch_ramdisk_cache(uid, patch_ramdisk_cache)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_ramdisk_cache: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Ramdisk cache uid or name. | 
 **patch_ramdisk_cache** | [**PatchRamdiskCache**](PatchRamdiskCache.md)|  | 

### Return type

[**PatchRamdiskCacheSuccess**](PatchRamdiskCacheSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching Ramdisk cache profile. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_requesttimeoutprofile**
> PatchRequesttimeoutprofileSuccess patch_requesttimeoutprofile(uid, patch_requesttimeoutprofile)

Patch an request timeout profile

Patch an existing request timeout profile. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Accesslog Profile uid or name.
patch_requesttimeoutprofile = r_s_waf_api_client.PatchRequesttimeoutprofile() # PatchRequesttimeoutprofile | 

    try:
        # Patch an request timeout profile
        api_response = api_instance.patch_requesttimeoutprofile(uid, patch_requesttimeoutprofile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_requesttimeoutprofile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Accesslog Profile uid or name. | 
 **patch_requesttimeoutprofile** | [**PatchRequesttimeoutprofile**](PatchRequesttimeoutprofile.md)|  | 

### Return type

[**PatchRequesttimeoutprofileSuccess**](PatchRequesttimeoutprofileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching request timeout profiles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_reverse_proxy**
> PatchReverseProxySuccess patch_reverse_proxy(uid, patch_reverse_proxy)

Patch a reverse proxy

Patch a existing reverse proxy. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Identifier of the reverse proxy.
patch_reverse_proxy = r_s_waf_api_client.PatchReverseProxy() # PatchReverseProxy | 

    try:
        # Patch a reverse proxy
        api_response = api_instance.patch_reverse_proxy(uid, patch_reverse_proxy)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_reverse_proxy: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Identifier of the reverse proxy. | 
 **patch_reverse_proxy** | [**PatchReverseProxy**](PatchReverseProxy.md)|  | 

### Return type

[**PatchReverseProxySuccess**](PatchReverseProxySuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_reverse_proxy_profile**
> PatchReverseProxyProfileSuccess patch_reverse_proxy_profile(uid, patch_reverse_proxy_profile)

Patch a reverse proxy profile

Patch a existing reverse proxy profile. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Reverse proxy profile uid or name.
patch_reverse_proxy_profile = r_s_waf_api_client.PatchReverseProxyProfile() # PatchReverseProxyProfile | 

    try:
        # Patch a reverse proxy profile
        api_response = api_instance.patch_reverse_proxy_profile(uid, patch_reverse_proxy_profile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_reverse_proxy_profile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Reverse proxy profile uid or name. | 
 **patch_reverse_proxy_profile** | [**PatchReverseProxyProfile**](PatchReverseProxyProfile.md)|  | 

### Return type

[**PatchReverseProxyProfileSuccess**](PatchReverseProxyProfileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing matching reverse proxy profiles. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_scoringlist**
> PatchScoringlistSuccess patch_scoringlist(uid, patch_scoringlist)

Patch a scoringlist configuration

Patch an existing scoringlist. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Scoringlist uid or name.
patch_scoringlist = r_s_waf_api_client.PatchScoringlist() # PatchScoringlist | 

    try:
        # Patch a scoringlist configuration
        api_response = api_instance.patch_scoringlist(uid, patch_scoringlist)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_scoringlist: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Scoringlist uid or name. | 
 **patch_scoringlist** | [**PatchScoringlist**](PatchScoringlist.md)|  | 

### Return type

[**PatchScoringlistSuccess**](PatchScoringlistSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching scoringlist configuration |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_scoringlist_exception**
> PatchScoringlistExceptionSuccess patch_scoringlist_exception(uid, patch_scoringlist_exception, scoringlist_uid=scoringlist_uid)

Patch a scoringlist exception

Patch an existing scoringlist exception. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Uid of the new scoringlist exception.
patch_scoringlist_exception = r_s_waf_api_client.PatchScoringlistException() # PatchScoringlistException | 
scoringlist_uid = 'scoringlist_uid_example' # str | Scoringlist uid. (optional)

    try:
        # Patch a scoringlist exception
        api_response = api_instance.patch_scoringlist_exception(uid, patch_scoringlist_exception, scoringlist_uid=scoringlist_uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_scoringlist_exception: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Uid of the new scoringlist exception. | 
 **patch_scoringlist_exception** | [**PatchScoringlistException**](PatchScoringlistException.md)|  | 
 **scoringlist_uid** | **str**| Scoringlist uid. | [optional] 

### Return type

[**PatchScoringlistExceptionSuccess**](PatchScoringlistExceptionSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The matching scoringlist exceptions |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_secondary_tunnel**
> PatchSecondaryTunnelSuccess patch_secondary_tunnel(uid, patch_secondary_tunnel)

Patch a secondary tunnel

Patch an existing secondary tunnel. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Secondary Tunnel uid.
patch_secondary_tunnel = r_s_waf_api_client.PatchSecondaryTunnel() # PatchSecondaryTunnel | 

    try:
        # Patch a secondary tunnel
        api_response = api_instance.patch_secondary_tunnel(uid, patch_secondary_tunnel)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_secondary_tunnel: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Secondary Tunnel uid. | 
 **patch_secondary_tunnel** | [**PatchSecondaryTunnel**](PatchSecondaryTunnel.md)|  | 

### Return type

[**PatchSecondaryTunnelSuccess**](PatchSecondaryTunnelSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching secondary tunnels |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_security_exception_profile**
> PatchSecurityExceptionProfileSuccess patch_security_exception_profile(uid, patch_security_exception_profile)

Patch a Security Exception Profile

Patch a security exception profile.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Security Exception Profile uid or name.
patch_security_exception_profile = r_s_waf_api_client.PatchSecurityExceptionProfile() # PatchSecurityExceptionProfile | 

    try:
        # Patch a Security Exception Profile
        api_response = api_instance.patch_security_exception_profile(uid, patch_security_exception_profile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_security_exception_profile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Security Exception Profile uid or name. | 
 **patch_security_exception_profile** | [**PatchSecurityExceptionProfile**](PatchSecurityExceptionProfile.md)|  | 

### Return type

[**PatchSecurityExceptionProfileSuccess**](PatchSecurityExceptionProfileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The created security exception profile data |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_sysctlprofile**
> PatchSysctlprofileSuccess patch_sysctlprofile(uid, patch_sysctlprofile)

Patch a Sysctl profile

Patch an existing Sysctl profile. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Sysctl profile uid or name.
patch_sysctlprofile = r_s_waf_api_client.PatchSysctlprofile() # PatchSysctlprofile | 

    try:
        # Patch a Sysctl profile
        api_response = api_instance.patch_sysctlprofile(uid, patch_sysctlprofile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_sysctlprofile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Sysctl profile uid or name. | 
 **patch_sysctlprofile** | [**PatchSysctlprofile**](PatchSysctlprofile.md)|  | 

### Return type

[**PatchSysctlprofileSuccess**](PatchSysctlprofileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching Sysctl profiles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patch_tunnel**
> PatchTunnelSuccess patch_tunnel(uid, patch_tunnel)

Patch a tunnel

Patch an existing tunnel. You just have to specify the attributes with changed values.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Tunnel uid.
patch_tunnel = r_s_waf_api_client.PatchTunnel() # PatchTunnel | 

    try:
        # Patch a tunnel
        api_response = api_instance.patch_tunnel(uid, patch_tunnel)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->patch_tunnel: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Tunnel uid. | 
 **patch_tunnel** | [**PatchTunnel**](PatchTunnel.md)|  | 

### Return type

[**PatchTunnelSuccess**](PatchTunnelSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching tunnels |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_log_filter**
> PutLogFilterSuccess put_log_filter(uid, body, content_md5=content_md5)

Upload LogFilter File

Upload the special logfilter file.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | logfilter's uid.
body = '/path/to/file' # file | 
content_md5 = 'content_md5_example' # str | The md5sum of uploaded file. To ensure that data is not corrupted traversing the network, user may use the header.                          <br />If you use cURL, specify <code>-H \"Content-MD5: b063ea6ddd4af4e20ab27e7fb8e62079\"</code>. (optional)

    try:
        # Upload LogFilter File
        api_response = api_instance.put_log_filter(uid, body, content_md5=content_md5)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->put_log_filter: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| logfilter&#39;s uid. | 
 **body** | **file**|  | 
 **content_md5** | **str**| The md5sum of uploaded file. To ensure that data is not corrupted traversing the network, user may use the header.                          &lt;br /&gt;If you use cURL, specify &lt;code&gt;-H \&quot;Content-MD5: b063ea6ddd4af4e20ab27e7fb8e62079\&quot;&lt;/code&gt;. | [optional] 

### Return type

[**PutLogFilterSuccess**](PutLogFilterSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: text/txt
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The updated logfilter |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setsslprofiles**
> SetsslprofilesSuccess setsslprofiles(uid, setsslprofiles)

Update a SSL Profile

Update the SSL Profile.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Select SSL Profile to update
setsslprofiles = r_s_waf_api_client.Setsslprofiles() # Setsslprofiles | 

    try:
        # Update a SSL Profile
        api_response = api_instance.setsslprofiles(uid, setsslprofiles)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->setsslprofiles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Select SSL Profile to update | 
 **setsslprofiles** | [**Setsslprofiles**](Setsslprofiles.md)|  | 

### Return type

[**SetsslprofilesSuccess**](SetsslprofilesSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing matching sslprofiles. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **shutdown**
> ShutdownSuccess shutdown(shutdown)

Shutdown/Halt the system

Shutdown/Halt the system

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    shutdown = r_s_waf_api_client.Shutdown() # Shutdown | 

    try:
        # Shutdown/Halt the system
        api_response = api_instance.shutdown(shutdown)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->shutdown: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shutdown** | [**Shutdown**](Shutdown.md)|  | 

### Return type

[**ShutdownSuccess**](ShutdownSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The result of the operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tunnel_backend_status**
> TunnelBackendStatusSuccess tunnel_backend_status(uid)

Get backend status

Get the backend status of a tunnel. You must specify a tunnel uid or a tunnel name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Tunnel uid or name.

    try:
        # Get backend status
        api_response = api_instance.tunnel_backend_status(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->tunnel_backend_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Tunnel uid or name. | 

### Return type

[**TunnelBackendStatusSuccess**](TunnelBackendStatusSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching status |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tunnel_listening_status**
> TunnelListeningStatusSuccess tunnel_listening_status(uid)

Get listening status

Get the listening status of a tunnel. You must specify a tunnel uid or a tunnel name.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Tunnel uid or name.

    try:
        # Get listening status
        api_response = api_instance.tunnel_listening_status(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->tunnel_listening_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Tunnel uid or name. | 

### Return type

[**TunnelListeningStatusSuccess**](TunnelListeningStatusSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tunnel_runtime_information**
> TunnelRuntimeInformationSuccess tunnel_runtime_information(uid)

Get runtime information

Get the runtime information of a tunnel. You must specify a tunnel uid or a tunnel name

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Tunnel uid or name.

    try:
        # Get runtime information
        api_response = api_instance.tunnel_runtime_information(uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->tunnel_runtime_information: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Tunnel uid or name. | 

### Return type

[**TunnelRuntimeInformationSuccess**](TunnelRuntimeInformationSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_accesslogprofile**
> UpdateAccesslogprofileSuccess update_accesslogprofile(update_accesslogprofile, uid=uid, name=name)

Update an access log profile

Update an access log profile.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    update_accesslogprofile = r_s_waf_api_client.UpdateAccesslogprofile() # UpdateAccesslogprofile | 
uid = 'uid_example' # str | Identifier of the access log profile. (optional)
name = 'name_example' # str | Name of the access log profile. (optional)

    try:
        # Update an access log profile
        api_response = api_instance.update_accesslogprofile(update_accesslogprofile, uid=uid, name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_accesslogprofile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **update_accesslogprofile** | [**UpdateAccesslogprofile**](UpdateAccesslogprofile.md)|  | 
 **uid** | **str**| Identifier of the access log profile. | [optional] 
 **name** | **str**| Name of the access log profile. | [optional] 

### Return type

[**UpdateAccesslogprofileSuccess**](UpdateAccesslogprofileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching access log profiles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_certificate**
> UpdateCertificateSuccess update_certificate(uid, content_md5=content_md5, password=password, type=type)

Update a certificate

Update the certificate.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Certificate's uid.
content_md5 = 'content_md5_example' # str | The md5sum of uploaded file. To ensure that data is not corrupted traversing the network, user may use the header.                          <br />If you use cURL, specify <code>-H \"Content-MD5: b063ea6ddd4af4e20ab27e7fb8e62079\"</code>. (optional)
password = 'password_example' # str | The password of the certificate (optional)
type = 'type_example' # str | The type of the certificate (optional)

    try:
        # Update a certificate
        api_response = api_instance.update_certificate(uid, content_md5=content_md5, password=password, type=type)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_certificate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Certificate&#39;s uid. | 
 **content_md5** | **str**| The md5sum of uploaded file. To ensure that data is not corrupted traversing the network, user may use the header.                          &lt;br /&gt;If you use cURL, specify &lt;code&gt;-H \&quot;Content-MD5: b063ea6ddd4af4e20ab27e7fb8e62079\&quot;&lt;/code&gt;. | [optional] 
 **password** | **str**| The password of the certificate | [optional] 
 **type** | **str**| The type of the certificate | [optional] 

### Return type

[**UpdateCertificateSuccess**](UpdateCertificateSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing new certificates. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_certificates_bundle**
> UpdateCertificatesBundleSuccess update_certificates_bundle(update_certificates_bundle, uid=uid, name=name)

Update a certificates bundle

Upload the special certificates bundle.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    update_certificates_bundle = r_s_waf_api_client.UpdateCertificatesBundle() # UpdateCertificatesBundle | 
uid = 'uid_example' # str | Identifier of the certificates bundle. (optional)
name = 'name_example' # str | Name of the certificates bundle. (optional)

    try:
        # Update a certificates bundle
        api_response = api_instance.update_certificates_bundle(update_certificates_bundle, uid=uid, name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_certificates_bundle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **update_certificates_bundle** | [**UpdateCertificatesBundle**](UpdateCertificatesBundle.md)|  | 
 **uid** | **str**| Identifier of the certificates bundle. | [optional] 
 **name** | **str**| Name of the certificates bundle. | [optional] 

### Return type

[**UpdateCertificatesBundleSuccess**](UpdateCertificatesBundleSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching certificates bundles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_compression_profile**
> UpdateCompressionProfileSuccess update_compression_profile(uid, update_compression_profile)

Update a compression profile

Update a existing compression profile

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Compression profile uid or name.
update_compression_profile = r_s_waf_api_client.UpdateCompressionProfile() # UpdateCompressionProfile | 

    try:
        # Update a compression profile
        api_response = api_instance.update_compression_profile(uid, update_compression_profile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_compression_profile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Compression profile uid or name. | 
 **update_compression_profile** | [**UpdateCompressionProfile**](UpdateCompressionProfile.md)|  | 

### Return type

[**UpdateCompressionProfileSuccess**](UpdateCompressionProfileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching compression profiles. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_icx**
> UpdateIcxSuccess update_icx(uid, update_icx)

Update an ICX configuration

Update the ICX configuration.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Identifier of the ICX configuration.
update_icx = r_s_waf_api_client.UpdateIcx() # UpdateIcx | 

    try:
        # Update an ICX configuration
        api_response = api_instance.update_icx(uid, update_icx)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_icx: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Identifier of the ICX configuration. | 
 **update_icx** | [**UpdateIcx**](UpdateIcx.md)|  | 

### Return type

[**UpdateIcxSuccess**](UpdateIcxSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching ICX configurations |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_label**
> UpdateLabelSuccess update_label(uid, update_label)

Update a label

Upload the special label.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Identifier of the label.
update_label = r_s_waf_api_client.UpdateLabel() # UpdateLabel | 

    try:
        # Update a label
        api_response = api_instance.update_label(uid, update_label)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_label: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Identifier of the label. | 
 **update_label** | [**UpdateLabel**](UpdateLabel.md)|  | 

### Return type

[**UpdateLabelSuccess**](UpdateLabelSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching labels |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_network_interface**
> UpdateNetworkInterfaceSuccess update_network_interface(name, update_network_interface)

Update a network interface

Update a existing network interface

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Identifier of the network interface.
update_network_interface = r_s_waf_api_client.UpdateNetworkInterface() # UpdateNetworkInterface | 

    try:
        # Update a network interface
        api_response = api_instance.update_network_interface(name, update_network_interface)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_network_interface: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Identifier of the network interface. | 
 **update_network_interface** | [**UpdateNetworkInterface**](UpdateNetworkInterface.md)|  | 

### Return type

[**UpdateNetworkInterfaceSuccess**](UpdateNetworkInterfaceSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object the matching network interface. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_ntp**
> UpdateNtpSuccess update_ntp(uid, update_ntp)

Update a ntp

Upload the special ntp.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Identifier of the ntp.
update_ntp = r_s_waf_api_client.UpdateNtp() # UpdateNtp | 

    try:
        # Update a ntp
        api_response = api_instance.update_ntp(uid, update_ntp)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_ntp: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Identifier of the ntp. | 
 **update_ntp** | [**UpdateNtp**](UpdateNtp.md)|  | 

### Return type

[**UpdateNtpSuccess**](UpdateNtpSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching ntps |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_ramdisk_cache**
> UpdateRamdiskCacheSuccess update_ramdisk_cache(uid, update_ramdisk_cache)

Update a Ramdisk cache profile

Upload the Ramdisk cache profile.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Identifier of the Ramdisk cache profile.
update_ramdisk_cache = r_s_waf_api_client.UpdateRamdiskCache() # UpdateRamdiskCache | 

    try:
        # Update a Ramdisk cache profile
        api_response = api_instance.update_ramdisk_cache(uid, update_ramdisk_cache)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_ramdisk_cache: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Identifier of the Ramdisk cache profile. | 
 **update_ramdisk_cache** | [**UpdateRamdiskCache**](UpdateRamdiskCache.md)|  | 

### Return type

[**UpdateRamdiskCacheSuccess**](UpdateRamdiskCacheSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching Ramdisk cache profile. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_requesttimeoutprofile**
> UpdateRequesttimeoutprofileSuccess update_requesttimeoutprofile(uid, update_requesttimeoutprofile)

Update an request timeout profile

Update an request timeout profile.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Identifier of the request timeout profile.
update_requesttimeoutprofile = r_s_waf_api_client.UpdateRequesttimeoutprofile() # UpdateRequesttimeoutprofile | 

    try:
        # Update an request timeout profile
        api_response = api_instance.update_requesttimeoutprofile(uid, update_requesttimeoutprofile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_requesttimeoutprofile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Identifier of the request timeout profile. | 
 **update_requesttimeoutprofile** | [**UpdateRequesttimeoutprofile**](UpdateRequesttimeoutprofile.md)|  | 

### Return type

[**UpdateRequesttimeoutprofileSuccess**](UpdateRequesttimeoutprofileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching request timeout profiles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_reverse_proxy**
> UpdateReverseProxySuccess update_reverse_proxy(uid, update_reverse_proxy)

Update a reverse proxy

Update a existing reverse proxy

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Identifier of the reverse proxy.
update_reverse_proxy = r_s_waf_api_client.UpdateReverseProxy() # UpdateReverseProxy | 

    try:
        # Update a reverse proxy
        api_response = api_instance.update_reverse_proxy(uid, update_reverse_proxy)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_reverse_proxy: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Identifier of the reverse proxy. | 
 **update_reverse_proxy** | [**UpdateReverseProxy**](UpdateReverseProxy.md)|  | 

### Return type

[**UpdateReverseProxySuccess**](UpdateReverseProxySuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing operation result. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_reverse_proxy_profile**
> UpdateReverseProxyProfileSuccess update_reverse_proxy_profile(uid, update_reverse_proxy_profile)

Update a reverse proxy profile

Update a existing reverse proxy profile

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Reverse proxy profile uid or name.
update_reverse_proxy_profile = r_s_waf_api_client.UpdateReverseProxyProfile() # UpdateReverseProxyProfile | 

    try:
        # Update a reverse proxy profile
        api_response = api_instance.update_reverse_proxy_profile(uid, update_reverse_proxy_profile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_reverse_proxy_profile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Reverse proxy profile uid or name. | 
 **update_reverse_proxy_profile** | [**UpdateReverseProxyProfile**](UpdateReverseProxyProfile.md)|  | 

### Return type

[**UpdateReverseProxyProfileSuccess**](UpdateReverseProxyProfileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing matching reverse proxy profiles. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_scoringlist**
> UpdateScoringlistSuccess update_scoringlist(uid, update_scoringlist)

Update a scoringlist configuration

Update the special scoringlist configuration.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Identifier of the scoringlist configuration.
update_scoringlist = r_s_waf_api_client.UpdateScoringlist() # UpdateScoringlist | 

    try:
        # Update a scoringlist configuration
        api_response = api_instance.update_scoringlist(uid, update_scoringlist)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_scoringlist: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Identifier of the scoringlist configuration. | 
 **update_scoringlist** | [**UpdateScoringlist**](UpdateScoringlist.md)|  | 

### Return type

[**UpdateScoringlistSuccess**](UpdateScoringlistSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching scoringlist configuration |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_scoringlist_exception**
> UpdateScoringlistExceptionSuccess update_scoringlist_exception(uid, update_scoringlist_exception, scoringlist_uid=scoringlist_uid)

Update a scoringlist exception

Update the special scoringlist exception.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | Uid of the new scoringlist exception.
update_scoringlist_exception = r_s_waf_api_client.UpdateScoringlistException() # UpdateScoringlistException | 
scoringlist_uid = 'scoringlist_uid_example' # str | Scoringlist uid. (optional)

    try:
        # Update a scoringlist exception
        api_response = api_instance.update_scoringlist_exception(uid, update_scoringlist_exception, scoringlist_uid=scoringlist_uid)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_scoringlist_exception: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| Uid of the new scoringlist exception. | 
 **update_scoringlist_exception** | [**UpdateScoringlistException**](UpdateScoringlistException.md)|  | 
 **scoringlist_uid** | **str**| Scoringlist uid. | [optional] 

### Return type

[**UpdateScoringlistExceptionSuccess**](UpdateScoringlistExceptionSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The matching scoringlist exceptions |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_security_exception_profile**
> UpdateSecurityExceptionProfileSuccess update_security_exception_profile(uid, update_security_exception_profile)

Update a Security Exception Profile

Update a security exception profile.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    uid = 'uid_example' # str | security exception profile uid or name.
update_security_exception_profile = r_s_waf_api_client.UpdateSecurityExceptionProfile() # UpdateSecurityExceptionProfile | 

    try:
        # Update a Security Exception Profile
        api_response = api_instance.update_security_exception_profile(uid, update_security_exception_profile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_security_exception_profile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**| security exception profile uid or name. | 
 **update_security_exception_profile** | [**UpdateSecurityExceptionProfile**](UpdateSecurityExceptionProfile.md)|  | 

### Return type

[**UpdateSecurityExceptionProfileSuccess**](UpdateSecurityExceptionProfileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The created security exception profile data |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_sysctlprofile**
> UpdateSysctlprofileSuccess update_sysctlprofile(name, update_sysctlprofile)

Update a Sysctl profile

Upload the special Sysctl profile.

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    name = 'name_example' # str | Identifier of the Sysctl profile.
update_sysctlprofile = r_s_waf_api_client.UpdateSysctlprofile() # UpdateSysctlprofile | 

    try:
        # Update a Sysctl profile
        api_response = api_instance.update_sysctlprofile(name, update_sysctlprofile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_sysctlprofile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Identifier of the Sysctl profile. | 
 **update_sysctlprofile** | [**UpdateSysctlprofile**](UpdateSysctlprofile.md)|  | 

### Return type

[**UpdateSysctlprofileSuccess**](UpdateSysctlprofileSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching Sysctl profiles |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_tunnel**
> UpdateTunnelSuccess update_tunnel(update_tunnel, uid=uid, name=name)

Update a tunnel

Update an existing tunnel

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    update_tunnel = r_s_waf_api_client.UpdateTunnel() # UpdateTunnel | 
uid = 'uid_example' # str | Tunnel uid. (optional)
name = 'name_example' # str | Tunnel name. (optional)

    try:
        # Update a tunnel
        api_response = api_instance.update_tunnel(update_tunnel, uid=uid, name=name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_tunnel: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **update_tunnel** | [**UpdateTunnel**](UpdateTunnel.md)|  | 
 **uid** | **str**| Tunnel uid. | [optional] 
 **name** | **str**| Tunnel name. | [optional] 

### Return type

[**UpdateTunnelSuccess**](UpdateTunnelSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Object containing the matching tunnels |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upload_license**
> UploadLicenseSuccess upload_license(content_md5=content_md5, upload=upload)

Upload a license file

Upload a license file

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import r_s_waf_api_client
from r_s_waf_api_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://ManagementWAF:3001/wafapi
# See configuration.py for a list of all supported configuration parameters.
configuration = r_s_waf_api_client.Configuration(
    host = "https://ManagementWAF:3001/wafapi"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = r_s_waf_api_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with r_s_waf_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = r_s_waf_api_client.DefaultApi(api_client)
    content_md5 = 'content_md5_example' # str | The md5sum of uploaded file. If you use cURL, specify <code>-H \"Content-MD5: b063ea6ddd4af4e20ab27e7fb8e62079\"</code> (optional)
upload = '/path/to/file' # file |  (optional)

    try:
        # Upload a license file
        api_response = api_instance.upload_license(content_md5=content_md5, upload=upload)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->upload_license: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **content_md5** | **str**| The md5sum of uploaded file. If you use cURL, specify &lt;code&gt;-H \&quot;Content-MD5: b063ea6ddd4af4e20ab27e7fb8e62079\&quot;&lt;/code&gt; | [optional] 
 **upload** | **file**|  | [optional] 

### Return type

[**UploadLicenseSuccess**](UploadLicenseSuccess.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | license object. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

