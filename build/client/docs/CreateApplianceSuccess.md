# CreateApplianceSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**CreateApplianceSuccessData**](CreateApplianceSuccessData.md) |  | [optional] 
**data_ssh_interfaces** | [**list[CreateApplianceInterfacesSuccessArray]**](CreateApplianceInterfacesSuccessArray.md) |  | [optional] 
**data_snmp_interfaces** | [**list[CreateApplianceInterfacesSuccessArray]**](CreateApplianceInterfacesSuccessArray.md) |  | [optional] 
**data_network_devices** | [**list[CreateApplianceNetworkDevicesSuccessArray]**](CreateApplianceNetworkDevicesSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


