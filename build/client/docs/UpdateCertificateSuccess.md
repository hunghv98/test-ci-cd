# UpdateCertificateSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**UpdateCertificateSuccessData**](UpdateCertificateSuccessData.md) |  | [optional] 
**data_labels** | [**list[UpdateCertificateLabelsSuccessArray]**](UpdateCertificateLabelsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


