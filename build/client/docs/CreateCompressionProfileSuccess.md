# CreateCompressionProfileSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**CreateCompressionProfileSuccessData**](CreateCompressionProfileSuccessData.md) |  | [optional] 
**data_used_by** | [**list[CreateCompressionProfileUsedBySuccessArray]**](CreateCompressionProfileUsedBySuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


