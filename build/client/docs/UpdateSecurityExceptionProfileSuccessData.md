# UpdateSecurityExceptionProfileSuccessData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**read_only** | **bool** |  | [optional] 
**description** | **str** |  | [optional] 
**rules** | [**list[UpdateSecurityExceptionProfileRulesSuccessArray]**](UpdateSecurityExceptionProfileRulesSuccessArray.md) |  | [optional] 
**t_update** | **float** |  | [optional] 
**t_create** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


