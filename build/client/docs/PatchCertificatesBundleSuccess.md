# PatchCertificatesBundleSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**PatchCertificatesBundleSuccessData**](PatchCertificatesBundleSuccessData.md) |  | [optional] 
**data_ca** | [**list[PatchCertificatesBundleCaSuccessArray]**](PatchCertificatesBundleCaSuccessArray.md) |  | [optional] 
**data_ca_ocsp** | [**list[PatchCertificatesBundleCaOCSPSuccessArray]**](PatchCertificatesBundleCaOCSPSuccessArray.md) |  | [optional] 
**data_crl** | [**list[PatchCertificatesBundleCrlSuccessArray]**](PatchCertificatesBundleCrlSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


