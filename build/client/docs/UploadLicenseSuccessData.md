# UploadLicenseSuccessData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**edition** | **str** |  | [optional] 
**serial_number** | **str** |  | [optional] 
**dedicated_management** | **bool** |  | [optional] 
**expiration** | **float** |  | [optional] 
**max_managed** | **str** |  | [optional] 
**modules** | [**GetLicensesDataSuccessArrayModules**](GetLicensesDataSuccessArrayModules.md) |  | [optional] 
**number_of** | [**GetLicensesDataSuccessArrayNumberOf**](GetLicensesDataSuccessArrayNumberOf.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


