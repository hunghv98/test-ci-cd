# UpdateScoringlistSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**UpdateScoringlistSuccessData**](UpdateScoringlistSuccessData.md) |  | [optional] 
**data_exceptions** | [**list[UpdateScoringlistExceptionsSuccessArray]**](UpdateScoringlistExceptionsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


