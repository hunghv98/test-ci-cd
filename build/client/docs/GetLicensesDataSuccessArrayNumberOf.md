# GetLicensesDataSuccessArrayNumberOf

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reverse_proxies** | **int** |  | [optional] 
**tunnels** | **int** |  | [optional] 
**max_client** | **int** |  | [optional] 
**wam_session** | **int** |  | [optional] 
**cpu** | **int** |  | [optional] 
**ram** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


