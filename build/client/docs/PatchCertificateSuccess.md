# PatchCertificateSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**PatchCertificateSuccessData**](PatchCertificateSuccessData.md) |  | [optional] 
**data_labels** | [**list[PatchCertificateLabelsSuccessArray]**](PatchCertificateLabelsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


