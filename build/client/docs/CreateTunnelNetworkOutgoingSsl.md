# CreateTunnelNetworkOutgoingSsl

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profile** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 
**certificate** | [**object**](.md) |  | [optional] 
**verify_backend_certificate** | [**CreateTunnelNetworkOutgoingSslVerifyBackendCertificate**](CreateTunnelNetworkOutgoingSslVerifyBackendCertificate.md) |  | [optional] 
**ajp_enable** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


