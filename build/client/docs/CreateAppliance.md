# CreateAppliance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**admin_ip** | **str** |  | [optional] 
**admin_port** | **int** |  | [optional] 
**location** | **str** |  | [optional] 
**contact** | **str** |  | [optional] 
**sysctl_profile** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


