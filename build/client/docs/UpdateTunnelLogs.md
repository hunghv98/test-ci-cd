# UpdateTunnelLogs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access** | [**CreateTunnelLogsAccess**](CreateTunnelLogsAccess.md) |  | [optional] 
**debug** | **bool** |  | [optional] 
**filter** | **str** |  | [optional] 
**realtime** | [**UpdateTunnelLogsRealtime**](UpdateTunnelLogsRealtime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


