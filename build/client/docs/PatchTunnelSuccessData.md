# PatchTunnelSuccessData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**network** | [**PatchTunnelSuccessDataNetwork**](PatchTunnelSuccessDataNetwork.md) |  | [optional] 
**workflow** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**workflow_parameters** | **list[object]** |  | [optional] 
**reverse_proxy** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**advanced** | [**CreateTunnelAdvanced**](CreateTunnelAdvanced.md) |  | [optional] 
**t_update** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


