# CreateTunnelLogsAccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **bool** |  | [optional] 
**file_format_profile** | **str** |  | [optional] 
**database** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


