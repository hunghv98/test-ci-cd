# GetReverseProxiesSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list[GetReverseProxiesDataSuccessArray]**](GetReverseProxiesDataSuccessArray.md) |  | [optional] 
**data_labels** | [**list[GetReverseProxiesLabelsSuccessArray]**](GetReverseProxiesLabelsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


