# GetNetworkDevicesDataSuccessArray

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appliance** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**mac** | **str** |  | [optional] 
**mtu** | **str** |  | [optional] 
**autoneg** | **str** |  | [optional] 
**duplex** | **str** |  | [optional] 
**speed** | **str** |  | [optional] 
**interfaces** | **list[object]** |  | [optional] 
**used_by** | [**list[GetNetworkDevicesUsedBySuccessArray]**](GetNetworkDevicesUsedBySuccessArray.md) |  | [optional] 
**t_update** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


