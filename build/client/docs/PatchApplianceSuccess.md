# PatchApplianceSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**PatchApplianceSuccessData**](PatchApplianceSuccessData.md) |  | [optional] 
**data_ssh_interfaces** | [**list[PatchApplianceInterfacesSuccessArray]**](PatchApplianceInterfacesSuccessArray.md) |  | [optional] 
**data_snmp_interfaces** | [**list[PatchApplianceInterfacesSuccessArray]**](PatchApplianceInterfacesSuccessArray.md) |  | [optional] 
**data_network_devices** | [**list[PatchApplianceNetworkDevicesSuccessArray]**](PatchApplianceNetworkDevicesSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


