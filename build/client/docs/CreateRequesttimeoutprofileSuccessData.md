# CreateRequesttimeoutprofileSuccessData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**header_timeout** | **str** |  | [optional] 
**body_timeout** | **str** |  | [optional] 
**header_rate** | **int** |  | [optional] 
**body_rate** | **int** |  | [optional] 
**t_update** | **float** |  | [optional] 
**t_create** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


