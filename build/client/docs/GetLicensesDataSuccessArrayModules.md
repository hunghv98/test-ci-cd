# GetLicensesDataSuccessArrayModules

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**security** | **bool** |  | [optional] 
**security_extended** | **bool** |  | [optional] 
**api** | **bool** |  | [optional] 
**api_extended** | **bool** |  | [optional] 
**geolocation** | **bool** |  | [optional] 
**analytics** | **bool** |  | [optional] 
**wam** | **bool** |  | [optional] 
**distributed** | **bool** |  | [optional] 
**ip_reputation** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


