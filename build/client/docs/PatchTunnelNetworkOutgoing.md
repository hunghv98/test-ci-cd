# PatchTunnelNetworkOutgoing

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **str** |  | [optional] 
**port** | **int** |  | [optional] 
**load_balancer** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 
**pooling_interface** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 
**pooling_port** | **int** |  | [optional] 
**ssl** | [**CreateTunnelNetworkOutgoingSsl**](CreateTunnelNetworkOutgoingSsl.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


