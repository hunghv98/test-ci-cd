# UpdateReverseProxySuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**UpdateReverseProxySuccessData**](UpdateReverseProxySuccessData.md) |  | [optional] 
**data_labels** | [**list[UpdateReverseProxyLabelsSuccessArray]**](UpdateReverseProxyLabelsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


