# CreateCertificateSuccessData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**private_key_name** | **str** |  | [optional] 
**certificate_name** | **str** |  | [optional] 
**chain_name** | **str** |  | [optional] 
**csr_name** | **str** |  | [optional] 
**expiration_date** | **int** |  | [optional] 
**labels** | [**list[CreateCertificateLabelsSuccessArray]**](CreateCertificateLabelsSuccessArray.md) |  | [optional] 
**t_update** | **float** |  | [optional] 
**t_create** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


