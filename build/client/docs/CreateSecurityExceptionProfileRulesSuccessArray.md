# CreateSecurityExceptionProfileRulesSuccessArray

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**description** | **str** |  | [optional] 
**conditions** | [**list[CreateSecurityExceptionProfileConditionsSuccessArray]**](CreateSecurityExceptionProfileConditionsSuccessArray.md) |  | [optional] 
**matching_parts** | [**list[CreateSecurityExceptionProfileMatchingPartsSuccessArray]**](CreateSecurityExceptionProfileMatchingPartsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


