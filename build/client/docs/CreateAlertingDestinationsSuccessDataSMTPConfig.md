# CreateAlertingDestinationsSuccessDataSMTPConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ip** | **str** |  | [optional] 
**port** | **int** |  | [optional] 
**login** | **str** |  | [optional] 
**_pass** | **str** |  | [optional] 
**mail_from** | **str** |  | [optional] 
**mail_to** | **str** |  | [optional] 
**mail_crypt_protocol** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


