# UpdateNetworkInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**device** | [**UpdateNetworkInterfaceDevice**](UpdateNetworkInterfaceDevice.md) |  | [optional] 
**ip** | **str** |  | [optional] 
**netmask** | **str** |  | [optional] 
**active** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


