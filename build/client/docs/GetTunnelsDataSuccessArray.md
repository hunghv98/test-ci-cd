# GetTunnelsDataSuccessArray

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**reverse_proxy** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**workflow** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**workflow_parameters** | [**list[GetTunnelsWorkflowParametersSuccessArray]**](GetTunnelsWorkflowParametersSuccessArray.md) |  | [optional] 
**enabled** | **str** |  | [optional] 
**labels** | [**list[GetTunnelsLabelsSuccessArray]**](GetTunnelsLabelsSuccessArray.md) |  | [optional] 
**network** | [**GetTunnelsDataSuccessArrayNetwork**](GetTunnelsDataSuccessArrayNetwork.md) |  | [optional] 
**secondary_tunnels** | [**list[GetTunnelsSecondaryTunnelsSuccessArray]**](GetTunnelsSecondaryTunnelsSuccessArray.md) |  | [optional] 
**appliance** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**advanced** | [**CreateTunnelAdvanced**](CreateTunnelAdvanced.md) |  | [optional] 
**t_update** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


