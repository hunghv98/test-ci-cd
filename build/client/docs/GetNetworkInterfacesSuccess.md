# GetNetworkInterfacesSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list[GetNetworkInterfacesDataSuccessArray]**](GetNetworkInterfacesDataSuccessArray.md) |  | [optional] 
**data_used_by** | [**list[GetNetworkInterfacesUsedBySuccessArray]**](GetNetworkInterfacesUsedBySuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


