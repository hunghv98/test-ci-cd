# PatchApplianceSuccessData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**role** | **str** |  | [optional] 
**autoscale** | **bool** |  | [optional] 
**admin_ip** | **str** |  | [optional] 
**admin_port** | **int** |  | [optional] 
**location** | **str** |  | [optional] 
**contact** | **str** |  | [optional] 
**os_version** | **str** |  | [optional] 
**ssh** | [**PatchApplianceSuccessDataSsh**](PatchApplianceSuccessDataSsh.md) |  | [optional] 
**dns** | [**CreateApplianceSuccessDataDns**](CreateApplianceSuccessDataDns.md) |  | [optional] 
**snmp** | [**PatchApplianceSuccessDataSnmp**](PatchApplianceSuccessDataSnmp.md) |  | [optional] 
**hosts** | [**CreateApplianceSuccessDataHosts**](CreateApplianceSuccessDataHosts.md) |  | [optional] 
**network_devices** | [**list[PatchApplianceNetworkDevicesSuccessArray]**](PatchApplianceNetworkDevicesSuccessArray.md) |  | [optional] 
**sysctl_profile** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**t_update** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


