# UpdateSecurityExceptionProfileSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**UpdateSecurityExceptionProfileSuccessData**](UpdateSecurityExceptionProfileSuccessData.md) |  | [optional] 
**data_rules** | [**list[UpdateSecurityExceptionProfileRulesSuccessArray]**](UpdateSecurityExceptionProfileRulesSuccessArray.md) |  | [optional] 
**data_rules_conditions** | [**list[UpdateSecurityExceptionProfileConditionsSuccessArray]**](UpdateSecurityExceptionProfileConditionsSuccessArray.md) |  | [optional] 
**data_rules_matching_parts** | [**list[UpdateSecurityExceptionProfileMatchingPartsSuccessArray]**](UpdateSecurityExceptionProfileMatchingPartsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


