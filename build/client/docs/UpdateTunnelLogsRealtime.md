# UpdateTunnelLogsRealtime

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**security** | **bool** |  | [optional] 
**security_format** | **str** |  | [optional] 
**wam_format** | **str** |  | [optional] 
**wam** | **bool** |  | [optional] 
**error** | **bool** |  | [optional] 
**access** | **bool** |  | [optional] 
**access_log_database** | **bool** |  | [optional] 
**syslog_destination_profiles** | [**list[UpdateTunnelSyslogDestinationProfilesArray]**](UpdateTunnelSyslogDestinationProfilesArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


