# UpdateCertificatesBundleSuccessData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**ca** | [**list[UpdateCertificatesBundleCaSuccessArray]**](UpdateCertificatesBundleCaSuccessArray.md) |  | [optional] 
**ca_ocsp** | [**list[UpdateCertificatesBundleCaOCSPSuccessArray]**](UpdateCertificatesBundleCaOCSPSuccessArray.md) |  | [optional] 
**crl** | [**list[UpdateCertificatesBundleCrlSuccessArray]**](UpdateCertificatesBundleCrlSuccessArray.md) |  | [optional] 
**t_update** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


