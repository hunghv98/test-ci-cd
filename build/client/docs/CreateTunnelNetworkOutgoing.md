# CreateTunnelNetworkOutgoing

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**load_balancer** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 
**address** | **str** |  | [optional] 
**port** | **int** |  | [optional] 
**pooling_interface** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 
**pooling_port** | **int** |  | [optional] 
**ssl** | [**CreateTunnelNetworkOutgoingSsl**](CreateTunnelNetworkOutgoingSsl.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


