# GetNetworkDevicesSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list[GetNetworkDevicesDataSuccessArray]**](GetNetworkDevicesDataSuccessArray.md) |  | [optional] 
**data_interfaces** | **list[object]** |  | [optional] 
**data_used_by** | [**list[GetNetworkDevicesUsedBySuccessArray]**](GetNetworkDevicesUsedBySuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


