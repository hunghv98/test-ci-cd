# PatchReverseProxySuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**PatchReverseProxySuccessData**](PatchReverseProxySuccessData.md) |  | [optional] 
**data_labels** | [**list[PatchReverseProxyLabelsSuccessArray]**](PatchReverseProxyLabelsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


