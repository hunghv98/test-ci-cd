# UpdateRamdiskCache

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**cache_ignore_cache_control** | **bool** |  | [optional] 
**cache_ignore_no_last_mod** | **bool** |  | [optional] 
**cache_ignore_query_string** | **bool** |  | [optional] 
**cache_store_no_store** | **bool** |  | [optional] 
**cache_store_private** | **bool** |  | [optional] 
**cache_default_expire** | **int** |  | [optional] 
**cache_ignore_headers** | **str** |  | [optional] 
**cache_ignore_url_session_identifiers** | **str** |  | [optional] 
**cache_last_modified_factor** | **float** |  | [optional] 
**cache_max_expire** | **int** |  | [optional] 
**cache_dir_length** | **int** |  | [optional] 
**cache_dir_levels** | **int** |  | [optional] 
**cache_max_file_size** | **int** |  | [optional] 
**cache_min_file_size** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


