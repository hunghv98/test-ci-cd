# PatchTunnel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**reverse_proxy** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 
**workflow** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**workflow_parameters** | **list[object]** |  | [optional] 
**application_template** | **str** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**labels** | [**list[PatchTunnelLabelsArray]**](PatchTunnelLabelsArray.md) |  | [optional] 
**network** | [**PatchTunnelNetwork**](PatchTunnelNetwork.md) |  | [optional] 
**network_incoming_server_alias** | **list[object]** |  | [optional] 
**performance** | [**CreateTunnelPerformance**](CreateTunnelPerformance.md) |  | [optional] 
**logs** | [**PatchTunnelLogs**](PatchTunnelLogs.md) |  | [optional] 
**logs_realtime_syslog_destination_profiles** | [**list[PatchTunnelSyslogDestinationProfilesArray]**](PatchTunnelSyslogDestinationProfilesArray.md) |  | [optional] 
**monitor** | [**CreateTunnelMonitor**](CreateTunnelMonitor.md) |  | [optional] 
**advanced** | [**CreateTunnelAdvanced**](CreateTunnelAdvanced.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


