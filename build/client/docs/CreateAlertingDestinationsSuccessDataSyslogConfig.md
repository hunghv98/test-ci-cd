# CreateAlertingDestinationsSuccessDataSyslogConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ip** | **str** |  | [optional] 
**port** | **int** |  | [optional] 
**protocol** | **str** |  | [optional] 
**severity** | **int** |  | [optional] 
**facility** | **int** |  | [optional] 
**format** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


