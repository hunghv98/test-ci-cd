# PatchSecurityExceptionProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**rules** | [**list[PatchSecurityExceptionProfileRulesArray]**](PatchSecurityExceptionProfileRulesArray.md) |  | [optional] 
**rules_conditions** | [**list[PatchSecurityExceptionProfileConditionsArray]**](PatchSecurityExceptionProfileConditionsArray.md) |  | [optional] 
**rules_matching_parts** | [**list[PatchSecurityExceptionProfileMatchingPartsArray]**](PatchSecurityExceptionProfileMatchingPartsArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


