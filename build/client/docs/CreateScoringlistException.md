# CreateScoringlistException

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scoringlist_uid** | **str** |  | [optional] 
**rule_uid** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**path** | **str** |  | [optional] 
**case_sensitive** | **bool** |  | [optional] 
**negative** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


