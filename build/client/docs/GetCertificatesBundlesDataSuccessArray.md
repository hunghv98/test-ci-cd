# GetCertificatesBundlesDataSuccessArray

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**ca** | [**list[GetCertificatesBundlesCaSuccessArray]**](GetCertificatesBundlesCaSuccessArray.md) |  | [optional] 
**ca_ocsp** | [**list[GetCertificatesBundlesCaOCSPSuccessArray]**](GetCertificatesBundlesCaOCSPSuccessArray.md) |  | [optional] 
**crl** | [**list[GetCertificatesBundlesCrlSuccessArray]**](GetCertificatesBundlesCrlSuccessArray.md) |  | [optional] 
**t_update** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


