# CreateLogfilterSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**CreateLogfilterSuccessData**](CreateLogfilterSuccessData.md) |  | [optional] 
**data_used_by** | [**list[CreateLogfilterUsedBySuccessArray]**](CreateLogfilterUsedBySuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


