# UpdateAccesslogprofileSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**UpdateAccesslogprofileSuccessData**](UpdateAccesslogprofileSuccessData.md) |  | [optional] 
**data_used_by** | [**list[UpdateAccesslogprofileUsedBySuccessArray]**](UpdateAccesslogprofileUsedBySuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


