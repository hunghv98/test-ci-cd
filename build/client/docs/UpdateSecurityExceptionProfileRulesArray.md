# UpdateSecurityExceptionProfileRulesArray

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**description** | **str** |  | [optional] 
**conditions** | [**list[UpdateSecurityExceptionProfileConditionsArray]**](UpdateSecurityExceptionProfileConditionsArray.md) |  | [optional] 
**matching_parts** | [**list[UpdateSecurityExceptionProfileMatchingPartsArray]**](UpdateSecurityExceptionProfileMatchingPartsArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


