# UpdateCertificatesBundleSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**UpdateCertificatesBundleSuccessData**](UpdateCertificatesBundleSuccessData.md) |  | [optional] 
**data_ca** | [**list[UpdateCertificatesBundleCaSuccessArray]**](UpdateCertificatesBundleCaSuccessArray.md) |  | [optional] 
**data_ca_ocsp** | [**list[UpdateCertificatesBundleCaOCSPSuccessArray]**](UpdateCertificatesBundleCaOCSPSuccessArray.md) |  | [optional] 
**data_crl** | [**list[UpdateCertificatesBundleCrlSuccessArray]**](UpdateCertificatesBundleCrlSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


