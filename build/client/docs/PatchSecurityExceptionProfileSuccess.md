# PatchSecurityExceptionProfileSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**PatchSecurityExceptionProfileSuccessData**](PatchSecurityExceptionProfileSuccessData.md) |  | [optional] 
**data_rules** | [**list[PatchSecurityExceptionProfileRulesSuccessArray]**](PatchSecurityExceptionProfileRulesSuccessArray.md) |  | [optional] 
**data_rules_conditions** | [**list[PatchSecurityExceptionProfileConditionsSuccessArray]**](PatchSecurityExceptionProfileConditionsSuccessArray.md) |  | [optional] 
**data_rules_matching_parts** | [**list[PatchSecurityExceptionProfileMatchingPartsSuccessArray]**](PatchSecurityExceptionProfileMatchingPartsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


