# CreateTunnelPerformance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timeout** | **int** |  | [optional] 
**proxy_timeout** | **int** |  | [optional] 
**keep_alive_timeout** | **int** |  | [optional] 
**ramdisk_cache** | [**CreateTunnelPerformanceRamdiskCache**](CreateTunnelPerformanceRamdiskCache.md) |  | [optional] 
**request_timeout_profile** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 
**compression_profile** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 
**workflow_preserve_deflate** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


