# CreateSecurityExceptionProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**rules** | [**list[CreateSecurityExceptionProfileRulesArray]**](CreateSecurityExceptionProfileRulesArray.md) |  | [optional] 
**rules_conditions** | [**list[CreateSecurityExceptionProfileConditionsArray]**](CreateSecurityExceptionProfileConditionsArray.md) |  | [optional] 
**rules_matching_parts** | [**list[CreateSecurityExceptionProfileMatchingPartsArray]**](CreateSecurityExceptionProfileMatchingPartsArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


