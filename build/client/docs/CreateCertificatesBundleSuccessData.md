# CreateCertificatesBundleSuccessData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**ca** | [**list[CreateCertificatesBundleCaSuccessArray]**](CreateCertificatesBundleCaSuccessArray.md) |  | [optional] 
**ca_ocsp** | [**list[CreateCertificatesBundleCaOCSPSuccessArray]**](CreateCertificatesBundleCaOCSPSuccessArray.md) |  | [optional] 
**crl** | [**list[CreateCertificatesBundleCrlSuccessArray]**](CreateCertificatesBundleCrlSuccessArray.md) |  | [optional] 
**t_update** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


