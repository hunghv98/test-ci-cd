# CreateTunnelPerformanceRamdiskCache

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profile** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 
**enabled_urls** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 
**disabled_urls** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


