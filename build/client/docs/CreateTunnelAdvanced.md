# CreateTunnelAdvanced

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**advanced_parameters** | [**ApplySSLKeyUid**](ApplySSLKeyUid.md) |  | [optional] 
**workflow_body** | **bool** |  | [optional] 
**workflow_url_decode_body_plus_as_space** | **bool** |  | [optional] 
**geo_ip_enabled** | **bool** |  | [optional] 
**limit_request_body** | **int** |  | [optional] 
**priority** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


