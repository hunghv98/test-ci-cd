# GetTunnelsSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list[GetTunnelsDataSuccessArray]**](GetTunnelsDataSuccessArray.md) |  | [optional] 
**data_workflow_parameters** | [**list[GetTunnelsWorkflowParametersSuccessArray]**](GetTunnelsWorkflowParametersSuccessArray.md) |  | [optional] 
**data_labels** | [**list[GetTunnelsLabelsSuccessArray]**](GetTunnelsLabelsSuccessArray.md) |  | [optional] 
**data_network_incoming_server_alias** | **list[object]** |  | [optional] 
**data_secondary_tunnels** | [**list[GetTunnelsSecondaryTunnelsSuccessArray]**](GetTunnelsSecondaryTunnelsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


