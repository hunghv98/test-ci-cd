# GetCertificatesSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list[GetCertificatesDataSuccessArray]**](GetCertificatesDataSuccessArray.md) |  | [optional] 
**data_labels** | [**list[GetCertificatesLabelsSuccessArray]**](GetCertificatesLabelsSuccessArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


