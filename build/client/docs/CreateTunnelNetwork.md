# CreateTunnelNetwork

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**incoming** | [**CreateTunnelNetworkIncoming**](CreateTunnelNetworkIncoming.md) |  | 
**outgoing** | [**CreateTunnelNetworkOutgoing**](CreateTunnelNetworkOutgoing.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


