# CreateAlertingDestinationsSuccessData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**smtp_config** | [**CreateAlertingDestinationsSuccessDataSMTPConfig**](CreateAlertingDestinationsSuccessDataSMTPConfig.md) |  | [optional] 
**syslog_config** | [**CreateAlertingDestinationsSuccessDataSyslogConfig**](CreateAlertingDestinationsSuccessDataSyslogConfig.md) |  | [optional] 
**snmp_config** | [**CreateAlertingDestinationsSuccessDataSNMPConfig**](CreateAlertingDestinationsSuccessDataSNMPConfig.md) |  | [optional] 
**t_update** | **float** |  | [optional] 
**t_create** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


