# UpdateScoringlist

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**static_scoringlist** | [**CreateApplianceSysctlProfile**](CreateApplianceSysctlProfile.md) |  | [optional] 
**xss** | **str** |  | [optional] 
**sqli** | **str** |  | [optional] 
**fi** | **str** |  | [optional] 
**cmdi** | **str** |  | [optional] 
**otheri** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


