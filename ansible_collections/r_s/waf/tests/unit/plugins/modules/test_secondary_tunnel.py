# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import secondary_tunnel


@pytest.mark.parametrize(
    "param",
    [
        {
            "reverse_proxy": "7ca63343ed7f97da38340d64152d89e8",
            "parent_tunnel": "73a0e2f2d46873749cd9f1bc759bc800",
            "in_network_interface": "ee2bc48cd85d38f954cffddf90747a37",
            "active": True,
        }
    ],
)
def test_req_object(param):
    request_object = secondary_tunnel.req(param)
    assert request_object.reverse_proxy.uid == param["reverse_proxy"]
    assert request_object.parent_tunnel.uid == param["parent_tunnel"]
    assert request_object.in_network_interface.uid == param["in_network_interface"]
    assert request_object.active == param["active"]
