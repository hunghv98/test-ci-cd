# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import ntp


@pytest.mark.parametrize(
    "param",
    [
        {
            "name": "test_rp",
            "server": "192.168.1.1",
        }
    ],
)
def test_req_object(param):
    request_object = ntp.req(param)
    assert request_object.name == param["name"]
    assert request_object.server == param["server"]


@pytest.mark.parametrize(
    "param,key_error",
    [
        ({"name": "test_rp"}, "server"),
    ],
)
def test_req_key_error(param, key_error):
    with pytest.raises(KeyError) as excinfo:
        ntp.req(param)

    assert key_error in str(excinfo.value)
