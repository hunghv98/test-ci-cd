# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import scoringlist


@pytest.mark.parametrize(
    "param",
    [
        {
            "name": "test_scoringlist",
            "description": "Test description",
            "template": {"uid": "scoringlistDefault"},
            "static_scoringlist": {"uid": "staticScoringlistDefault"},
        }
    ],
)
def test_req_object(param):
    request_object = scoringlist.req(param)
    assert request_object.name == param["name"]
    assert request_object.description == param["description"]
    assert request_object.template == param["template"]
    assert request_object.static_scoringlist == param["static_scoringlist"]
