# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import reverse_proxy_profile

@pytest.mark.parametrize("param",
    [{
        "name":"test_reverse_proxy_profile",
        "start_server": 2,
        "server_limit": 10,
        "max_clients": 1000,
        "max_spare_threads": 500,
        "min_spare_threads": 100,
        "thread_per_child": 100,
        "max_requests_per_child": 10000000,
        "limit_request_field_size": 12400,
        "timeout": 300,
        "proxy_timeout": 60,
        "keep_alive": "true",
        "keep_alive_timeout": 5,
        "max_keep_alive_requests": 300,
        "ssl_session_cache_size": 256,
        "ssl_session_cache_timeout": 300,
    }]
)
def test_req_object(param):
    request_object = reverse_proxy_profile.req(param)
    assert request_object.name == param["name"]
    assert request_object.start_server == param["start_server"]
    assert request_object.server_limit == param["server_limit"]
    assert request_object.max_clients == param["max_clients"]
    assert request_object.max_spare_threads == param["max_spare_threads"]
    assert request_object.min_spare_threads == param["min_spare_threads"]
    assert request_object.thread_per_child == param["thread_per_child"]
    assert request_object.max_requests_per_child == param["max_requests_per_child"]
    assert request_object.limit_request_field_size == param["limit_request_field_size"]
    assert request_object.timeout == param["timeout"]
    assert request_object.proxy_timeout == param["proxy_timeout"]
    assert request_object.keep_alive == param["keep_alive"]
    assert request_object.keep_alive_timeout == param["keep_alive_timeout"]
    assert request_object.max_keep_alive_requests == param["max_keep_alive_requests"]
    assert request_object.ssl_session_cache_size == param["ssl_session_cache_size"]
    assert request_object.ssl_session_cache_timeout == param["ssl_session_cache_timeout"]
