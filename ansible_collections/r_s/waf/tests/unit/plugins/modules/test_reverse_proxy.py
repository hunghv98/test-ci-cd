# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import reverse_proxy


@pytest.mark.parametrize(
    "param",
    [
        {
            "name": "test_rp",
            "appliance": {"name": "app1"},
            "labels": ["label1", "label2"],
        }
    ],
)
def test_req_object(param):
    request_object = reverse_proxy.req(param)
    assert request_object.name == param["name"]
    assert request_object.appliance.name == param["appliance"]["name"]
    assert request_object.labels[0]["name"] == param["labels"][0]
    assert request_object.labels[1]["name"] == param["labels"][1]


@pytest.mark.parametrize(
    "param,key_error",
    [
        ({"name": "test_rp"}, "appliance"),
        ({"name": "test_rp", "appliance": {"name": "app1"}}, "labels"),
    ],
)
def test_req_key_error(param, key_error):
    with pytest.raises(KeyError) as excinfo:
        reverse_proxy.req(param)

    assert key_error in str(excinfo.value)
