# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import access_log_profile


@pytest.mark.parametrize("param", [{"name": "test", "type": "newncsa", "format": None}])
def test_req_object(param):
    request_object = access_log_profile.req(param)
    assert request_object.name == param["name"]
    assert request_object.type == param["type"]


@pytest.mark.parametrize(
    "param,key_error", [({"name": "test", "type": "newncsa"}, "format")]
)
def test_req_key_error(param, key_error):
    with pytest.raises(KeyError) as excinfo:
        access_log_profile.req(param)

    assert key_error in str(excinfo.value)
