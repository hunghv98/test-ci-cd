# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import blacklist

@pytest.mark.parametrize("param",
    [{
        "name":"test_blacklist",
        "description": "Test description",
        "template": {
            "uid" : "blacklistDefault"
        },
        "static_blacklist": {
            "uid" : "staticBlacklistDefault"
        }
    }]
)
def test_req_object(param):
    request_object = blacklist.req(param)
    assert request_object.name == param["name"]
    assert request_object.description == param["description"]
    assert request_object.template == param["template"]
    assert request_object.static_blacklist == param["static_blacklist"]
