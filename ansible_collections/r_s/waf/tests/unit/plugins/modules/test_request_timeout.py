# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import request_timeout


@pytest.mark.parametrize(
    "param",
    [
        {
            "name": "test_timeout",
            "header_timeout": "20-50",
            "body_timeout": "30-60",
            "header_rate": 30,
            "body_rate": 40,
        }
    ],
)
def test_req_object(param):
    request_object = request_timeout.req(param)
    assert request_object.name == param["name"]
    assert request_object.header_timeout == param["header_timeout"]
    assert request_object.body_timeout == param["body_timeout"]
    assert request_object.header_rate == param["header_rate"]
    assert request_object.body_rate == param["body_rate"]
