# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import compression


@pytest.mark.parametrize(
    "param",
    [
        {
            "name": "test_compression",
            "buffer_size": 20,
            "compression_level": 60,
            "memory_level": 30,
            "window_size": 40,
        }
    ],
)
def test_req_object(param):
    request_object = compression.req(param)
    assert request_object.name == param["name"]
    assert request_object.buffer_size == param["buffer_size"]
    assert request_object.compression_level == param["compression_level"]
    assert request_object.memory_level == param["memory_level"]
    assert request_object.window_size == param["window_size"]
