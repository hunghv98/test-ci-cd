# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import appliance


@pytest.mark.parametrize(
    "param",
    [
        {
            "name": "test_appliance",
            "admin_ip": "192.168.254.192",
            "admin_port": 3001,
            "location": "/test",
            "contact": "admin@rswaf.com",
        }
    ],
)
def test_req_object(param):
    request_object = appliance.req(param)
    assert request_object.name == param["name"]
    assert request_object.admin_ip == param["admin_ip"]
    assert request_object.admin_port == param["admin_port"]
    assert request_object.location == param["location"]
    assert request_object.contact == param["contact"]
