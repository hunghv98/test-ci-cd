# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import alerting_destination


@pytest.mark.parametrize(
    "param",
    [
        {
            "name": "test",
            "type": "syslog",
            "syslog_config": {
                "ip": "192.168.0.191",
                "port": 5008,
                "protocol": "tcp",
                "severity": 5,
                "facility": 1,
                "format": "rfc5424",
            },
        }
    ],
)
def test_req_object(param):
    request_object = alerting_destination.req(param)
    assert request_object.name == param["name"]
    assert request_object.type == param["type"]
    assert request_object.syslog_config.ip == param["syslog_config"]["ip"]
    assert request_object.syslog_config.port == param["syslog_config"]["port"]
    assert request_object.syslog_config.protocol == param["syslog_config"]["protocol"]
    assert request_object.syslog_config.severity == param["syslog_config"]["severity"]
    assert request_object.syslog_config.facility == param["syslog_config"]["facility"]
    assert request_object.syslog_config.format == param["syslog_config"]["format"]


@pytest.mark.parametrize(
    "param,key_error",
    [
        ({"name": "test", "type": "syslog"}, "syslog_config"),
        ({"name": "test", "type": "syslog", "syslog_config": {}}, "ip"),
        (
            {
                "name": "test",
                "type": "syslog",
                "syslog_config": {"ip": "192.168.0.254"},
            },
            "port",
        ),
        (
            {
                "name": "test",
                "type": "syslog",
                "syslog_config": {"ip": "192.168.0.254", "port": 80},
            },
            "protocol",
        ),
        (
            {
                "name": "test",
                "type": "syslog",
                "syslog_config": {"ip": "192.168.0.254", "port": 80, "protocol": "tcp"},
            },
            "severity",
        ),
        (
            {
                "name": "test",
                "type": "syslog",
                "syslog_config": {
                    "ip": "192.168.0.254",
                    "port": 80,
                    "protocol": "tcp",
                    "severity": 5,
                },
            },
            "facility",
        ),
        (
            {
                "name": "test",
                "type": "syslog",
                "syslog_config": {
                    "ip": "192.168.0.254",
                    "port": 80,
                    "protocol": "tcp",
                    "severity": 5,
                    "facility": 2,
                },
            },
            "format",
        ),
    ],
)
def test_req_key_error(param, key_error):
    with pytest.raises(KeyError) as excinfo:
        alerting_destination.req(param)

    assert key_error in str(excinfo.value)
