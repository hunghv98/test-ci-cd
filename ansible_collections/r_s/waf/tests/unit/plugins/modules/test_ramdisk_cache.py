# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import ramdisk_cache


@pytest.mark.parametrize(
    "param",
    [
        {
            "name": "test",
            "cache_ignore_cache_control": "true",
            "cache_ignore_no_last_mod": "true",
            "cache_ignore_query_string": "true",
            "cache_store_no_store": "true",
            "cache_store_private": "true",
            "cache_default_expire": 7200,
            "cache_ignore_headers": "test",
            "cache_ignore_url_session_identifiers": "test url",
            "cache_last_modified_factor": "0.1",
            "cache_max_expire": 7200,
            "cache_dir_length": 2,
            "cache_dir_levels": 4,
            "cache_max_file_size": 600000,
            "cache_min_file_size": 600,
        }
    ],
)
def test_req_object(param):
    request_object = ramdisk_cache.req(param)
    assert (
        request_object.cache_ignore_cache_control == param["cache_ignore_cache_control"]
    )
    assert request_object.cache_ignore_no_last_mod == param["cache_ignore_no_last_mod"]
    assert (
        request_object.cache_ignore_query_string == param["cache_ignore_query_string"]
    )
    assert request_object.cache_store_no_store == param["cache_store_no_store"]
    assert request_object.cache_store_private == param["cache_store_private"]
    assert request_object.cache_default_expire == param["cache_default_expire"]
    assert request_object.cache_ignore_headers == param["cache_ignore_headers"]
    assert (
        request_object.cache_ignore_url_session_identifiers
        == param["cache_ignore_url_session_identifiers"]
    )
    assert (
        request_object.cache_last_modified_factor == param["cache_last_modified_factor"]
    )
    assert request_object.cache_max_expire == param["cache_max_expire"]
    assert request_object.cache_dir_length == param["cache_dir_length"]
    assert request_object.cache_dir_levels == param["cache_dir_levels"]
    assert request_object.cache_max_file_size == param["cache_max_file_size"]
    assert request_object.cache_min_file_size == param["cache_min_file_size"]


@pytest.mark.parametrize(
    "param,key_error",
    [
        ({"name": "test"}, "cache_ignore_cache_control"),
        (
            {"name": "test", "cache_ignore_cache_control": "true"},
            "cache_ignore_no_last_mod",
        ),
        (
            {
                "name": "test",
                "cache_ignore_cache_control": "true",
                "cache_ignore_no_last_mod": "true",
            },
            "cache_ignore_query_string",
        ),
        (
            {
                "name": "test",
                "cache_ignore_cache_control": "true",
                "cache_ignore_no_last_mod": "true",
                "cache_ignore_query_string": "true",
            },
            "cache_store_no_store",
        ),
        (
            {
                "name": "test",
                "cache_ignore_cache_control": "true",
                "cache_ignore_no_last_mod": "true",
                "cache_ignore_query_string": "true",
                "cache_store_no_store": "true",
            },
            "cache_store_private",
        ),
        (
            {
                "name": "test",
                "cache_ignore_cache_control": "true",
                "cache_ignore_no_last_mod": "true",
                "cache_ignore_query_string": "true",
                "cache_store_no_store": "true",
                "cache_store_private": "true",
            },
            "cache_default_expire",
        ),
        (
            {
                "name": "test",
                "cache_ignore_cache_control": "true",
                "cache_ignore_no_last_mod": "true",
                "cache_ignore_query_string": "true",
                "cache_store_no_store": "true",
                "cache_store_private": "true",
                "cache_default_expire": 3600,
            },
            "cache_ignore_headers",
        ),
        (
            {
                "name": "test",
                "cache_ignore_cache_control": "true",
                "cache_ignore_no_last_mod": "true",
                "cache_ignore_query_string": "true",
                "cache_store_no_store": "true",
                "cache_store_private": "true",
                "cache_default_expire": 3600,
                "cache_ignore_headers": "test",
            },
            "cache_ignore_url_session_identifiers",
        ),
        (
            {
                "name": "test",
                "cache_ignore_cache_control": "true",
                "cache_ignore_no_last_mod": "true",
                "cache_ignore_query_string": "true",
                "cache_store_no_store": "true",
                "cache_store_private": "true",
                "cache_default_expire": 3600,
                "cache_ignore_headers": "test",
                "cache_ignore_url_session_identifiers": "url",
            },
            "cache_last_modified_factor",
        ),
        (
            {
                "name": "test",
                "cache_ignore_cache_control": "true",
                "cache_ignore_no_last_mod": "true",
                "cache_ignore_query_string": "true",
                "cache_store_no_store": "true",
                "cache_store_private": "true",
                "cache_default_expire": 3600,
                "cache_ignore_headers": "test",
                "cache_ignore_url_session_identifiers": "url",
                "cache_last_modified_factor": 2.1,
            },
            "cache_max_expire",
        ),
        (
            {
                "name": "test",
                "cache_ignore_cache_control": "true",
                "cache_ignore_no_last_mod": "true",
                "cache_ignore_query_string": "true",
                "cache_store_no_store": "true",
                "cache_store_private": "true",
                "cache_default_expire": 3600,
                "cache_ignore_headers": "test",
                "cache_ignore_url_session_identifiers": "url",
                "cache_last_modified_factor": 2.1,
                "cache_max_expire": 500000,
            },
            "cache_dir_length",
        ),
        (
            {
                "name": "test",
                "cache_ignore_cache_control": "true",
                "cache_ignore_no_last_mod": "true",
                "cache_ignore_query_string": "true",
                "cache_store_no_store": "true",
                "cache_store_private": "true",
                "cache_default_expire": 3600,
                "cache_ignore_headers": "test",
                "cache_ignore_url_session_identifiers": "url",
                "cache_last_modified_factor": 2.1,
                "cache_max_expire": 500000,
                "cache_dir_length": 1,
            },
            "cache_dir_levels",
        ),
        (
            {
                "name": "test",
                "cache_ignore_cache_control": "true",
                "cache_ignore_no_last_mod": "true",
                "cache_ignore_query_string": "true",
                "cache_store_no_store": "true",
                "cache_store_private": "true",
                "cache_default_expire": 3600,
                "cache_ignore_headers": "test",
                "cache_ignore_url_session_identifiers": "url",
                "cache_last_modified_factor": 2.1,
                "cache_max_expire": 500000,
                "cache_dir_length": 1,
                "cache_dir_levels": 2,
            },
            "cache_max_file_size",
        ),
        (
            {
                "name": "test",
                "cache_ignore_cache_control": "true",
                "cache_ignore_no_last_mod": "true",
                "cache_ignore_query_string": "true",
                "cache_store_no_store": "true",
                "cache_store_private": "true",
                "cache_default_expire": 3600,
                "cache_ignore_headers": "test",
                "cache_ignore_url_session_identifiers": "url",
                "cache_last_modified_factor": 2.1,
                "cache_max_expire": 500000,
                "cache_dir_length": 1,
                "cache_dir_levels": 2,
                "cache_max_file_size": 1,
            },
            "cache_min_file_size",
        ),
    ],
)
def test_req_key_error(param, key_error):
    with pytest.raises(KeyError) as excinfo:
        ramdisk_cache.req(param)

    assert key_error in str(excinfo.value)
