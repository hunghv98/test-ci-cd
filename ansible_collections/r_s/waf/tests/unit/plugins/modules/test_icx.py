# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import icx


@pytest.mark.parametrize(
    "param",
    [{"name": "test_icx", "description": 20, "template": {"uid": "icxtplowa2010"}}],
)
def test_req_object(param):
    request_object = icx.req(param)
    assert request_object.name == param["name"]
    assert request_object.description == param["description"]
    assert request_object.template == param["template"]
