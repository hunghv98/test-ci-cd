# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import normalization


@pytest.mark.parametrize(
    "param",
    [
        {
            "name": "test_normalization",
            "description": "Test description",
            "template": {"uid": "normalizationDefault"},
        }
    ],
)
def test_req_object(param):
    request_object = normalization.req(param)
    assert request_object.name == param["name"]
    assert request_object.description == param["description"]
    assert request_object.template == param["template"]
