# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import tunnel


@pytest.mark.parametrize(
    "param",
    [
        {
            "name": "test",
            "reverse_proxy": "test_rp",
            "workflow": "WAF ICX Default",
            "network": {
                "incoming": {
                    "interface": "test",
                    "port": "8080",
                    "server_name": "test.local",
                    "server_alias": "test",
                },
                "outgoing": {"address": "test", "port": "8080"},
            },
            "workflow_parameters": {},
            "labels": ["test_label"],
        }
    ],
)
def test_req_object(param):
    request_object = tunnel.req(param)
    assert request_object.name == param["name"]
    assert request_object.reverse_proxy["uid"] == param["reverse_proxy"]
    assert request_object.workflow["name"] == param["workflow"]
    assert request_object.network.incoming.interface["uid"] == "test"


@pytest.mark.parametrize(
    "param,key_error",
    [
        ({"reverse_proxy": "test_rp"}, "workflow"),
        (
            {"reverse_proxy": "test_rp", "workflow": "WAF ICX Default"},
            "workflow_parameters",
        ),
        (
            {
                "reverse_proxy": "test_rp",
                "workflow": "WAF ICX Default",
                "workflow_parameters": {},
                "network": {},
            },
            "incoming",
        ),
    ],
)
def test_req_key_error(param, key_error):
    with pytest.raises(KeyError) as excinfo:
        tunnel.req(param)

    assert key_error in str(excinfo.value)
