# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import ssl_profile


@pytest.mark.parametrize(
    "param",
    [
        {
            "name": "test_ssl_profile",
            "cipher": "ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384",
            "protocol": "TLSv1.2:TLSv1.1:TLSv1:SSLv3",
        }
    ],
)
def test_req_object(param):
    request_object = ssl_profile.req(param)
    assert request_object.name == param["name"]
    assert request_object.cipher == param["cipher"]
    assert request_object.protocol == param["protocol"]
