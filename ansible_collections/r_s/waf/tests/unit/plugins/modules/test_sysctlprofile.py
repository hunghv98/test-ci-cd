# -*- coding: utf-8 -*-
import pytest
from ansible_collections.r_s.waf.plugins.modules import sysctlprofile

@pytest.mark.parametrize("param",
    [{
        "name":"test_sysctlprofile",
        "kernel_shm_max": 1024,
        "net_ipv4_conf_all_rp_filter": 2,
        "net_core_optmem_max": 20480,
        "net_ipv4_tcp_ecn": 2,
        "net_ipv4_tcp_congestion_control": "cubic",
        "net_ipv4_tcp_fack": 1,
        "net_ipv4_tcp_sack": 1,
        "net_ipv4_tcp_dsack": 1,
        "net_ipv4_tcp_timestamps": 1,
        "net_ipv4_tcp_windows_scaling": 1,
        "net_ipv4_tcp_adv_win_scale": 1,
        "net_ipv4_tcp_workaround_signed_windows": 1,
        "net_ipv4_tcp_syncookies": 1,
        "net_ipv4_tcp_fin_timeout": 1,
        "net_ipv4_tcp_keepalive_time": 1,
        "net_ipv4_tcp_keepalive_intvl": 1,
        "net_ipv4_tcp_keepalive_probes": 1,
        "net_ipv4_tcp_tw_reuse": 1,
        "net_ipv4_tcp_max_tw_buckets": 1
    }]
)
def test_req_object(param):
    request_object = sysctlprofile.req(param)
    assert request_object.name == param["name"]
    assert request_object.kernel_shm_max == param["kernel_shm_max"]
    assert request_object.net_ipv4_conf_all_rp_filter == param["net_ipv4_conf_all_rp_filter"]
    assert request_object.net_core_optmem_max == param["net_core_optmem_max"]
    assert request_object.net_ipv4_tcp_ecn == param["net_ipv4_tcp_ecn"]
    assert request_object.net_ipv4_tcp_congestion_control == param["net_ipv4_tcp_congestion_control"]
    assert request_object.net_ipv4_tcp_fack == param["net_ipv4_tcp_fack"]
    assert request_object.net_ipv4_tcp_sack == param["net_ipv4_tcp_sack"]
    assert request_object.net_ipv4_tcp_dsack == param["net_ipv4_tcp_dsack"]
    assert request_object.net_ipv4_tcp_timestamps == param["net_ipv4_tcp_timestamps"]
    assert request_object.net_ipv4_tcp_windows_scaling == param["net_ipv4_tcp_windows_scaling"]
    assert request_object.net_ipv4_tcp_adv_win_scale == param["net_ipv4_tcp_adv_win_scale"]
    assert request_object.net_ipv4_tcp_workaround_signed_windows == param["net_ipv4_tcp_workaround_signed_windows"]
    assert request_object.net_ipv4_tcp_syncookies == param["net_ipv4_tcp_syncookies"]
    assert request_object.net_ipv4_tcp_fin_timeout == param["net_ipv4_tcp_fin_timeout"]
    assert request_object.net_ipv4_tcp_keepalive_time == param["net_ipv4_tcp_keepalive_time"]
    assert request_object.net_ipv4_tcp_keepalive_intvl == param["net_ipv4_tcp_keepalive_intvl"]
    assert request_object.net_ipv4_tcp_keepalive_probes == param["net_ipv4_tcp_keepalive_probes"]
    assert request_object.net_ipv4_tcp_tw_reuse == param["net_ipv4_tcp_tw_reuse"]
    assert request_object.net_ipv4_tcp_max_tw_buckets == param["net_ipv4_tcp_max_tw_buckets"]
