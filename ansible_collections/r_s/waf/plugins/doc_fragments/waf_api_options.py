# -*- coding: utf-8 -*-

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type


class ModuleDocFragment(object):
    # Standard documentation
    DOCUMENTATION = r"""
options:
    credentials:
        description:
            - Credentials for R&S®Web Application Firewall
        type: dict
        required: true
        suboptions:
            host:
                description:
                    - R&S®Web Application Firewall host
                required: true
                type: str

            username:
                description:
                    - Administrator user name
                required: true
                type: str

            password:
                description:
                    - Administrator password
                required: true
                type: str

            verify_ssl:
                description:
                    - Set to false to disable SSL verification
                default: true
                type: bool
    """
