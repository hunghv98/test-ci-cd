#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r'''
---
module: sysctlprofile

short_description: Managed sysctl profiles

version_added: "1.0"

description:
    - "Manage sysctl profiles"

options:
    name:
        description: Name of the sysctl profile.
        required: true
        type: str
    kernel_shm_max:
        description: kernel_shm_max of the sysctl profile
        default: 2048
        type: int
        required: false
    net_ipv4_conf_all_rp_filter:
        description: If you need to set several IPs with the same network range on several interfaces. You need to set netIpv4ConfAllRpFilter=2. We don't recommend because of security issues.
        default: 2
        required: false
        type: int
    net_core_optmem_max:
        description: net_core_optmem_max of sysctlprofile
        default: 20480
        type: int
        required: false
    net_ipv4_tcp_ecn:
        description: net_ipv4_tcp_ecn of sysctlprofilen
        default: 2
        type: int
        required: false
    net_ipv4_tcp_congestion_control:
        description: net_ipv4_tcp_congestion_control of sysctlprofile
        default: cubic
        type: str
        required: false
        choices: ["cubic", "westwood"]
    net_ipv4_tcp_fack:
        description: net_ipv4_tcp_fack of sysctlprofile
        default: 1
        type: int
        required: false
    net_ipv4_tcp_sack:
        description: net_ipv4_tcp_sack of sysctlprofile
        default: 1
        type: int
        required: false
    net_ipv4_tcp_dsack:
        description: net_ipv4_tcp_dsack of sysctlprofile
        default: 1
        type: int
        required: false
    net_ipv4_tcp_timestamps:
        description: net_ipv4_tcp_timestamps of sysctlprofile
        default: 1
        type: int
        required: false
    net_ipv4_tcp_windows_scaling:
        description: net_ipv4_tcp_windows_scaling of sysctlprofile
        default: 1
        type: int
        required: false
    net_ipv4_tcp_adv_win_scale:
        description: net_ipv4_tcp_adv_win_scale of sysctlprofile
        default: 6
        type: int
        required: false
    net_ipv4_tcp_workaround_signed_windows:
        description: net_ipv4_tcp_workaround_signed_windows of sysctlprofile
        default: 0
        type: int
        required: false
    net_ipv4_tcp_syncookies:
        description: net_ipv4_tcp_syncookies of sysctlprofile
        default: 0
        type: int
        required: false
    net_ipv4_tcp_fin_timeout:
        description: net_ipv4_tcp_fin_timeout of sysctlprofile
        default: 15
        type: int
        required: false
    net_ipv4_tcp_keepalive_time:
        description: net_ipv4_tcp_keepalive_time of sysctlprofile
        default: 1800
        type: int
        required: false
    net_ipv4_tcp_keepalive_intvl:
        description: net_ipv4_tcp_keepalive_intvl of sysctlprofile
        default: 75
        type: int
        required: false
    net_ipv4_tcp_keepalive_probes:
        description: net_ipv4_tcp_keepalive_probes of sysctlprofile
        default: 9
        type: int
        required: false
    net_ipv4_tcp_tw_reuse:
        description: net_ipv4_tcp_tw_reuse of sysctlprofile
        default: 0
        type: int
        required: false
    net_ipv4_tcp_max_tw_buckets:
        description: net_ipv4_tcp_max_tw_buckets of sysctlprofile
        default: 131072
        type: int
        required: false
    net_ipv4_tcp_max_orphans:
        description: net_ipv4_tcp_max_orphans of sysctlprofile
        default: 131072
        type: int
        required: false
    net_ipv4_tcp_max_syn_backlog:
        description: net_ipv4_tcp_max_syn_backlog of sysctlprofile
        default: 8192
        type: int
        required: false
    netCoreNetdevMaxBacklog:
        description: netCoreNetdevMaxBacklog of sysctlprofile
        default: 2500
        type: int
        required: false
    net_ipv4_tcp_moderate_rcvbuf:
        description: net_ipv4_tcp_moderate_rcvbuf of sysctlprofile
        default: 1
        type: int
        required: false
    net_ipv4_tcp_rmem_min:
        description: net_ipv4_tcp_rmem_min of sysctlprofile
        default: 4096
        type: int
        required: false
    net_ipv4_tcp_rmem_default:
        description: net_ipv4_tcp_rmem_default of sysctlprofile
        default: 87380
        type: int
        required: false
    net_ipv4_tcp_rmem_max:
        description: net_ipv4_tcp_rmem_max of sysctlprofile
        default: 6291456
        type: int
        required: false
    net_ipv4_tcp_wmem_min:
        description: net_ipv4_tcp_wmem_min of sysctlprofile
        default: 4096
        type: int
        required: false
    net_ipv4_tcp_wmem_default:
        description: net_ipv4_tcp_wmem_default of sysctlprofile
        default: 16384
        type: int
        required: false
    net_ipv4_tcp_wmem_max:
        description: net_ipv4_tcp_wmem_max of sysctlprofile
        default: 4194304
        type: int
        required: false
    net_core_rmem_default:
        description: net_core_rmem_default of sysctlprofile
        default: 212992
        type: int
        required: false
    net_core_wmem_default:
        description: net_core_wmem_default of sysctlprofile
        default: 212992
        type: int
        required: false
    net_core_somaxconn:
        description: net_core_somaxconn of sysctlprofile
        default: 1024
        type: int
        required: false
    netIpv4TcpAbortOnOverflow:
        description: netIpv4TcpAbortOnOverflow of sysctlprofile
        default: 0
        type: int
        required: false
    net_ipv4_tcp_abort_on_overflow:
        description: net_ipv4_tcp_abort_on_overflow of sysctlprofile
        default: 5
        type: int
        required: false
    net_ipv4_tcp_syn_ack_retries:
        description: net_ipv4_tcp_syn_ack_retries of sysctlprofile
        default: 3
        type: int
        required: false
    net_ipv4_tcp_tw_recycle:
        description: net_ipv4_tcp_tw_recycle of sysctlprofile
        default: 0
        type: int
        required: false
    net_ipv4_tcp_rfc1337:
        description: net_ipv4_tcp_rfc1337 of sysctlprofile
        default: 0
        type: int
        required: false
    state:
        description:
            - State of the sysctl profile.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team
'''

EXAMPLES = r'''
- name: Have sysctl profile

  r_s.waf.sysctlprofile:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    name: "test sysctl"
	kernel_shm_max: 2048
	net_ipv4_conf_all_rp_filter: 2
	net_core_optmem_max: 20480
    net_ipv4_tcp_ecn: 2
    net_ipv4_tcp_congestion_control: "cubic"
    net_ipv4_tcp_fack: 1
    net_ipv4_tcp_sack: 1
    net_ipv4_tcp_dsack: 1
    net_ipv4_tcp_timestamps: 1
    net_ipv4_tcp_windows_scaling: 1
    net_ipv4_tcp_adv_win_scale: 1
    net_ipv4_tcp_workaround_signed_windows: 1
    net_ipv4_tcp_syncookies: 1
    net_ipv4_tcp_fin_timeout: 1
    net_ipv4_tcp_keepalive_time: 1
    net_ipv4_tcp_keepalive_intvl: 1
    net_ipv4_tcp_keepalive_probes: 1
    net_ipv4_tcp_tw_reuse: 1
    net_ipv4_tcp_max_tw_buckets: 1
'''

RETURN = r'''
'''

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils import r_s_waf
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
    get_value,
)


def req(params):
    body = waf_client.CreateSysctlprofile(
        name=params["name"],
        kernel_shm_max=params["kernel_shm_max"],
        net_ipv4_conf_all_rp_filter=params["net_ipv4_conf_all_rp_filter"],
        net_core_optmem_max=params["net_core_optmem_max"],
        net_ipv4_tcp_ecn=params["net_ipv4_tcp_ecn"],
        net_ipv4_tcp_congestion_control=params["net_ipv4_tcp_congestion_control"],
        net_ipv4_tcp_fack=params["net_ipv4_tcp_fack"],
        net_ipv4_tcp_sack=params["net_ipv4_tcp_sack"],
        net_ipv4_tcp_dsack=params["net_ipv4_tcp_dsack"],
        net_ipv4_tcp_timestamps=params["net_ipv4_tcp_timestamps"],
        net_ipv4_tcp_windows_scaling=params["net_ipv4_tcp_windows_scaling"],
        net_ipv4_tcp_adv_win_scale=params["net_ipv4_tcp_adv_win_scale"],
        net_ipv4_tcp_workaround_signed_windows=params["net_ipv4_tcp_workaround_signed_windows"],
        net_ipv4_tcp_syncookies=params["net_ipv4_tcp_syncookies"],
        net_ipv4_tcp_fin_timeout=params["net_ipv4_tcp_fin_timeout"],
        net_ipv4_tcp_keepalive_time=params["net_ipv4_tcp_keepalive_time"],
        net_ipv4_tcp_keepalive_intvl=params["net_ipv4_tcp_keepalive_intvl"],
        net_ipv4_tcp_keepalive_probes=params["net_ipv4_tcp_keepalive_probes"],
        net_ipv4_tcp_tw_reuse=params["net_ipv4_tcp_tw_reuse"],
        net_ipv4_tcp_max_tw_buckets=params["net_ipv4_tcp_max_tw_buckets"]
    )
    return body


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
			kernel_shm_max=dict(type="int", default=2048),
			net_ipv4_conf_all_rp_filter=dict(type="int", default=2),
            net_core_optmem_max=dict(type="int", default=2),
            net_ipv4_tcp_ecn=dict(type="int", default=2),
            net_ipv4_tcp_congestion_control=dict(type="str", default="cubic"),
            net_ipv4_tcp_fack=dict(type="int", default=2),
            net_ipv4_tcp_sack=dict(type="int", default=2),
            net_ipv4_tcp_dsack=dict(type="int", default=2),
            net_ipv4_tcp_timestamps=dict(type="int", default=2),
            net_ipv4_tcp_windows_scaling=dict(type="int", default=2),
            net_ipv4_tcp_adv_win_scale=dict(type="int", default=2),
            net_ipv4_tcp_workaround_signed_windows=dict(type="int", default=2),
            net_ipv4_tcp_syncookies=dict(type="int", default=2),
            net_ipv4_tcp_fin_timeout=dict(type="int", default=2),
            net_ipv4_tcp_keepalive_time=dict(type="int", default=2),
            net_ipv4_tcp_keepalive_intvl=dict(type="int", default=2),
            net_ipv4_tcp_keepalive_probes=dict(type="int", default=2),
            net_ipv4_tcp_tw_reuse=dict(type="int", default=2),
            net_ipv4_tcp_max_tw_buckets=dict(type="int", default=2),
            state=dict(type="str", default="present", choices=["present", "absent"])
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    if module.check_mode:
        module.exit_json(change=True, msg="Not supported")

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.get_sysctlprofiles(name=module.params["name"])
        rp = resp.data[0]
        uid = resp.data[0].uid
        name = resp.data[0].name
    except ApiException as e:
        uid = None
        #module.fail_json(msg="Fail to get sysctl profile: %s" % e)
    except IndexError:
        uid = None

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False)

        try:
            resp = client.del_sysctlprofile(uid=uid)
        except ApiException as e:
            module.fail_json(msg="Fail to delete sysctl profile: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete sysctl profile %s" % module.params["name"],
        )

    if uid:
        def need_update():
            if rp.name != module.params["name"]:
                return True
            if rp.kernel_shm_max != module.params["kernel_shm_max"]:
                return True
            return False

        if need_update():
            try:
                resp = client.update_sysctlprofile(name=name, update_sysctlprofile=req(module.params))
            except ApiException as e:
                module.fail_json(msg="Fail to update sysctl profile: %s" % e)

            module.exit_json(
                changed=True,
                msg="Successfully update sysctl profile %s" % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.create_sysctlprofile(create_sysctlprofile=req(module.params))
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create sysctl profile: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create sysctl profile %s" % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
