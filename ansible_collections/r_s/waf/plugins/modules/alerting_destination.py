#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: alerting_destination

short_description: Manage alerting destination

version_added: "1.0.0"

description:
    - "Manage alerting destination."

options:
    name:
        description:
            - Name of the alerting destination.
        required: true
        type: str
    type:
        description:
            - Type of the alerting destination
        choices: ['smtp', 'syslog', 'snmp']
        default: 'syslog'
        type: str
    syslog_config:
        description:
            - The syslog configuration
        type: dict
        suboptions:
            ip:
                description:
                    - ip of the syslog configuration.
                required: true
                type: str
            port:
                description:
                    - port of the syslog configuration.
                required: true
                type: int
            protocol:
                description:
                    - protocol of the syslog configuration.
                required: true
                type: str
            severity:
                description:
                    - severity of the syslog configuration.
                required: true
                type: int
            facility:
                description:
                    - facility of the syslog configuration.
                required: true
                type: int
            format:
                description:
                    - format of the syslog configuration.
                choices: ['legacyV5', 'rfc5424', 'rfc3164']
                default: legacyV5
                type: str
    state:
        description:
            - State of the alerting destination.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team(@rs_team)
"""

EXAMPLES = r"""
- name: Have test syslog destination
  r_s.waf.alerting_destination:
    credentials:
      host: rswaf.local:3001
      password: Denyall@0
      username: superadmin
      verify_ssl: false
    name: test
    type: syslog
    syslog_config:
      ip: 192.168.0.191
      port: 5008
      protocol: tcp
      severity: 5
      facility: 1
      format: rfc5424
"""

RETURN = r"""

"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
)


def req(params):
    syslog_config = params["syslog_config"]
    syslog_config_data = None
    if syslog_config is not None:
        syslog_config_data = waf_client.CreateAlertingDestinationsSyslogConfig(
            ip=syslog_config["ip"],
            port=syslog_config["port"],
            protocol=syslog_config["protocol"],
            severity=syslog_config["severity"],
            facility=syslog_config["facility"],
            format=syslog_config["format"],
        )
    body = waf_client.CreateAlertingDestinations(
        name=params["name"], type=params["type"], syslog_config=syslog_config_data
    )
    return body


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            type=dict(type="str", default="syslog", choices=["smtp", "syslog", "snmp"]),
            syslog_config=dict(
                type="dict",
                options=dict(
                    ip=dict(type="str", required=True),
                    port=dict(type="int", required=True),
                    protocol=dict(type="str", required=True),
                    severity=dict(type="int", required=True),
                    facility=dict(type="int", required=True),
                    format=dict(
                        type="str",
                        default="legacyV5",
                        choices=["legacyV5", "rfc5424", "rfc3164"],
                    ),
                ),
            ),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(msg="Not supported")

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.get_alertingdestinations(name=module.params["name"])
        uid = resp.data[0].uid
    except ApiException as e:
        uid = None
    except IndexError:
        uid = None
    else:
        if uid:
            alertingdestination = resp.data[0]

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False, msg="Alerting destination does not exist")

        try:
            resp = client.del_alerting_destination(name=module.params["name"])
        except ApiException as e:
            module.fail_json(msg="Fail to delete alerting destination: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete alerting destination %s"
            % module.params["name"],
            uid=uid,
        )

    if uid:

        def need_update():
            if alertingdestination.name != module.params["name"]:
                return True
            if alertingdestination.type != module.params["type"]:
                return True
            if (
                alertingdestination.syslog_config.ip
                != module.params["syslog_config"]["ip"]
            ):
                return True
            if (
                alertingdestination.syslog_config.port
                != module.params["syslog_config"]["port"]
            ):
                return True
            if (
                alertingdestination.syslog_config.protocol
                != module.params["syslog_config"]["protocol"]
            ):
                return True
            if (
                alertingdestination.syslog_config.severity
                != module.params["syslog_config"]["severity"]
            ):
                return True
            if (
                alertingdestination.syslog_config.facility
                != module.params["syslog_config"]["facility"]
            ):
                return True
            if (
                alertingdestination.syslog_config.format
                != module.params["syslog_config"]["format"]
            ):
                return True

            return False

        if need_update():
            try:
                resp = client.patch_alerting_destination(
                    name=module.params["name"],
                    patch_alerting_destination=req(module.params),
                )
            except ApiException as e:
                module.fail_json(msg="Fail to update alerting destination: %s" % e)

            module.exit_json(
                changed=True,
                msg="Successfully update alerting destination %s"
                % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.create_alerting_destinations(
            create_alerting_destinations=req(module.params)
        )
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create alerting destination: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create alerting destination %s" % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
