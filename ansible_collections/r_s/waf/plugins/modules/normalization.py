#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: normalization

short_description: Manage normalization configuration

version_added: "1.0.0"

description:
    - "Manage Manage normalization configuration."

options:
    name:
        description:
            - Name of the normalization configuration.
        required: true
        type: str
    description:
        description:
            - Description of the normalization configuration.
        required: false
        type: str
    template:
        description:
            - The template of the normalization configuration.
        required: false
        type: dict
        suboptions:
            uid:
                description:
                    - Uid of the template.
                required: true
                type: str
    state:
        description:
            - State of the normalization configuration.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: Have test normalization configuration

  r_s.waf.normalization:

    credentials:
      host: rswaf.local:3001
      password: Denyall@0
      username: superadmin
      verify_ssl: false
    name: test
    description: "normalization configuration description"
    template:
      uid: normalizationDefault
"""

RETURN = r"""

"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
)


def req(params):
    body = waf_client.CreateNormalization(
        name=params["name"],
        description=params["description"],
        template=params["template"],
    )
    return body


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            description=dict(type="str", required=False),
            template=dict(
                type="dict",
                options=dict(uid=dict(type="str", required=True)),
            ),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(msg="Not supported")

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.get_normalization(name=module.params["name"])
        uid = resp.data[0].uid
    except ApiException as e:
        uid = None
    except IndexError:
        uid = None
    else:
        if uid:
            normalization = resp.data[0]

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False, msg="normalization does not exist")

        try:
            resp = client.del_normalization(uid=uid)
        except ApiException as e:
            module.fail_json(msg="Fail to delete normalization: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete normalization %s" % module.params["name"],
            uid=uid,
        )

    if uid:

        def need_update():
            if normalization.description != module.params["description"]:
                return True
            return False

        if need_update():
            try:
                patch_normalization = waf_client.PatchNormalization(
                    description=module.params["description"]
                )
                resp = client.patch_normalization(
                    uid=uid, patch_normalization=patch_normalization
                )
            except ApiException as e:
                module.fail_json(msg="Fail to update normalization: %s" % e)

            module.exit_json(
                changed=True,
                msg="Successfully update normalization %s" % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.create_normalization(create_normalization=req(module.params))
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create normalization: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create normalization %s" % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
