#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: appliance

short_description: Manages appliances

version_added: "1.0.0"

description:
    - "Manages appliances"

options:
    name:
        description:
            - Name of the appliance.
        required: true
        type: str
    admin_ip:
        description:
            - Appliance IP address.
        required: true
        type: str
    admin_port:
        description:
            - TCP port to connect to the web controller.
        required: true
        type: str
    location:
        description:
            - The location of the appliance.
        required: false
        type: str
    contact:
        description:
            - Information to contact the appliance's administrator.
        required: false
        type: str
    sysctl_profile:
        description:
            - The uid of sysctl profile.
        required: false
        type: str
    state:
        description:
            - State of the appliance.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: Have appliance

  r_s.waf.appliance:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    name: Managed 1
    admin_ip: 192.168.254.192
    admin_port: 3001
    location: /
    contact: admin@rswaf.com
    sysctl_profile: null
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils import r_s_waf
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
    get_value,
)


def req(params):
    sysctl_profile_data = get_value(params, "sysctl_profile", None)
    sysctl_profile = (
        dict(uid=sysctl_profile_data) if sysctl_profile_data is not None else None
    )

    body = waf_client.CreateAppliance(
        name=params["name"],
        admin_ip=params["admin_ip"],
        admin_port=params["admin_port"],
        location=get_value(params, "location", None),
        contact=get_value(params, "contact", None),
        sysctl_profile=sysctl_profile,
    )
    return body


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            admin_ip=dict(type="str", required=True),
            admin_port=dict(type="str", required=True),
            location=dict(type="str", required=False),
            contact=dict(type="str", required=False),
            sysctl_profile=dict(type="str", required=False),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    if module.check_mode:
        module.exit_json(change=True, msg="Not supported")

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.get_appliances(name=module.params["name"])
        uid = resp.data[0].uid
    except ApiException as e:
        uid = None
    except IndexError:
        uid = None
    else:
        if uid:
            app = resp.data[0]

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False)

        try:
            resp = client.del_appliance(uid=uid)
        except ApiException as e:
            module.fail_json(msg="Fail to delete appliance: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete appliance %s" % module.params["name"],
        )

    if uid:

        def need_update():
            if app.name != module.params["name"]:
                return True
            if app.admin_ip != module.params["admin_ip"]:
                return True
            if app.admin_port != module.params["admin_port"]:
                return True
            if app.location != module.params["location"]:
                return True
            if app.contact != module.params["contact"]:
                return True
            if app.sysctl_profile.uid != module.params["sysctl_profile"]:
                return True
            return False

        if need_update():
            try:
                resp = client.patch_appliance(
                    uid=uid, patch_appliance=req(module.params)
                )
            except ApiException as e:
                module.fail_json(msg="Fail to update appliance: %s" % e)

            module.exit_json(
                changed=True,
                msg="Successfully update appliance %s" % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.create_appliance(create_appliance=req(module.params))
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create appliance: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create appliance %s" % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
