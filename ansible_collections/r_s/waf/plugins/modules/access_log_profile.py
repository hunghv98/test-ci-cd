#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: access_log_profile

short_description: Manage access log profiles

description:
    - "Manage access log profiles."

options:
    name:
        description:
            - Name of the access log profile.
        required: true
        type: str
    type:
        description:
            - Type of the access log profile
        choices: ['custom', 'clf', 'clfvh', 'ncsa', 'newncsa', 'newclfvh', 'newclf']
        default: custom
        type: str
    format:
        description:
            - Format of the access log profile.
        required: false
        type: str

    state:
        description:
            - State of the access log profile.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author: RS team (@rs_team)
"""

EXAMPLES = r"""
- name: have "test" access log profile

  r_s.waf.access_log_profile:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    name: test
    type: custom
    format: test_format
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
)


def req(params):
    body = waf_client.CreateAccesslogprofile(
        name=params["name"], type=params["type"], format=params["format"]
    )
    return body


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            type=dict(
                type="str",
                default="custom",
                choices=[
                    "custom",
                    "clf",
                    "clfvh",
                    "ncsa",
                    "newncsa",
                    "newclfvh",
                    "newclf",
                ],
            ),
            format=dict(type="str", required=False),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(msg="Not supported")

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.get_access_log_profiles(name=module.params["name"])
        uid = resp.data[0].uid
    except ApiException as e:
        uid = None
    except IndexError:
        uid = None
    else:
        if uid:
            access_log_profiles = resp.data[0]

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False, msg="Access log profile does not exist")

        try:
            resp = client.del_accesslogprofile(name=module.params["name"])
        except ApiException as e:
            module.fail_json(msg="Fail to delete access log profile: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete access log profile %s" % module.params["name"],
            uid=uid,
        )

    if uid:

        def need_update():
            if access_log_profiles.name != module.params["name"]:
                return True
            if access_log_profiles.type != module.params["type"]:
                return True
            if access_log_profiles.format != module.params["format"]:
                return True

            return False

        if need_update():
            try:
                resp = client.patch_accesslogprofile(
                    name=module.params["name"],
                    patch_accesslogprofile=req(module.params),
                )
            except ApiException as e:
                module.fail_json(msg="Fail to update access log profile: %s" % e)

            module.exit_json(
                changed=True,
                msg="Successfully update access log profile %s" % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.create_accesslogprofile(
            create_accesslogprofile=req(module.params)
        )
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create access log profile: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create access log profile %s" % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
