#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: reverse_proxy

short_description: Managed reverse proxies

version_added: "1.0.0"

description:
    - "Manage reverse proxies"

options:
    name:
        description:
            - Name of the reverse proxy.
        required: true
        type: str
    appliance:
        description:
            - Appliance of the reverse proxy.
        required: true
        type: dict
        suboptions:
            uid:
                description:
                    - Appliance ID of the reverse proxy.
                required: false
                type: str
            name:
                description:
                    - Appliance Name of the reverse proxy.
                required: false
                type: str
    labels:
        description:
            - Labels list.
        required: false
        type: list
        elements: str
    filter:
        description:
            - Enable filter
        required: false
        type: dict
        suboptions:
            label:
                description:
                    - Label object to filter
                type: dict
                suboptions:
                    name:
                        description:
                            - Name of the label.
                        required: false
                        type: str
    state:
        description:
            - State of the reverse proxy.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: Have reverse proxy

  r_s.waf.reverse_proxy:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    appliance:
      name: app1
    name: rp1
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils import r_s_waf
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
)


def req(params):
    app_uid = params["appliance"].get("uid", None)
    app_name = params["appliance"].get("name", None)
    app = None
    if app_uid is not None:
        app = waf_client.CreateApplianceSysctlProfile(uid=app_uid)
    if app_name is not None:
        app = waf_client.CreateApplianceSysctlProfile(name=app_name)
    body = waf_client.AddReverseProxy(
        name=params["name"],
        appliance=app,
        labels=[{"name": label} for label in params["labels"]],
    )
    return body


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            appliance=dict(
                type="dict",
                required=True,
                options=dict(
                    uid=dict(type="str"),
                    name=dict(type="str"),
                ),
            ),
            labels=r_s_waf.gen_labels_args(),
            filter=dict(
                type="dict",
                required=False,
                options=dict(
                    label=dict(
                        type="dict", required=False, options=dict(name=dict(type="str"))
                    )
                ),
            ),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    if module.check_mode:
        module.exit_json(change=True, msg="Not supported")

    client = get_client(module.params.get("credentials"))

    try:
        # Filter by label
        if (
            module.params["filter"] is not None
            and module.params["filter"]["label"] is not None
            and module.params["filter"]["label"]["name"]
        ):
            label_name = module.params["filter"]["label"]["name"]
            resp = client.get_reverse_proxies(label_name=label_name)

            module.exit_json(
                changed=False,
                result=[{"uid": rp.uid, "name": rp.name} for rp in resp.data],
                msg="Successfully filter reverse proxies by label",
            )
        else:
            resp = client.get_reverse_proxies(name=module.params["name"])
            rp = resp.data[0]
            uid = resp.data[0].uid
    except ApiException as e:
        uid = None
        # module.fail_json(msg="Fail to get reverse_proxy: %s" % e)
    except IndexError:
        uid = None

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False)

        try:
            resp = client.delete_reverse_proxies(uid=uid)
        except ApiException as e:
            module.fail_json(msg="Fail to delete reverse_proxy: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete reverse_proxy %s" % module.params["name"],
        )

    if uid:

        def need_update():
            if rp.name != module.params["name"]:
                return True
            if [l.name for l in rp.labels if l] != [l for l in module.params["labels"]]:
                return True
            return False

        if need_update():
            try:
                resp = client.update_reverse_proxy(
                    uid=uid, update_reverse_proxy=req(module.params)
                )
            except ApiException as e:
                module.fail_json(msg="Fail to update reverse_proxy: %s" % e)

            module.exit_json(
                changed=True,
                msg="Successfully update reverse_proxy %s" % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.add_reverse_proxy(add_reverse_proxy=req(module.params))
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create reverse_proxy: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create reverse_proxy %s" % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
