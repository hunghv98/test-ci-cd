#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: ntp

short_description: Managed ntps

version_added: "1.0.0"

description:
    - "Manage ntps"

options:
    name:
        description:
            - Name of the ntp.
        required: true
        type: str
    server:
        description:
            - Server of ntp
        required: true
        type: str
    state:
        description:
            - State of the ntp.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: Have ntp

  r_s.waf.ntp:
    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    name: test
    server: time.google.com
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils import r_s_waf
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
)


def req(params):
    body = waf_client.CreateNtp(
        name=params["name"],
        server=params["server"],
    )
    return body


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            server=dict(type="str", required=True),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    if module.check_mode:
        module.exit_json(change=True, msg="Not supported")

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.get_ntps(name=module.params["name"])
        ntp = resp.data[0]
        uid = resp.data[0].uid
    except ApiException as e:
        uid = None
        # module.fail_json(msg="Fail to get ntp: %s" % e)
    except IndexError:
        uid = None

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False)

        try:
            resp = client.del_ntp(uid=uid)
        except ApiException as e:
            module.fail_json(msg="Fail to delete ntp: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete ntp %s" % module.params["name"],
        )

    if uid:

        def need_update():
            if ntp.server != module.params["server"]:
                return True
            return False

        if need_update():
            try:
                resp = client.update_ntp(uid=uid, update_ntp=req(module.params))
            except ApiException as e:
                module.fail_json(msg="Fail to update ntp: %s" % e)

            module.exit_json(
                changed=True,
                msg="Successfully update ntp %s" % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.create_ntp(create_ntp=req(module.params))
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create ntp: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create ntp %s" % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
