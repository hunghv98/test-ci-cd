#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: log_filter

short_description: Managed log filter

version_added: "1.0.0"

description:
    - "Managed log filter"

options:
    name:
        description:
            - Name of the log filter.
        required: true
        type: str
    description:
        description:
            - Description of the log filter.
        required: false
        type: str
    upload:
        description:
            - The path to log filter file.
        required: false
        type: path
    state:
        description:
            - State of the log filter.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: have test log filter

  r_s.waf.log_filter:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@1"
      verify_ssl: false
    name: test3
    description: test3 log filter
    upload: ./files/logfilter.dat
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils import r_s_waf
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
)


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            description=dict(type="str"),
            upload=dict(default=None, type="path"),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.get_log_filter(name=module.params["name"])

        log_filter = resp.data[0]
        uid = resp.data[0].uid
    except ApiException as e:
        uid = None
        # module.fail_json(msg="Fail to get certificate: %s" % e)
    except IndexError:
        uid = None

    # if module.params["state"] == "absent":
    #     if not uid:
    #         # already absent
    #         module.exit_json(changed=False)

    #     try:
    #         resp = client.delete_log_filter(name=module.params["name"])
    #     except ApiException as e:
    #         module.fail_json(msg="Fail to delete log filter: %s" % e)

    #     module.exit_json(
    #         changed=True,
    #         result="Successfully delete log filter %s" % module.params["name"],
    #     )

    name = module.params.get("name", None)
    description = module.params.get("description", None)
    upload = module.params.get("upload", None)

    kvargs = {
        "name": name,
        "description": description,
    }

    if upload is not None:
        kvargs.update({"upload": upload})

    if uid:

        def need_update():
            if log_filter.name != module.params["name"]:
                return True
            if log_filter.description != module.params["description"]:
                return True
            return False

        if need_update():
            try:
                f = open(upload, "rb")
                f_data = f.read()
                f.close()
                resp = client.put_log_filter(uid=uid, body=f_data)
            except ApiException as e:
                module.fail_json(msg="Fail to update log filter: %s" % e)

            module.exit_json(
                changed=True,
                msg="Successfully update log filter %s" % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.create_logfilter(**kvargs)
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create log filter: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create log filter %s" % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
