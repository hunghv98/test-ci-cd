#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: security_exception_profile

short_description: Managed security exceptions

version_added: "1.0.0"

description:
    - "Manage security exceptions"

options:
    name:
        description:
            - Name of the security exception.
        required: true
        type: str
    description:
        description:
            - Description of the security exception.
        required: false
        type: str
    rules:
        description:
            - List rules of the security exceptions.
        required: false
        type: list
        elements: dict
        suboptions:
            name:
                description:
                    - Name of the rule.
                required: true
                type: str
            description:
                description:
                    - Description of the rule.
                required: false
                type: str
            enable:
                description:
                    - Enable/Disable of the rule.
                required: false
                type: bool
                default: true
            conditions:
                description:
                    - List of the security exception conditions.
                required: false
                type: list
                suboptions:
                    part:
                        description:
                            - Part of the security exception condition.
                        required: false
                        type: str
                    key_operator:
                        description:
                            - Key operator of the security exception condition.
                        required: false
                        type: str
                    key:
                        description:
                            - Key of the security exception condition.
                        required: false
                        type: str
                    value_operator:
                        description:
                            - Value operator of the security exception condition.
                        required: false
                        type: str
                    value:
                        description:
                            - Value of the security exception condition.
                        required: false
                        type: str
            matching_parts:
                description:
                    - List of the security exception matching parts.
                required: false
                type: list
                suboptions:
                    part:
                        description:
                            - Part of the security exception matching part.
                        required: false
                        type: str
                    key_operator:
                        description:
                            - Key operator of the security exception matching part.
                        required: false
                        type: str
                    key:
                        description:
                            - Key of the security exception matching part.
                        required: false
                        type: str
                    value_type:
                        description:
                            - Value type of the security exception matching part.
                        required: false
                        type: str
                    value_operator:
                        description:
                            - Value operator of the security exception matching part.
                        required: false
                        type: str
                    value:
                        description:
                            - Value of the security exception matching part.
                        required: false
                        type: str
    state:
        description:
            - State of the security exception.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: Have security exception

  r_s.waf.security_exception_profile:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    name: req_timeout
    header_timeout: "20-50"
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils import r_s_waf
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
    get_value,
)


def req(params):
    rules = []

    if get_value(params, "rules") is not None:
        for rule in params["rules"]:
            matching_parts = []
            conditions = []

            if rule["conditions"] is not None:
                for condition in rule["conditions"]:
                    conditions.append(
                        waf_client.CreateSecurityExceptionProfileConditionsArray(
                            part=get_value(condition, "part", None),
                            key_operator=get_value(condition, "key_operator", None),
                            key=get_value(condition, "key", None),
                            value_operator=get_value(condition, "value_operator", None),
                            value=get_value(condition, "value", None),
                        )
                    )

            if get_value(rule, "matching_parts") is not None:
                for matching_part in rule["matching_parts"]:
                    matching_parts.append(
                        waf_client.CreateSecurityExceptionProfileMatchingPartsArray(
                            part=get_value(matching_part, "part", None),
                            key_operator=get_value(matching_part, "key_operator", None),
                            key=get_value(matching_part, "key", None),
                            value_type=get_value(matching_part, "value_type", None),
                            value_operator=get_value(
                                matching_part, "value_operator", None
                            ),
                            value=get_value(matching_part, "value", None),
                        )
                    )
            rule_data = waf_client.CreateSecurityExceptionProfileRulesArray(
                name=get_value(rule, "name"),
                description=get_value(rule, "description", None),
                enabled=get_value(rule, "enabled", True),
                conditions=conditions,
                matching_parts=matching_parts,
            )
            rules.append(rule_data)

    body = waf_client.CreateSecurityExceptionProfile(
        name=params["name"],
        description=params["description"],
        rules=rules,
    )
    return body


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            description=dict(type="str"),
            rules=dict(type="list", default=[], required=False, elements="dict"),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    if module.check_mode:
        module.exit_json(change=True, msg="Not supported")

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.get_security_exception_profiles(name=module.params["name"])
        rp = resp.data[0]
        uid = resp.data[0].uid
    except ApiException as e:
        uid = None
    except IndexError:
        uid = None

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False)

        try:
            resp = client.del_security_exception_profile(uid=uid)
        except ApiException as e:
            module.fail_json(msg="Fail to delete security_exception_profile: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete security_exception_profile %s"
            % module.params["name"],
        )

    if uid:

        def need_update():
            return True

        if need_update():
            try:
                resp = client.update_security_exception_profile(
                    uid=uid, update_security_exception_profile=req(module.params)
                )
            except ApiException as e:
                module.fail_json(
                    msg="Fail to update security_exception_profile: %s" % e
                )

            module.exit_json(
                changed=True,
                msg="Successfully update security_exception_profile %s"
                % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.create_security_exception_profile(
            create_security_exception_profile=req(module.params)
        )
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create security_exception_profile: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create security_exception_profile %s"
        % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
