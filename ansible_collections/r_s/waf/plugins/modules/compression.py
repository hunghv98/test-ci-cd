#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: compression

short_description: Managed compressions

version_added: "1.0.0"

description:
    - "Manage compressions"

options:
    name:
        description:
            - Name of the compression.
        required: true
        type: str
    content_types:
        description:
            - Content types that will be compressed
        required: false
        type: str
    exclude_browser:
        description:
            - Enter the signatures of browsers for which you don't want to enable compression.
        required: false
        type: str
    buffer_size:
        description:
            - Enter the size of the compression buffer
        required: true
        type: int
    compression_level:
        description:
            - Enter the compression ratio. The higher the ratio, the smaller the size of the data will be, but the use of hardware resources will be higher.
        required: true
        type: int
    memory_level:
        description:
            - Enter the level of memory used for the compression. The higher the level, the more hardware resources will be used.
        required: true
        type: int
    window_size:
        description:
            - Enter the size of the window used for compression.
        required: true
        type: int
    state:
        description:
            - State of the compression.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: Have compression

  r_s.waf.compression:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    name: compression
    buffer_size: 8096
    compression_level: 5
    memory_level: 9
    window_size: 15
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils import r_s_waf
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
    get_value,
)


def req(params):
    body = waf_client.CreateCompressionProfile(
        name=params["name"],
        content_types=get_value(params, "content_types", None),
        exclude_browser=get_value(params, "exclude_browser", None),
        buffer_size=get_value(params, "buffer_size", None),
        compression_level=get_value(params, "compression_level", None),
        memory_level=get_value(params, "memory_level", None),
        window_size=get_value(params, "window_size", None),
    )
    return body


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            content_types=dict(type="str", required=False),
            exclude_browser=dict(type="str", required=False),
            buffer_size=dict(type="int", required=True),
            compression_level=dict(type="int", required=True),
            memory_level=dict(type="int", required=True),
            window_size=dict(type="int", required=True),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    if module.check_mode:
        module.exit_json(change=True, msg="Not supported")

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.get_compression_profiles(name=module.params["name"])
        rp = resp.data[0]
        uid = resp.data[0].uid
    except ApiException as e:
        uid = None
        # module.fail_json(msg="Fail to get compression: %s" % e)
    except IndexError:
        uid = None

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False)

        try:
            resp = client.del_compression_profile(uid=uid)
        except ApiException as e:
            module.fail_json(msg="Fail to delete compression: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete compression %s" % module.params["name"],
        )

    if uid:

        def need_update():
            if rp.name != module.params["name"]:
                return True
            return False

        if need_update():
            try:
                resp = client.update_compression_profile(
                    uid=uid, update_compression_profile=req(module.params)
                )
            except ApiException as e:
                module.fail_json(msg="Fail to update compression: %s" % e)

            module.exit_json(
                changed=True,
                msg="Successfully update compression %s" % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.create_compression_profile(
            create_compression_profile=req(module.params)
        )
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create compression: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create compression %s" % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
