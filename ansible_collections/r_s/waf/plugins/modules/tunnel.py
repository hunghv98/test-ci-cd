#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: tunnel

short_description: Manage tunnel

version_added: "1.0.0"

description:
    - "Manage tunnel."

options:
    name:
        description:
            - Name of the tunnel.
        required: true
        type: str
    labels:
        description:
            - Labels list.
        required: false
        type: list
        elements: str
    reverse_proxy:
        description:
            - Reverse proxy of the tunnel
        required: true
        type: str
    workflow:
        description:
            - The Workflow of the tunnel
        required: true
        type: str
    workflow_parameters:
        description:
            - Array of workflow parameters
        required: false
        type: dict
    network:
        description:
            - Network of the tunnel
        required: true
        type: dict
        suboptions:
            incoming:
                description:
                    - Incoming of the tunnel
                required: true
                type: dict
                suboptions:
                    interface:
                        description:
                            - Object describing of the used interface
                        required: true
                        type: str
                    port:
                        description:
                            - Incoming port
                        required: false
                        type: int
                    server_name:
                        description:
                            - Server name
                        required: false
                        type: str
                    server_alias:
                        description:
                            - Server alias
                        required: false
                        type: list
                        elements: str
                    ssl:
                        description:
                            - The incoming ssl.
                        required: false
                        type: dict
                        suboptions:
                            profile:
                                description:
                                    - The uif of incoming ssl profile.
                                required: false
                                type: str
                            certificate:
                                description:
                                    - The incoming proxy certificate (Server)
                                required: false
                                type: str
                            sni_vhost_check:
                                description:
                                    - Force SNI verification
                                required: false
                                type: bool
                            sslhsts_enable:
                                description:
                                    - Force HTTP Strict transport security
                                required: false
                                type: bool
                            verify_client_certificate:
                                description:
                                    - The presence of verifyClientCertificate activates the incoming SSl verify client certificates.
                                required: false
                                type: dict
                                suboptions:
                                    ssl_redirect_enable:
                                        description:
                                            - The HTTP redirect on a https
                                        required: false
                                        type: bool
                                    ssl_redirect_port_in:
                                        description:
                                            - The port of clear traffic to be redirected to HTTPS
                                        required: false
                                        type: int
                                    bundle:
                                        description:
                                            - Bundle of the incoming ssl verifyClientCertificate configuration.
                                        required: false
                                        type: dict
                                        suboptions:
                                            uid:
                                                description:
                                                    - Uid of the bundle.
                                                required: false
                                                type: str
                                            name:
                                                description:
                                                    - Name of the bundle.
                                                required: false
                                                type: str
                                    ca:
                                        description:
                                            - CA bundle of the incoming ssl verifyClientCertificate configuration.
                                        required: false
                                        type: dict
                                        suboptions:
                                            uid:
                                                description:
                                                    - Uid of the ca bundle.
                                                required: false
                                                type: str
                                            name:
                                                description:
                                                    - Name of the ca bundle.
                                                required: false
                                                type: str
                                    ocsp:
                                        description:
                                            - OCSP bundle of the incoming ssl verifyClientCertificate configuration.
                                        required: false
                                        type: dict
                                        suboptions:
                                            uid:
                                                description:
                                                    - Uid of the ocsp bundle.
                                                required: false
                                                type: str
                                            name:
                                                description:
                                                    - Name of the ocsp bundle.
                                                required: false
                                                type: str
            outgoing:
                description:
                    -  Outgoing of the tunnel
                required: true
                type: dict
                suboptions:
                    address:
                        description:
                            - Backend IP/Host. Mandatory if loadBalancer is not used.
                        required: false
                        type: str
                    port:
                        description:
                            - Backend port
                        required: false
                        type: int
                    ssl:
                        description:
                            -  The outgoing ssl
                        required: false
                        type: dict
                        suboptions:
                            ajp_enable:
                                description:
                                    - Enable AJP for outgoing connections
                                required: false
                                type: bool
    performance:
        description:
            -  The perfomance of the tunnel
        required: false
        type: dict
        suboptions:
            timeout:
                description:
                    - Timeout (s)
                required: false
                type: int
            proxy_timeout:
                description:
                    - Proxy timeout(s)
                required: false
                default: 60
                type: int
            keep_alive_timeout:
                description:
                    - Keepalive timout (s)
                required: false
                type: int
            ramdisk_cache:
                description:
                    - Object describing the ramdisk cache options
                required: false
                type: dict
                suboptions:
                    profile:
                        description:
                            - Object describing of the used ramdisk cache profile
                        required: true
                        type: str
            request_timeout_profile:
                description:
                    - The uid of request timeout profile.
                required: false
                type: str
            compression_profile:
                description:
                    - The uid of compression profile.
                required: false
                type: str
            workflow_preserve_deflate:
                description:
                    - Enable Forward gzip encoding
                required: false
                default: true
                type: bool
    logs:
        description:
            - The log configuraiton of the tunnel
        required: false
        type: dict
        suboptions:
            access:
                description:
                    - The access logs options
                required: false
                type: dict
                suboptions:
                    database:
                        description:
                            - Enable access log database
                        required: false
                        default: false
                        type: bool
                    file_format_profile:
                        description:
                            - The uid of access log profiles of log files
                        required: false
                        type: str
            filter:
                description:
                    - The uid of log filter options
                required: false
                type: str
            realtime:
                description:
                    - The realtime options
                required: false
                type: dict
                suboptions:
                    syslog_destination_profiles:
                        description:
                            - List of the realtime alerting destinations.
                        required: false
                        type: list
                        elements: dict
                        suboptions:
                            uid:
                                description:
                                    - Uid of the realtime alerting destination.
                                required: false
                                type: str
                            name:
                                description:
                                    - Name of the realtime alerting destination.
                                required: false
                                type: str
            debug:
                description:
                    - Enable debug logs.
                required: false
                type: bool
    monitor:
        description:
            - The monitor options of the tunnel
        required: false
        type: dict
        suboptions:
            enabled:
                description:
                    - Enable tunnel monitoring
                required: false
                default: true
                type: bool
            backend:
                description:
                    - Backend of monitoring
                required: false
                type: dict
                suboptions:
                    enabled:
                        description:
                            - Enable Backend monitor check
                        required: false
                        default: true
                        type: bool
                    method:
                        description:
                            - Backend monitor method
                        required: false
                        choices: ['head', 'get']
                        default: 'head'
                        type: str
                    url:
                        description:
                            - Backend monitor URL
                        required: false
                        default: "/"
                        type: str
                    http_host:
                        description:
                            - Backend monitor host
                        required: false
                        type: str
                    frequency:
                        description:
                            - Backend monitor frequancy (min)
                        required: false
                        default: 1
                        type: int
                    timeout:
                        description:
                            - Backend monitor timeout (s)
                        required: false
                        default: 2
                        type: int
                    return_code:
                        description:
                            - Backend monitor return code
                        required: false
                        default: "!5**"
                        type: str
    advanced:
        description:
            - Advanced configuration of the tunnel
        required: false
        type: dict
        suboptions:
            workflow_body:
                description:
                    - Enable advanced body fetching
                required: false
                default: false
                type: bool
            workflow_url_decode_body_plus_as_space:
                description:
                    - Enable URL decode + as space.
                required: false
                default: true
                type: bool
            geo_ip_enabled:
                description:
                    - Enable geolocation.
                required: false
                default: false
                type: bool
            limit_request_body:
                description:
                    - Request body size limit
                required: false
                default: 0
                type: int
            priority:
                description:
                    - Tunnel priority
                required: false
                default: 50
                type: int
    filter:
        description:
            - Enable filter
        required: false
        type: dict
        suboptions:
            label:
                description:
                    - Label object to filter
                type: dict
                suboptions:
                    name:
                        description:
                            - Name of the label.
                        required: false
                        type: str
    state:
        description:
            - State of the tunnel.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: Have tunnel

  r_s.waf.tunnel:

    credentials:
      host: 192.168.254.183:3001
      username: superadmin
      password: "Denyall@1"
      verify_ssl: false
    name: "tun-test"
    reverse_proxy: "d1942d0f00548f6cac6a45708cd7d44f"
    workflow: "WAF ICX Default"
    network:
      incoming:
        interface: "753db29c09364e6b25dc3867a092b65e"
        port: 15000
        server_name: "tun-test"
      outgoing:
        address: "test"
        port: 8080
    filter:
      label:
        name: "test"
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils import r_s_waf
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
    get_value,
)


def req(params):
    workflow_parameters = []

    if params["workflow_parameters"] is not None:
        for name, value in params["workflow_parameters"].items():
            workflow_parameters.append(
                waf_client.CreateTunnelWorkflowParametersArray(name=name, value=value)
            )

    ssl_profile = params["network"]["incoming"].get("ssl", None)
    ssl_data = None
    cert_bundle = None
    ca_bundle = None
    ocsp_bundle = None
    client_certificate = None
    client_certificate_data = None

    if ssl_profile is not None:
        client_certificate = get_value(
            params["network"]["incoming"]["ssl"], "verify_client_certificate", None
        )

    if client_certificate is not None:
        bundle = client_certificate.get("bundle", None)

        if bundle is not None:
            bundle_uid = bundle.get("uid", None)
            bundle_name = bundle.get("name", None)

            if bundle_uid is not None:
                cert_bundle = waf_client.CreateApplianceSysctlProfile(uid=bundle_uid)

            if bundle_name is not None:
                cert_bundle = waf_client.CreateApplianceSysctlProfile(name=bundle_name)

        ca = client_certificate.get("ca", None)

        if bundle is not None:
            ca_uid = ca.get("uid", None)
            ca_name = ca.get("name", None)

            if ca_uid is not None:
                ca_bundle = waf_client.CreateApplianceSysctlProfile(uid=ca_uid)

            if ca_name is not None:
                ca_bundle = waf_client.CreateApplianceSysctlProfile(name=ca_name)

        ocsp = client_certificate.get("ocsp", None)

        if ocsp is not None:
            ocsp_uid = ocsp.get("uid", None)
            ocsp_name = ocsp.get("name", None)

            if ocsp_uid is not None:
                ocsp_bundle = waf_client.CreateApplianceSysctlProfile(uid=ocsp_uid)

            if ocsp_name is not None:
                ocsp_bundle = waf_client.CreateApplianceSysctlProfile(name=ocsp_name)

        client_certificate_data = (
            waf_client.CreateTunnelNetworkIncomingSslVerifyClientCertificate(
                ssl_redirect_enable=get_value(
                    client_certificate, "ssl_redirect_enable", False
                ),
                ssl_redirect_port_in=get_value(
                    client_certificate, "ssl_redirect_port_in", 80
                ),
                bundle=cert_bundle,
                ca=ca_bundle,
                ocsp=ocsp_bundle,
            )
        )

    if ssl_profile is not None:
        ssl_data = waf_client.CreateTunnelNetworkIncomingSsl(
            profile=dict(uid=ssl_profile["profile"]),
            certificate=dict(uid=ssl_profile["certificate"]),
            sni_vhost_check=ssl_profile["sni_vhost_check"],
            sslhsts_enable=ssl_profile["sslhsts_enable"],
            verify_client_certificate=client_certificate_data,
        )
    out_ssl = params["network"]["outgoing"].get("ssl", None)
    out_ssl_data = None

    if out_ssl is not None:
        out_ssl_data = waf_client.CreateTunnelNetworkOutgoingSsl(
            ajp_enable=out_ssl["ajp_enable"]
        )
    performance_data = params.get("performance", None)
    perf_data = None

    if performance_data is not None:
        ramdisk_cache = performance_data.get("ramdisk_cache", None)
        ramdisk_cache_data = (
            waf_client.CreateTunnelPerformanceRamdiskCache(
                profile=dict(uid=ramdisk_cache.get("profile", None))
            )
            if ramdisk_cache is not None
            else None
        )

        perf_data = waf_client.CreateTunnelPerformance(
            timeout=performance_data.get("timeout", None),
            proxy_timeout=performance_data.get("proxy_timeout", None),
            keep_alive_timeout=performance_data.get("keep_alive_timeout", None),
            ramdisk_cache=ramdisk_cache_data if ramdisk_cache_data else None,
            request_timeout_profile=dict(
                uid=get_value(performance_data, "request_timeout_profile", None)
            ),
            compression_profile=dict(
                uid=get_value(performance_data, "compression_profile", None)
            ),
            workflow_preserve_deflate=performance_data.get(
                "workflow_preserve_deflate", None
            ),
        )
    logs = params.get("logs", None)
    log_data = None

    if logs is not None:
        realtime = logs.get("realtime")
        realtime_data = None
        destinations = []
        access_data = None
        if logs.get("access", None) is not None:
            file_format_profile = get_value(logs["access"], "file_format_profile", None)
            file_enable = file_format_profile is not None
            access_data = waf_client.CreateTunnelLogsAccess(
                database=get_value(logs["access"], "database", None),
                file_format_profile=file_format_profile,
                file=file_enable,
            )

        if realtime is not None:
            for destination in get_value(realtime, "syslog_destination_profiles", []):
                destination_data = (
                    waf_client.CreateTunnelSyslogDestinationProfilesArray(
                        uid=get_value(destination, "uid", None),
                        name=get_value(destination, "name", None),
                    )
                )
                destinations.append(destination_data)

            realtime_data = waf_client.CreateTunnelLogsRealtime(
                syslog_destination_profiles=destinations,
                security=get_value(destination, "security", True),
                wam=get_value(destination, "wam", True),
                error=get_value(destination, "error", False),
                access=get_value(destination, "access", True),
            )

        log_data = waf_client.CreateTunnelLogs(
            access=access_data,
            realtime=realtime_data,
            filter=get_value(logs, "filter", None),
            debug=get_value(logs, "debug", False),
        )
    monitor = params.get("monitor")
    monitor_data = None

    if monitor is not None:
        monitor_data = waf_client.CreateTunnelMonitor(
            enabled=params["monitor"]["enabled"],
            backend=waf_client.CreateTunnelMonitorBackend(
                enabled=params["monitor"]["backend"]["enabled"],
                method=params["monitor"]["backend"]["method"],
                url=params["monitor"]["backend"]["url"],
                http_host=params["monitor"]["backend"]["http_host"],
                frequency=params["monitor"]["backend"]["frequency"],
                timeout=params["monitor"]["backend"]["timeout"],
                return_code=params["monitor"]["backend"]["return_code"],
            ),
        )

    advanced = params.get("advanced")
    advanced_data = None

    if advanced is not None:
        advanced_data = waf_client.CreateTunnelAdvanced(
            workflow_body=params["advanced"]["workflow_body"],
            workflow_url_decode_body_plus_as_space=params["advanced"][
                "workflow_url_decode_body_plus_as_space"
            ],
            geo_ip_enabled=params["advanced"]["geo_ip_enabled"],
            limit_request_body=params["advanced"]["limit_request_body"],
            priority=params["advanced"]["priority"],
        )

    body = waf_client.CreateTunnel(
        name=params["name"],
        labels=[{"name": label} for label in params["labels"]],
        workflow=dict(name=params["workflow"]),
        workflow_parameters=workflow_parameters,
        network=waf_client.CreateTunnelNetwork(
            incoming=waf_client.CreateTunnelNetworkIncoming(
                interface=dict(uid=params["network"]["incoming"]["interface"]),
                port=params["network"]["incoming"]["port"],
                server_name=params["network"]["incoming"]["server_name"],
                server_alias=params["network"]["incoming"]["server_alias"],
                ssl=ssl_data,
            ),
            outgoing=waf_client.CreateTunnelNetworkOutgoing(
                address=params["network"]["outgoing"]["address"],
                port=params["network"]["outgoing"]["port"],
                ssl=out_ssl_data,
            ),
        ),
        reverse_proxy=dict(uid=params["reverse_proxy"]),
        performance=perf_data,
        logs=log_data,
        monitor=monitor_data,
        advanced=advanced_data,
    )

    return body


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            labels=r_s_waf.gen_labels_args(),
            reverse_proxy=dict(type="str", required=True),
            workflow=dict(type="str", required=True),
            workflow_parameters=dict(required=False, type="dict"),
            network=dict(
                type="dict",
                required=True,
                options=dict(
                    incoming=dict(
                        type="dict",
                        required=True,
                        options=dict(
                            interface=dict(type="str", required=True),
                            port=dict(type="int"),
                            server_name=dict(type="str"),
                            server_alias=dict(
                                required=False, type="list", default=[], elements="str"
                            ),
                            ssl=dict(
                                type="dict",
                                options=dict(
                                    profile=dict(type="str"),
                                    certificate=dict(type="str"),
                                    sni_vhost_check=dict(type="bool"),
                                    sslhsts_enable=dict(type="bool"),
                                    verify_client_certificate=dict(
                                        type="dict",
                                        options=dict(
                                            ssl_redirect_enable=dict(type="bool"),
                                            ssl_redirect_port_in=dict(type="int"),
                                            bundle=dict(
                                                type="dict",
                                                required=False,
                                                options=dict(
                                                    uid=dict(type="str"),
                                                    name=dict(type="str"),
                                                ),
                                            ),
                                            ca=dict(
                                                type="dict",
                                                required=False,
                                                options=dict(
                                                    uid=dict(type="str"),
                                                    name=dict(type="str"),
                                                ),
                                            ),
                                            ocsp=dict(
                                                type="dict",
                                                required=False,
                                                options=dict(
                                                    uid=dict(type="str"),
                                                    name=dict(type="str"),
                                                ),
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                    outgoing=dict(
                        type="dict",
                        required=True,
                        options=dict(
                            address=dict(type="str"),
                            port=(dict(type="int")),
                            ssl=dict(
                                type="dict",
                                options=dict(
                                    ajp_enable=dict(type="bool"),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
            performance=dict(
                type="dict",
                required=False,
                options=dict(
                    timeout=dict(type="int"),
                    proxy_timeout=dict(type="int", default=60),
                    keep_alive_timeout=dict(type="int"),
                    ramdisk_cache=dict(
                        type="dict",
                        required=False,
                        options=dict(
                            profile=dict(type="str", required=True),
                        ),
                    ),
                    request_timeout_profile=dict(type="str"),
                    compression_profile=dict(type="str"),
                    workflow_preserve_deflate=dict(type="bool", default=True),
                ),
            ),
            logs=dict(
                type="dict",
                required=False,
                options=dict(
                    access=dict(
                        type="dict",
                        options=dict(
                            database=dict(type="bool", default=False),
                            file_format_profile=dict(type="str"),
                        ),
                    ),
                    realtime=dict(
                        type="dict",
                        options=dict(
                            syslog_destination_profiles=dict(
                                type="list",
                                elements="dict",
                                options=dict(
                                    uid=dict(type="str"),
                                    name=dict(type="str"),
                                ),
                            ),
                        ),
                    ),
                    filter=dict(type="str"),
                    debug=dict(type="bool"),
                ),
            ),
            monitor=dict(
                type="dict",
                required=False,
                options=dict(
                    enabled=dict(type="bool", default=True),
                    backend=dict(
                        type="dict",
                        options=dict(
                            enabled=dict(type="bool", default=True),
                            method=dict(
                                type="str", choices=["head", "get"], default="head"
                            ),
                            url=dict(type="str", default="/"),
                            http_host=dict(type="str"),
                            frequency=dict(type="int", default=1),
                            timeout=dict(type="int", default=2),
                            return_code=dict(type="str", default="!5**"),
                        ),
                    ),
                ),
            ),
            advanced=dict(
                type="dict",
                required=False,
                options=dict(
                    workflow_body=dict(type="bool", default=False),
                    workflow_url_decode_body_plus_as_space=dict(
                        type="bool", default=True
                    ),
                    geo_ip_enabled=dict(type="bool", default=False),
                    limit_request_body=dict(type="int"),
                    priority=dict(type="int", default=50),
                ),
            ),
            filter=dict(
                type="dict",
                required=False,
                options=dict(
                    label=dict(
                        type="dict", required=False, options=dict(name=dict(type="str"))
                    )
                ),
            ),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(msg="Not supported")

    client = get_client(module.params.get("credentials"))

    try:
        # Filter by label
        if (
            module.params["filter"] is not None
            and module.params["filter"]["label"] is not None
            and module.params["filter"]["label"]["name"]
        ):
            label_name = module.params["filter"]["label"]["name"]
            resp = client.get_tunnels(label_name=label_name)

            module.exit_json(
                changed=False,
                result=[
                    {"uid": tunnel.uid, "name": tunnel.name} for tunnel in resp.data
                ],
                msg="Successfully filter tunnels by label",
            )
        else:
            resp = client.get_tunnels(name=module.params["name"])
            uid = resp.data[0].uid
    except ApiException as e:
        uid = None
    except IndexError:
        uid = None
    else:
        if uid:
            tunnel = resp.data[0]

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False, msg="Tunnel does not exists")

        try:
            resp = client.del_tunnel(uid=uid)
        except ApiException as e:
            module.fail_json(msg="Fail to delete tunnel: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete tunnel %s" % module.params["name"],
            uid=uid,
        )

    if uid:

        def need_update():
            if tunnel.name != module.params["name"]:
                return True
            if tunnel.reverse_proxy.uid != module.params["reverse_proxy"]:
                return True
            if tunnel.workflow.name != module.params["workflow"]:
                return True
            if (
                tunnel.network.incoming.server_alias
                != module.params["network"]["incoming"]["server_alias"]
            ):
                return True
            if (
                tunnel.network.incoming.server_name
                != module.params["network"]["incoming"]["server_name"]
            ):
                return True
            if (
                tunnel.network.outgoing.address
                != module.params["network"]["outgoing"]["address"]
            ):
                return True
            if (
                tunnel.network.outgoing.port
                != module.params["network"]["outgoing"]["port"]
            ):
                return True

            if (
                tunnel.network.incoming.port
                != module.params["network"]["incoming"]["port"]
            ):
                return True

            if (
                tunnel.network.incoming.ssl is not None
                and tunnel.network.incoming.ssl.certificate is not None
            ):
                if module.params["network"]["incoming"].get("ssl", None) is not None:
                    if (
                        tunnel.network.incoming.ssl.certificate.uid
                        != module.params["network"]["incoming"]["ssl"]["certificate"]
                    ):
                        return True
                else:
                    return True
            elif module.params["network"]["incoming"].get("ssl", None) is not None:
                return True

            return False

        if need_update():
            try:
                resp = client.update_tunnel(
                    name=module.params["name"],
                    update_tunnel=req(module.params),
                )
            except ApiException as e:
                module.fail_json(msg="Fail to update tunnel: %s" % e)

            module.exit_json(
                changed=True,
                msg="Successfully update tunnel %s" % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.create_tunnel(create_tunnel=req(module.params))
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create tunnel: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create tunnel %s" % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
