#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: apply

short_description: Apply new configuration

version_added: "1.0.0"

description:
    - "Apply new boxes (appliances), tunnels, reverse proxies configuration."

options:
    tunnels:
        description:
            - List of tunnel uid to apply.
        type: list
        elements: str
    secondary_tunnels:
        description:
            - List of secondary tunnel uid to apply.
        type: list
        elements: str
    reverseproxies:
        description:
            - List of reverse proxy uid to apply.
        type: list
        elements: str
    appliances:
        description:
            - List of appliance uid to apply.
        type: list
        elements: str
    cold_restart:
        description:
            - Perform cold restart on applied reverse proxies.
        default: false
        type: bool

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: Apply reverses and tunnels"
  r_s.waf.apply:
    credentials:
      host: rswaf.local:3001
      password: Denyall@0
      username: superadmin
      verify_ssl: false
    reverseproxies:
      - a24db5e577ea16cd02a0cd84883f9253
      - 3a2ea9cc2d6f805d87616e728535bb0e
    tunnels:
      - a24db5e577ea16cd02a0cd84883f9253
      - 3a2ea9cc2d6f805d87616e728535bb0e
    secondary_tunnels:
      - a24db5e577ea16cd02a0cd84883f9254
      - 3a2ea9cc2d6f805d87616e728535bb0f
    cold_restart: true
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
)


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            tunnels=dict(type="list", elements="str", default=[]),
            secondary_tunnels=dict(type="list", elements="str", default=[]),
            reverseproxies=dict(type="list", elements="str", default=[]),
            appliances=dict(type="list", elements="str", default=[]),
            cold_restart=dict(type="bool", default=False),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=False)

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.apply(
            waf_client.Apply(
                tunnels=[dict(uid=tun) for tun in module.params["tunnels"]],
                secondary_tunnels=[
                    dict(uid=sectun) for sectun in module.params["secondary_tunnels"]
                ],
                reverse_proxies=[
                    dict(uid=rp) for rp in module.params["reverseproxies"]
                ],
                appliances=[dict(uid=app) for app in module.params["appliances"]],
                cold_restart=False,
            )
        )
    except ApiException as e:
        module.fail_json(msg="Fail to apply: %s" % e)

    changed = False
    msg = ""
    for auid, appliance in resp.data[0].items():
        for item in ["reverseProxies", "workflow"]:
            if item not in appliance:
                continue
            for ruid, rp in appliance[item].items():
                for status in rp:
                    if status["status"] not in ["success", "skipped"]:
                        module.fail_json(
                            msg='Fail to apply with status "%s" and error "%s"'
                            % (status["status"], status["message"])
                        )
                    elif status["status"] == "skipped":
                        msg = "Some items has been skipped"
                    elif status["status"] == "success":
                        changed = True

    module.exit_json(changed=changed, msg=msg or "All items has been applied")


def main():
    run_module()


if __name__ == "__main__":
    main()
