#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: get_interface

short_description: Get an interface

version_added: "1.0.0"

description:
    - "Get an interface."

options:
    appliance:
        description:
            - Appliance ID.
        required: true
        type: str
    name:
        description:
            - Name of the interface.
        required: true
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: get eth0
  r_s.waf.get_interface:
    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@1"
      verify_ssl: false
    name: eth0
    appliance: a24db5e577ea16cd02a0cd84883f9253
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
)


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            appliance=dict(type="str", required=True),
            name=dict(type="str", required=True),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=False)

    client = get_client(module.params.get("credentials"))

    try:
        api_response = client.get_network_interfaces(name=module.params["name"])
        uid = api_response.data[0].uid
    except ApiException as e:
        module.fail_json(msg="Fail to get interface: %s" % e)

    module.exit_json(changed=False, uid=uid)


def main():
    run_module()


if __name__ == "__main__":
    main()
