#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: certificate_bundle

short_description: Managed certificate bundles

version_added: "1.0.0"

description:
    - "Manage certificate bundles"

options:
    name:
        description:
            - Name of the certificate bundle.
        required: true
        type: str
    ca:
        description:
            - List of the ca certificate.
        required: false
        type: list
        elements: dict
        suboptions:
            name:
                description:
                    - Name of the ca certificate.
                required: false
                type: str
            bundle_uid:
                description:
                    - Bundle UID.
                required: false
                type: str
            upload:
                description:
                    - The path of the ca certificate file.
                required: false
                type: path
    crl:
        description:
            - List of the crl certificate.
        required: false
        type: list
        elements: dict
        suboptions:
            name:
                description:
                    - Name of the crl certificate.
                required: false
                type: str
            bundle_uid:
                description:
                    - Bundle UID.
                required: false
                type: str
            upload:
                description:
                    - The path of the crl certificate file.
                required: false
                type: path
    ocsp:
        description:
            - List of the OCSP certificate.
        required: false
        type: list
        elements: dict
        suboptions:
            name:
                description:
                    - Name of the ocsp certificate.
                required: false
                type: str
            bundle_uid:
                description:
                    - Bundle UID.
                required: false
                type: str
            upload:
                description:
                    - The path of the ocsp certificate file.
                required: false
                type: path
    state:
        description:
            - State of the certificate bundle.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: have "test" certificate bundle
  r_s.waf.certificate_bundle:
    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@1"
      verify_ssl: false
    name: test
    ca:
      - name: "ca1.pem"
        upload: ./files/ca1.crt
      - name: "ca2.pem"
        upload: ./files/ca2.crt
    crl:
      - name: "crl1.pem"
        upload: ./files/cert.pem
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils import r_s_waf
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
    get_value,
)


def req(params):
    body = waf_client.CreateCertificatesBundle(
        name=params["name"],
    )
    return body


def run_module():
    bundle = None
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            ca=dict(
                type="list",
                required=False,
                elements="dict",
                options=dict(
                    name=dict(type="str"),
                    bundle_uid=dict(type="str"),
                    upload=dict(default=None, type="path"),
                ),
            ),
            crl=dict(
                type="list",
                required=False,
                elements="dict",
                options=dict(
                    name=dict(type="str"),
                    bundle_uid=dict(type="str"),
                    upload=dict(default=None, type="path"),
                ),
            ),
            ocsp=dict(
                type="list",
                required=False,
                elements="dict",
                options=dict(
                    name=dict(type="str"),
                    bundle_uid=dict(type="str"),
                    upload=dict(default=None, type="path"),
                ),
            ),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)
    cas = get_value(module.params, "ca", [])
    crls = get_value(module.params, "crl", [])
    ocsps = get_value(module.params, "ocsp", [])

    if module.check_mode:
        for ca in cas:
            upload = ca.get("upload", None)

            if upload is None or ca.get("name", None) is None:
                module.fail_json(msg="name and upload must be set")

            if not os.path.exists(upload):
                module.fail_json(msg="No such file or directory: {}".format(upload))

        for crl in crls:
            upload = crl.get("upload", None)

            if upload is None or crl.get("name", None) is None:
                module.fail_json(msg="name and upload must be set")

            if not os.path.exists(upload):
                module.fail_json(msg="No such file or directory: {}".format(upload))

        for ocsp in ocsps:
            upload = ocsp.get("upload", None)

            if upload is None or ocsp.get("name", None) is None:
                module.fail_json(msg="name and upload must be set")

            if not os.path.exists(upload):
                module.fail_json(msg="No such file or directory: {}".format(upload))

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.get_certificates_bundles(name=module.params["name"])
        bundle = resp.data[0]
        uid = resp.data[0].uid
    except ApiException as e:
        uid = None
        # module.fail_json(msg="Fail to get certificate_bundle: %s" % e)
    except IndexError:
        uid = None

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False)

        try:
            resp = client.del_certificates_bundle(name=module.params["name"])
        except ApiException as e:
            module.fail_json(msg="Fail to delete certificate_bundle: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete certificate_bundle %s" % module.params["name"],
        )

    if uid:

        def need_update():
            return True

        if need_update() and bundle is not None:
            try:
                for ca in bundle.ca:
                    resp = client.del_ca_certificate_bundle(ca.uid)

                for crl in bundle.crl:
                    resp = client.del_crl_certificate_bundle(crl.uid)

                for ocsp in bundle.ca_ocsp:
                    resp = client.del_ocsp_certificate_bundle(ocsp.uid)
            except ApiException as e:
                module.fail_json(msg="Fail to update certificate_bundle: %s" % e)

        else:
            module.exit_json(changed=False, uid=uid)
    else:
        try:
            resp = client.create_certificates_bundle(
                create_certificates_bundle=req(module.params)
            )
            uid = resp.data.uid
        except ApiException as e:
            module.fail_json(msg="Fail to create certificate_bundle: %s" % e)

    try:
        for ca in cas:
            kvargs = {
                "name": ca.get("name"),
                "upload": ca.get("upload", None),
                "bundle_uid": uid,
            }
            client.create_ca_certificate(**kvargs)
    except ApiException as e:
        module.fail_json(msg="Fail to upload ca certificate in bundle: %s" % e)

    try:
        for crl in crls:
            kvargs = {
                "name": crl.get("name"),
                "upload": crl.get("upload", None),
                "bundle_uid": uid,
            }
            client.create_crl_certificate(**kvargs)
    except ApiException as e:
        module.fail_json(msg="Fail to upload ca certificate in bundle: %s" % e)

    try:
        for ocsp in ocsps:
            kvargs = {
                "name": ocsp.get("name"),
                "upload": ocsp.get("upload", None),
                "bundle_uid": uid,
            }
            client.create_ocsp_certificate(**kvargs)
    except ApiException as e:
        module.fail_json(msg="Fail to upload ca certificate in bundle: %s" % e)

    if bundle is not None:
        module.exit_json(
            changed=True,
            msg="Successfully update certificate_bundle %s" % module.params["name"],
            uid=uid,
        )
    else:
        module.exit_json(
            changed=True,
            result="Successfully create certificate bundle %s" % module.params["name"],
            uid=uid,
        )


def main():
    run_module()


if __name__ == "__main__":
    main()
