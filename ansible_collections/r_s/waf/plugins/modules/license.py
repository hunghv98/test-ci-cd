#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: license

short_description: Managed licenses

version_added: "1.0.0"

description:
    - "Manage certificates"

options:
    upload:
        description:
            - The path to the licens file.
        required: false
        type: path
    state:
        description:
            - State of the license.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: have license

  r_s.waf.license:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@1"
      verify_ssl: false
    upload: ./files/QACluster_VP-VMware-56-4d-d0-f2-76-51-b1-5e-1b-b8-d2-6c-df-7c-62-4e-1547199966.lic
"""

RETURN = r"""
"""

import copy
import os

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils import r_s_waf
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
)


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            upload=dict(default=None, type="path"),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    upload = module.params.get("upload", None)

    if module.check_mode:
        if not os.path.exists(upload):
            module.fail_json(msg="No such file or directory: {}".format(upload))

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.get_licenses()
        data = resp.data[0]
    except ApiException as e:
        module.fail_json(msg="Fail to get license: %s" % e)

    kvargs = {"upload": upload}

    try:
        resp = client.upload_license(**kvargs)
    except ApiException as e:
        module.fail_json(msg="Fail to create license: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully upload license",
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
