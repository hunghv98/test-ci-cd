#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r'''
---
module: reverse_proxy_profile

short_description: Managed reverse proxy profiles

version_added: "1.0"

description:
    - "Manage reverse proxy profiles"

options:
    name:
        description:
            - Name of the reverse proxy profiles.
        required: true
        type: str
    start_server:
        description: start_server of the reverse proxy profiles.
        required: false
        default: 1
        type: int
    server_limit:
        description: server_limit of the reverse proxy profiles.
        required: false
        default: 2
        type: int
    max_clients:
        description: max_clients of the reverse proxy profiles.
        required: false
        default: 10
        type: int
    max_spare_threads:
        description: max_spare_threads of the reverse proxy profiles.
        required: false
        default: 1000
        type: int
    min_spare_threads:
        description: min_spare_threads of the reverse proxy profiles.
        required: false
        default: 500
        type: int
    thread_per_child:
        description: thread_per_child of the reverse proxy profiles.
        required: false
        default: 100
        type: int
    max_requests_per_child:
        description: max_requests_per_child of the reverse proxy profiles.
        required: false
        default: 100
        type: int
    limit_request_field_size:
        description: limit_request_field_size of the reverse proxy profiles.
        required: false
        default: 10000000
        type: int
    timeout:
        description: timeout of the reverse proxy profiles.
        required: false
        default: 12400
        type: int
    proxy_timeout:
        description: proxy_timeout of the reverse proxy profiles.
        required: false
        default: 300
        type: int
    keep_alive:
        description: keep_alive of the reverse proxy profiles.
        required: false
        default: true
        type: bool
    keep_alive_timeout:
        description: keep_alive_timeout of the reverse proxy profiles.
        required: false
        default: 60
        type: int
    max_keep_alive_requests:
        description: max_keep_alive_requests of the reverse proxy profiles.
        required: false
        default: 5
        type: int
    ssl_session_cache_size:
        description: ssl_session_cache_size of the reverse proxy profiles.
        required: false
        default: 300
        type: int
    ssl_session_cache_timeout:
        description: ssl_session_cache_timeout of the reverse proxy profiles.
        required: false
        default: 256
        type: int
    state:
        description:
            - State of the reverse proxy profiles.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team
'''

EXAMPLES = r'''
- name: Have reverse proxy profiles

  r_s.waf.reverse_proxy_profile:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    name: test reverse proxy profiles
    start_server: 2
    server_limit: 10
    max_clients: 1000
    max_spare_threads: 500
    min_spare_threads: 100
    thread_per_child: 100
    max_requests_per_child: 10000000
    limit_request_field_size: 12400
    timeout: 300
    proxy_timeout: 60
    keep_alive: true
    keep_alive_timeout: 5
    max_keep_alive_requests: 300
    ssl_session_cache_size: 256
    ssl_session_cache_timeout: 300
'''

RETURN = r'''
'''

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils import r_s_waf
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
    get_value,
)


def req(params):
    body = waf_client.AddReverseProxyProfile(
        name=params["name"],
        start_server=params["start_server"],
        server_limit=params["server_limit"],
        max_clients=params["max_clients"],
        max_spare_threads=params["max_spare_threads"],
        min_spare_threads=params["min_spare_threads"],
        thread_per_child=params["thread_per_child"],
        max_requests_per_child=params["max_requests_per_child"],
        limit_request_field_size=params["limit_request_field_size"],
        timeout=params["timeout"],
        proxy_timeout=params["proxy_timeout"],
        keep_alive=params["keep_alive"],
        keep_alive_timeout=params["keep_alive_timeout"],
        max_keep_alive_requests=params["max_keep_alive_requests"],
        ssl_session_cache_size=params["ssl_session_cache_size"],
        ssl_session_cache_timeout=params["ssl_session_cache_timeout"]
    )
    return body


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            start_server=dict(type="str", default=1),
            server_limit=dict(type="int", default=10),
            max_clients=dict(type="int", default=1000),
            max_spare_threads=dict(type="int", default=500),
            min_spare_threads=dict(type="int", default=100),
            thread_per_child=dict(type="int", default=100),
            max_requests_per_child=dict(type="int", default=10000000),
            limit_request_field_size=dict(type="int", default=12400),
            timeout=dict(type="int", default=300),
            proxy_timeout=dict(type="int", default=60),
            keep_alive=dict(type="bool", default=True),
            keep_alive_timeout=dict(type="int", default=5),
            max_keep_alive_requests=dict(type="int", default=300),
            ssl_session_cache_size=dict(type="int", default=256),
            ssl_session_cache_timeout=dict(type="int", default=300),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    if module.check_mode:
        module.exit_json(change=True, msg="Not supported")

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.get_reverse_proxy_profiles(name=module.params["name"])
        rp = resp.data[0]
        uid = resp.data[0].uid
    except ApiException as e:
        uid = None
        #module.fail_json(msg="Fail to get reverse proxy profile: %s" % e)
    except IndexError:
        uid = None

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False)

        try:
            resp = client.delete_reverse_proxy_profile(uid=uid)
        except ApiException as e:
            module.fail_json(msg="Fail to delete reverse proxy profile: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete reverse proxy profile %s" % module.params["name"],
        )

    if uid:

        def need_update():
            if rp.name != module.params["name"]:
                return True
            if rp.start_server != module.params["start_server"]:
                return True
            return False

        if need_update():
            try:
                resp = client.update_reverse_proxy_profile(uid=uid, update_reverse_proxy_profile=req(module.params))
            except ApiException as e:
                module.fail_json(msg="Fail to update reverse proxy profile: %s" % e)

            module.exit_json(
                changed=True,
                msg="Successfully update reverse proxy profile %s" % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.add_reverse_proxy_profile(add_reverse_proxy_profile=req(module.params))
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create reverse proxy profile: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create reverse proxy profile %s" % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
