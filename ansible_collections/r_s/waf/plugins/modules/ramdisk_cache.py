#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: ramdisk_cache

short_description: Manage Ramdisk cache profiles

description:
    - "Manage Ramdisk cache profiles."

options:
    name:
        description:
            - Name of the Ramdisk cache profile.
        required: true
        type: str
    cache_ignore_cache_control:
        description:
            - Ignore request to not serve cached content to client.
        type: bool
        default: true
    cache_ignore_no_last_mod:
        description:
            - Ignore the fact that a response has no Last Modified header.
        type: bool
        default: true
    cache_ignore_query_string:
        description:
            - Ignore query string when caching.
        type: bool
        default: true
    cache_store_no_store:
        description:
            - Attempt to cache requests or responses that have been marked as no-store.
        type: bool
        default: true
    cache_store_private:
        description:
            - Attempt to cache responses that the server has marked as private.
        type: int
        default: 1
    cache_default_expire:
        description:
            - The default duration to cache a document when no expiry date is specified.
        type: int
        default: 3600
    cache_ignore_headers:
        description:
            - Do not store the given HTTP header(s) in the cache.
        type: str
        default: ""
    cache_ignore_url_session_identifiers:
        description:
            - Ignore defined session identifiers encoded in the URL when caching.
        type: str
        default: ""
    cache_last_modified_factor:
        description:
            - The factor used to compute an expiry date based on the LastModified date.
        type: float
        default: 0.1
    cache_max_expire:
        description:
            - The maximum time in seconds to cache a document.
        type: int
        default: 3600
    cache_dir_length:
        description:
            - The number of characters in subdirectory names.
        type: int
        default: 1
    cache_dir_levels:
        description:
            - The number of levels of subdirectories in the cache.
        type: int
        default: 2
    cache_max_file_size:
        description:
            - The maximum size (in bytes) of a document to be placed in the cache.
        type: int
        default: 500000
    cache_min_file_size:
        description:
            - The minimum size (in bytes) of a document to be placed in the cache.
        type: int
        default: 500
    state:
        description:
            - State of the Ramdisk cache profile.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author: RS team (@rs_team)
"""

EXAMPLES = r"""
- name: have "test" access log profile
  r_s.waf.access_log_profile:
    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    name: test
    cache_ignore_cache_control: false
    cache_ignore_no_last_mod: true
    cache_ignore_query_string: true
    cache_store_no_store: true
    cache_store_private: true
    cache_default_expire: 7200
    cache_ignore_headers: test
    cache_ignore_url_session_identifiers: test url
    cache_last_modified_factor: "0.1"
    cache_max_expire: 7200
    cache_dir_length: 2
    cache_dir_levels: 4
    cache_max_file_size: 600000
    cache_min_file_size: 600
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
)


def req(params):
    body = waf_client.CreateRamdiskCache(
        name=params["name"],
        cache_ignore_cache_control=params["cache_ignore_cache_control"],
        cache_ignore_no_last_mod=params["cache_ignore_no_last_mod"],
        cache_ignore_query_string=params["cache_ignore_query_string"],
        cache_store_no_store=params["cache_store_no_store"],
        cache_store_private=params["cache_store_private"],
        cache_default_expire=params["cache_default_expire"],
        cache_ignore_headers=params["cache_ignore_headers"],
        cache_ignore_url_session_identifiers=params[
            "cache_ignore_url_session_identifiers"
        ],
        cache_last_modified_factor=params["cache_last_modified_factor"],
        cache_max_expire=params["cache_max_expire"],
        cache_dir_length=params["cache_dir_length"],
        cache_dir_levels=params["cache_dir_levels"],
        cache_max_file_size=params["cache_max_file_size"],
        cache_min_file_size=params["cache_min_file_size"],
    )
    return body


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            cache_ignore_cache_control=dict(type="bool", required=False, default=True),
            cache_ignore_no_last_mod=dict(type="bool", required=False, default=True),
            cache_ignore_query_string=dict(type="bool", required=False, default=True),
            cache_store_no_store=dict(type="bool", required=False, default=True),
            cache_store_private=dict(type="int", required=False, default=1),
            cache_default_expire=dict(type="int", required=False, default=3600),
            cache_ignore_headers=dict(type="str", required=False, default=""),
            cache_ignore_url_session_identifiers=dict(
                type="str", required=False, default=""
            ),
            cache_last_modified_factor=dict(type="float", required=False, default=0.1),
            cache_max_expire=dict(type="int", required=False, default=3600),
            cache_dir_length=dict(type="int", required=False, default=1),
            cache_dir_levels=dict(type="int", required=False, default=2),
            cache_max_file_size=dict(type="int", required=False, default=500000),
            cache_min_file_size=dict(type="int", required=False, default=500),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(msg="Not supported")

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.get_ramdisk_cache(name=module.params["name"])
        uid = resp.data[0].uid
    except ApiException as e:
        uid = None
    except IndexError:
        uid = None
    else:
        if uid:
            ramdisk_cache = resp.data[0]

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False, msg="Ramdisk cache profile does not exist")

        try:
            resp = client.del_ramdisk_cache(uid=ramdisk_cache.uid)
        except ApiException as e:
            module.fail_json(msg="Fail to delete Ramdisk cache profile: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete Ramdisk cache profile %s"
            % module.params["name"],
            uid=uid,
        )

    if uid:

        def need_update():
            if ramdisk_cache.name != module.params["name"]:
                return True
            if (
                ramdisk_cache.cache_ignore_cache_control
                != module.params["cache_ignore_cache_control"]
            ):
                return True
            if (
                ramdisk_cache.cache_ignore_no_last_mod
                != module.params["cache_ignore_no_last_mod"]
            ):
                return True
            if (
                ramdisk_cache.cache_ignore_query_string
                != module.params["cache_ignore_query_string"]
            ):
                return True
            if (
                ramdisk_cache.cache_store_no_store
                != module.params["cache_store_no_store"]
            ):
                return True
            if (
                ramdisk_cache.cache_store_private
                != module.params["cache_store_private"]
            ):
                return True
            if (
                ramdisk_cache.cache_default_expire
                != module.params["cache_default_expire"]
            ):
                return True
            if (
                ramdisk_cache.cache_ignore_headers
                != module.params["cache_ignore_headers"]
            ):
                return True
            if (
                ramdisk_cache.cache_ignore_url_session_identifiers
                != module.params["cache_ignore_url_session_identifiers"]
            ):
                return True
            if (
                ramdisk_cache.cache_last_modified_factor
                != module.params["cache_last_modified_factor"]
            ):
                return True
            if ramdisk_cache.cache_max_expire != module.params["cache_max_expire"]:
                return True
            if ramdisk_cache.cache_dir_length != module.params["cache_dir_length"]:
                return True
            if ramdisk_cache.cache_dir_levels != module.params["cache_dir_levels"]:
                return True
            if (
                ramdisk_cache.cache_max_file_size
                != module.params["cache_max_file_size"]
            ):
                return True
            if (
                ramdisk_cache.cache_min_file_size
                != module.params["cache_min_file_size"]
            ):
                return True

            return False

        if need_update():
            try:
                resp = client.patch_ramdisk_cache(
                    uid=ramdisk_cache.uid,
                    patch_ramdisk_cache=req(module.params),
                )
            except ApiException as e:
                module.fail_json(msg="Fail to update Ramdisk cache profile: %s" % e)

            module.exit_json(
                changed=True,
                msg="Successfully update Ramdisk cache profile %s"
                % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.create_ramdisk_cache(create_ramdisk_cache=req(module.params))
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create Ramdisk cache profile: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create Ramdisk cache profile %s" % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
