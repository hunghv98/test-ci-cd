#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: get_appliance

short_description: Get an appliance

version_added: "1.0.0"

description:
    - "Get an appliance."

options:
    name:
        description:
            - Name of the appliance.
        required: true
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: get all appliance
  r_s.waf.get_appliance:
    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@1"
      verify_ssl: false
    name: application1
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
)


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    client = get_client(module.params.get("credentials"))

    result = {"changed": False}
    try:
        resp = client.get_appliances(name=module.params["name"])
        result["name"] = resp.data[0].name
        result["uid"] = resp.data[0].uid
    except ApiException as e:
        module.fail_json(msg="Fail to get appliance: %s" % e)

    module.exit_json(**result)


def main():
    run_module()


if __name__ == "__main__":
    main()
