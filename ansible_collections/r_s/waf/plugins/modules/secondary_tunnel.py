#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: secondary_tunnel

short_description: Managed secondary tunnels

version_added: "1.0.0"

description:
    - "Manage secondary tunnels"

options:
    reverse_proxy:
        description:
            - Uid of the reverse proxy.
        required: true
        type: str
    parent_tunnel:
        description:
            - Uid of the primary tunnel.
        required: true
        type: str
    in_network_interface:
        description:
            - Uid of the network interface.
        required: true
        type: str
    active:
        description:
            - Active secondary tunnel.
        default: true
        type: bool
    state:
        description:
            - State of the secondary tunnel.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: secondary tunnel
  r_s.waf.secondary_tunnel:
    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    reverse_proxy:  7ca63343ed7f97da38340d64152d89e8
    parent_tunnel: 73a0e2f2d46873749cd9f1bc759bc800
    in_network_interface: ee2bc48cd85d38f954cffddf90747a37
    active: true
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils import r_s_waf
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
)


def req(params):
    body = waf_client.CreateSecondaryTunnel(
        reverse_proxy=waf_client.UpdateNetworkInterfaceDevice(
            uid=params["reverse_proxy"]
        ),
        parent_tunnel=waf_client.UpdateNetworkInterfaceDevice(
            uid=params["parent_tunnel"]
        ),
        in_network_interface=waf_client.UpdateNetworkInterfaceDevice(
            uid=params["in_network_interface"]
        ),
        active=params["active"],
    )
    return body


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            reverse_proxy=dict(type="str", required=True),
            parent_tunnel=dict(type="str", required=True),
            in_network_interface=dict(type="str", required=True),
            active=dict(type="bool", required=False, default=True),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    if module.check_mode:
        module.exit_json(change=True, msg="Not supported")

    client = get_client(module.params.get("credentials"))
    uid = None

    try:
        resp = client.get_secondary_tunnels()
        data = resp.data
        for secondary_tunnel in resp.data:
            if (
                (secondary_tunnel.parent_tunnel.uid == module.params["parent_tunnel"])
                and (
                    secondary_tunnel.in_network_interface.uid
                    == module.params["in_network_interface"]
                )
                and (
                    secondary_tunnel.reverse_proxy.uid == module.params["reverse_proxy"]
                )
            ):
                uid = secondary_tunnel.uid

    except ApiException as e:
        uid = None
    except IndexError:
        uid = None

    if uid:
        module.exit_json(
            changed=False,
            result="The secondary tunnel %s has already existed." % uid,
            uid=uid,
        )

    try:
        resp = client.create_secondary_tunnel(
            create_secondary_tunnel=req(module.params)
        )
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create secondary tunnel: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create secondary tunnel %s" % uid,
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
