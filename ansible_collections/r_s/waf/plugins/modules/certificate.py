#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: certificate

short_description: Managed certificates

version_added: "1.0.0"

description:
    - "Manage certificates"

options:
    name:
        description:
            - Name of the certificate.
        required: true
        type: str
    labels:
        description:
            - Labels list.
        required: false
        type: list
        elements: str
    password:
        description:
            - Password of the certificate.
        required: false
        type: str
    type:
        description:
            - Type of the certificate.
        choices: ['pem', 'p12']
        default: 'pem'
        type: str
    pkcs12:
        description:
            - The path to pkcs12 the certificate file.
        required: false
        type: path
    crt:
        description:
            - The path to the certificate file.
        aliases:
            - certificate
        required: false
        type: path
    key:
        description:
            - Key file path of the certificate.
        aliases:
            - private_key
        required: false
        type: path
    chain:
        description:
            - Chain file path of the certificate.
        required: false
        type: path
    filter:
        description:
            - Enable filter
        required: false
        type: dict
        suboptions:
            label:
                description:
                    - Label object to filter
                type: dict
                suboptions:
                    name:
                        description:
                            - Name of the label.
                        required: false
                        type: str
    state:
        description:
            - State of the certificate.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: have "test" certificate
  r_s.waf.certificate:
    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@1"
      verify_ssl: false
    name: test
    type: pem
    certificate: ./files/cert.pem
    private_key: ./files/key-nopass.pem
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils import r_s_waf
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
)


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            labels=r_s_waf.gen_labels_args(),
            password=dict(type="str", no_log=True),
            type=dict(default="pem", choices=["pem", "p12"]),
            pkcs12=dict(default=None, type="path"),
            crt=dict(default=None, type="path", aliases=["certificate"]),
            key=dict(default=None, type="path", aliases=["private_key"]),
            chain=dict(default=None, type="path"),
            filter=dict(
                type="dict",
                required=False,
                options=dict(
                    label=dict(
                        type="dict", required=False, options=dict(name=dict(type="str"))
                    )
                ),
            ),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    client = get_client(module.params.get("credentials"))

    try:
        # Filter by label
        if (
            module.params["filter"] is not None
            and module.params["filter"]["label"] is not None
            and module.params["filter"]["label"]["name"]
        ):
            label_name = module.params["filter"]["label"]["name"]
            resp = client.get_certificates(label_name=label_name)

            module.exit_json(
                changed=False,
                result=[{"uid": cert.uid, "name": cert.name} for cert in resp.data],
                msg="Successfully filter certificate by label",
            )
        else:
            resp = client.get_certificates(name=module.params["name"])
            cert = resp.data[0]
            uid = resp.data[0].uid
    except ApiException as e:
        uid = None
        # module.fail_json(msg="Fail to get certificate: %s" % e)
    except IndexError:
        uid = None

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False)

        try:
            resp = client.delete_certificate(uid=uid)
        except ApiException as e:
            module.fail_json(msg="Fail to delete certificate: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete certificate %s" % module.params["name"],
        )

    name = module.params.get("name", None)
    labels = module.params.get("labels", None)
    patch_certificate = None

    if labels is not None:
        labels_list = []

        for label in labels:
            patch_certificate_labels = waf_client.PatchCertificateLabelsArray(
                name=label
            )
            labels_list.append(patch_certificate_labels)

            patch_certificate = waf_client.PatchCertificate(labels=labels_list)

    password = module.params.get("password", None)
    type = module.params.get("type", None)
    crt = module.params.get("crt", None)
    pkcs12 = module.params.get("pkcs12", None)
    key = module.params.get("key", None)
    chain = module.params.get("chain", None)

    kvargs = {
        "name": name,
        "type": type,
    }

    if crt is not None:
        kvargs.update({"crt": crt})

    if key is not None:
        kvargs.update({"key": key})

    if chain is not None:
        kvargs.update({"chain": chain})

    if pkcs12 is not None:
        kvargs.update({"pkcs12": pkcs12})

    if password is not None:
        kvargs.update({"password": password})

    if uid:

        def need_update():
            if cert.name != module.params["name"]:
                return True
            return False

        if need_update():
            try:
                resp = client.update_certificate(uid=uid, **kvargs)

                # Update lables
                if patch_certificate is not None:
                    client.patch_certificate(
                        uid=uid, patch_certificate=patch_certificate
                    )
            except ApiException as e:
                module.fail_json(msg="Fail to update certificate: %s" % e)

            module.exit_json(
                changed=True,
                msg="Successfully update certificate %s" % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.create_certificate(**kvargs)
        uid = resp.data.uid

        # Update labels
        if patch_certificate is not None:
            client.patch_certificate(uid=uid, patch_certificate=patch_certificate)
    except ApiException as e:
        module.fail_json(msg="Fail to create certificate: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create certificate %s" % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
