#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: ssl_profile

short_description: Managed ssl profiles

version_added: "1.0.0"

description:
    - "Manage ssl profiles"

options:
    name:
        description:
            - A name of this SSL Profile
        required: true
        type: str
    cipher:
        description:
            - A cipher suite
        required: true
        type: str
    protocol:
        description:
            - A protocol list
        required: true
        type: str
    state:
        description:
            - State of the ssl profile.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: Have ssl profile

  r_s.waf.ssl_profile:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    name: test
    cipher: "ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384"
    protocol: "TLSv1.2:TLSv1.1:TLSv1:SSLv3"
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils import r_s_waf
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
    get_value,
)


def req(params):
    body = waf_client.Addsslprofiles(
        name=params["name"], cipher=params["cipher"], protocol=params["protocol"]
    )
    return body


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            cipher=dict(type="str", required=True),
            protocol=dict(type="str", required=True),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    if module.check_mode:
        module.exit_json(change=True, msg="Not supported")

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.get_sslprofiles(name=module.params["name"])
        rp = resp.data[0]
        uid = resp.data[0].uid
    except ApiException as e:
        uid = None
        # module.fail_json(msg="Fail to get ssl profile: %s" % e)
    except IndexError:
        uid = None

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False)

        try:
            resp = client.deletesslprofiles(uid=uid)
        except ApiException as e:
            module.fail_json(msg="Fail to delete ssl profile: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete ssl profile %s" % module.params["name"],
        )

    if uid:

        def need_update():
            if rp.name != module.params["name"]:
                return True
            if rp.cipher != module.params["cipher"]:
                return True
            if rp.protocol != module.params["protocol"]:
                return True
            return False

        if need_update():
            try:
                resp = client.setsslprofiles(uid=uid, setsslprofiles=req(module.params))
            except ApiException as e:
                module.fail_json(msg="Fail to update ssl profile: %s" % e)

            module.exit_json(
                changed=True,
                msg="Successfully update ssl profile %s" % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.addsslprofiles(addsslprofiles=req(module.params))
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create ssl profile: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create ssl profile %s" % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
