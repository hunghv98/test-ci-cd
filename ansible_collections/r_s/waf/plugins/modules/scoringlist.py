#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: scoringlist

short_description: Manage scoringlist configuration

version_added: "1.0.0"

description:
    - "Manage Manage scoringlist configuration."

options:
    name:
        description:
            - Name of the scoringlist configuration.
        required: true
        type: str
    description:
        description:
            - Description of the scoringlist configuration.
        required: false
        type: str
    template:
        description:
            - The template of the scoringlist configuration.
        required: false
        type: dict
        suboptions:
            uid:
                description:
                    - Uid of the template.
                required: true
                type: str
    static_scoringlist:
        description:
            - The static scoringlist.
        required: false
        type: dict
        suboptions:
            uid:
                description:
                    - Uid of the static scoringlist.
                required: true
                type: str
    state:
        description:
            - State of the scoringlist configuration.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: Have test scoringlist configuration

  r_s.waf.scoringlist:

    credentials:
      host: rswaf.local:3001
      password: Denyall@0
      username: superadmin
      verify_ssl: false
    name: test
    description: "scoringlist configuration description"
    template:
      uid: scoringlistDefault
    static_scoringlist:
      uid: staticScoringlistDefault
"""

RETURN = r"""

"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
)


def req(params):
    body = waf_client.CreateScoringlist(
        name=params["name"],
        description=params["description"],
        template=params["template"],
        static_scoringlist=params["static_scoringlist"],
    )
    return body


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            description=dict(type="str", required=False),
            template=dict(
                type="dict",
                options=dict(uid=dict(type="str", required=True)),
            ),
            static_scoringlist=dict(
                type="dict",
                options=dict(uid=dict(type="str", required=True)),
            ),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(msg="Not supported")

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.get_scoringlists(name=module.params["name"])
        uid = resp.data[0].uid
    except ApiException as e:
        uid = None
    except IndexError:
        uid = None
    else:
        if uid:
            scoringlist = resp.data[0]

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False, msg="scoringlist does not exist")

        try:
            resp = client.del_scoringlist(uid=uid)
        except ApiException as e:
            module.fail_json(msg="Fail to delete scoringlist: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete scoringlist %s" % module.params["name"],
            uid=uid,
        )

    if uid:

        def need_update():
            if scoringlist.description != module.params["description"]:
                return True
            return False

        if need_update():
            try:
                patch_scoringlist = waf_client.PatchScoringlist(
                    description=module.params["description"]
                )
                resp = client.patch_scoringlist(
                    uid=uid, patch_scoringlist=patch_scoringlist
                )
            except ApiException as e:
                module.fail_json(msg="Fail to update scoringlist: %s" % e)

            module.exit_json(
                changed=True,
                msg="Successfully update scoringlist %s" % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.create_scoringlist(create_scoringlist=req(module.params))
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create scoringlist: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create scoringlist %s" % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
