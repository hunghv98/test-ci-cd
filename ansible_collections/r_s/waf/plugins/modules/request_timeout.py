#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: request_timeout

short_description: Managed request timeouts

version_added: "1.0.0"

description:
    - "Manage request timeouts"

options:
    name:
        description:
            - Name of the request timeout.
        required: true
        type: str
    header_timeout:
        description:
            - Header timeout of the request timeout.
        required: false
        type: str
    body_timeout:
        description:
            - Body timeout of the request timeout.
        required: false
        type: str
    header_rate:
        description:
            - Header rate of the request timeout.
        required: false
        type: int
    body_rate:
        description:
            - Body rate of the request timeout.
        required: false
        type: int
    state:
        description:
            - State of the request timeout.
        choices: ['present', 'absent']
        default: 'present'
        type: str

extends_documentation_fragment:
- r_s.waf.waf_api_options

author:
    - RS team (@rs_team)
"""

EXAMPLES = r"""
- name: Have request timeout

  r_s.waf.request_timeout:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    name: req_timeout
    header_timeout: "20-50"
"""

RETURN = r"""
"""

import copy

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.r_s.waf.plugins.module_utils import r_s_waf
from ansible_collections.r_s.waf.plugins.module_utils.r_s_waf import (
    ApiException,
    base_args,
    get_client,
    waf_client,
    get_value,
)


def req(params):
    body = waf_client.CreateRequesttimeoutprofile(
        name=params["name"],
        header_timeout=get_value(params, "header_timeout", None),
        body_timeout=get_value(params, "body_timeout", None),
        header_rate=get_value(params, "header_rate", None),
        body_rate=get_value(params, "body_rate", None),
    )
    return body


def run_module():
    module_args = copy.deepcopy(base_args)
    module_args.update(
        dict(
            name=dict(type="str", required=True),
            header_timeout=dict(type="str", required=False),
            body_timeout=dict(type="str", required=False),
            header_rate=dict(type="int", required=False),
            body_rate=dict(type="int", required=False),
            state=dict(type="str", default="present", choices=["present", "absent"]),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    if module.check_mode:
        module.exit_json(change=True, msg="Not supported")

    client = get_client(module.params.get("credentials"))

    try:
        resp = client.get_requesttimeoutprofiles(name=module.params["name"])
        rp = resp.data[0]
        uid = resp.data[0].uid
    except ApiException as e:
        uid = None
        # module.fail_json(msg="Fail to get request_timeout: %s" % e)
    except IndexError:
        uid = None

    if module.params["state"] == "absent":
        if not uid:
            # already absent
            module.exit_json(changed=False)

        try:
            resp = client.del_requesttimeoutprofile(uid=uid)
        except ApiException as e:
            module.fail_json(msg="Fail to delete request_timeout: %s" % e)

        module.exit_json(
            changed=True,
            result="Successfully delete request_timeout %s" % module.params["name"],
        )

    if uid:

        def need_update():
            if rp.name != module.params["name"]:
                return True
            return False

        if need_update():
            try:
                resp = client.update_requesttimeoutprofile(
                    uid=uid, update_request_timeout=req(module.params)
                )
            except ApiException as e:
                module.fail_json(msg="Fail to update request_timeout: %s" % e)

            module.exit_json(
                changed=True,
                msg="Successfully update request_timeout %s" % module.params["name"],
                uid=uid,
            )
        else:
            module.exit_json(changed=False, uid=uid)

    try:
        resp = client.create_requesttimeoutprofile(
            create_requesttimeoutprofile=req(module.params)
        )
        uid = resp.data.uid
    except ApiException as e:
        module.fail_json(msg="Fail to create request_timeout: %s" % e)

    module.exit_json(
        changed=True,
        result="Successfully create request_timeout %s" % module.params["name"],
        uid=uid,
    )


def main():
    run_module()


if __name__ == "__main__":
    main()
