# -*- coding: utf-8 -*-

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

HAS_R_S_WAF_API = False
try:
    import r_s_waf_api_client as waf_client
    from r_s_waf_api_client.rest import ApiException

    HAS_R_S_WAF_API = True
except ImportError:
    HAS_R_S_WAF_API = False

__metaclass__ = type

base_args = dict(
    credentials=dict(
        type="dict",
        required=True,
        options=dict(
            host=dict(type="str", required=True),
            username=dict(type="str", required=True),
            password=dict(type="str", required=True, no_log=True),
            verify_ssl=dict(type="bool", default=True),
        ),
    )
)


def gen_labels_args():
    return dict(
        type="list",
        elements="str",
        default=[],
        # options=dict(name=dict(type="str"), uid=dict(type="str"),),
    )


def get_client(cred):
    if not HAS_R_S_WAF_API:
        return None
    configuration = waf_client.Configuration()
    configuration.host = "https://%s/api/v1" % cred.get("host")
    configuration.username = cred.get("username")
    configuration.password = cred.get("password")
    configuration.verify_ssl = cred.get("verify_ssl")

    return waf_client.DefaultApi(waf_client.ApiClient(configuration))


def get_value(object, fields, default=None):
    item = object.get(fields, None)

    if default is not None and item is None:
        item = default

    return item
