> CERTIFICATE    (contrib/ansible/ansible_collections/r_s/waf/plugins/modules/certificate.py)

        Manage certificates

OPTIONS (= is mandatory):

- chain
        Chain file path of the certificate.
        [Default: (null)]
        type: path

- credentials
        Credentials for R&S®Web Application Firewall
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

- crt
        The path to the certificate file.
        [Default: (null)]
        type: path

- key
        Key file path of the certificate.
        [Default: (null)]
        type: path

= name
        Name of the certificate.

        type: str

- password
        Password of the certificate.
        [Default: (null)]
        type: str

- pkcs12
        The path to pkcs12 the certificate file.
        [Default: (null)]
        type: path

- state
        State of the certificate.
        (Choices: present, absent)[Default: present]
        type: str

- type
        Type of the certificate.
        (Choices: pem, p12)[Default: pem]
        type: str


AUTHOR: RS team (@rs_team)

EXAMPLES:

- name: have "test" certificate
  r_s.waf.certificate:
    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@1"
      verify_ssl: false
    name: test
    type: pem
    certificate: ./files/cert.pem
    private_key: ./files/key-nopass.pem

