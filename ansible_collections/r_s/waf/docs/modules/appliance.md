> APPLIANCE    (ansible/ansible_collections/r_s/waf/plugins/modules/appliance.py)

        Manages appliances

OPTIONS (= is mandatory):

= admin_ip
        Appliance IP address.

        type: str

= admin_port
        TCP port to connect to the web controller.

        type: str

- contact
        Information to contact the appliance's administrator.
        [Default: (null)]
        type: str

= credentials
        Credentials for R&S®Web Application Firewall

        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

- location
        The location of the appliance.
        [Default: (null)]
        type: str

= name
        Name of the appliance.

        type: str

- state
        State of the appliance.
        (Choices: present, absent)[Default: present]
        type: str

- sysctl_profile
        The uid of sysctl profile.
        [Default: (null)]
        type: dict


AUTHOR: RS team

EXAMPLES:

- name: Have appliance

  r_s.waf.appliance:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    name: Managed 1
    admin_ip: 192.168.254.192
    admin_port: 3001
    location: /
    contact: admin@rswaf.com
    sysctl_profile: null

