> CERTIFICATE_BUNDLE    (contrib/ansible/ansible_collections/r_s/waf/plugins/modules/certificate_bundle.py)

        Manage certificate bundles

OPTIONS (= is mandatory):

- ca
        List of the ca certificate.
        [Default: (null)]
        type: list

        SUBOPTIONS:

        - name
            Name of the ca certificate.
            [Default: (null)]
            type: str

        - upload
            The path of the ca certificate file.
            [Default: (null)]
            type: path

- credentials
        Credentials for R&S®Web Application Firewall
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

- crl
        List of the crl certificate.
        [Default: (null)]
        type: list

        SUBOPTIONS:

        - name
            Name of the crl certificate.
            [Default: (null)]
            type: str

        - upload
            The path of the crl certificate file.
            [Default: (null)]
            type: path

= name
        Name of the certificate bundle.

        type: str

- ocsp
        List of the OCSP certificate.
        [Default: (null)]
        type: list

        SUBOPTIONS:

        - name
            Name of the ocsp certificate.
            [Default: (null)]
            type: str

        - upload
            The path of the ocsp certificate file.
            [Default: (null)]
            type: path

- state
        State of the certificate bundle.
        (Choices: present, absent)[Default: present]
        type: str


AUTHOR: RS team (@rs_team)

EXAMPLES:

- name: have "test" certificate bundle

  r_s.waf.certificate_bundle:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@1"
      verify_ssl: false
    name: test
      ca:
        - name: "ca1.pem"
          upload: ./files/ca1.crt
        - name: "ca2.pem"
          upload: ./files/ca2.crt
      crl:
        - name: "crl1.pem"
          upload: ./files/cert.pem

