> NORMALIZATION    (ansible_collections/r_s/waf/plugins/modules/normalization.py)

        Manage Manage normalization configuration.

OPTIONS (= is mandatory):

= credentials
        Credentials for R&S®Web Application Firewall

        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

= description
        Description of the normalization configuration.

        type: str

= name
        Name of the normalization configuration.

        type: str

- state
        State of the normalization configuration.
        (Choices: present, absent)[Default: present]
        type: str

- template
        The template of the normalization configuration.
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        - name
            Name of the template.
            [Default: (null)]
            type: str

        - uid
            Uid of the template.
            [Default: (null)]
            type: str


AUTHOR: RS team (@rs_team)

EXAMPLES:

- name: Have test normalization configuration

  r_s.waf.normalization:

    credentials:
      host: rswaf.local:3001
      password: Denyall@0
      username: superadmin
      verify_ssl: false
    name: test
    description: "normalization configuration description"
    template:
      uid: normalizationDefault

