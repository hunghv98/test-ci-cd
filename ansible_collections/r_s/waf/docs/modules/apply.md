> APPLY    (contrib/ansible/ansible_collections/r_s/waf/plugins/modules/apply.py)

        Apply new boxes (appliances), tunnels, reverse proxies
        configuration.

OPTIONS (= is mandatory):

= appliances
        List of appliance uid to apply.

        type: list

- cold_restart
        Perform cold restart on applied reverse proxies.
        [Default: False]
        type: bool

- credentials
        Credentials for R&S®Web Application Firewall
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

= reverseProxies
        List of reverse proxy uid to apply.

        type: list

= tunnel
        List of tunnel uid to apply.

        type: list


AUTHOR: RS team

EXAMPLES:

- name: Apply reverses and tunnels"

      r_s.waf.apply:

        credentials:
          host: rswaf.local:3001
          password: Denyall@0
          username: superadmin
          verify_ssl: false
        reverseproxies:
          - a24db5e577ea16cd02a0cd84883f9253
          - 3a2ea9cc2d6f805d87616e728535bb0e
        tunnels:
          - a24db5e577ea16cd02a0cd84883f9253
          - 3a2ea9cc2d6f805d87616e728535bb0e
        cold_restart: true

