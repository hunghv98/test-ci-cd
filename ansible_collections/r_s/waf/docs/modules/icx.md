> ICX    (/ansible/ansible_collections/r_s/waf/plugins/modules/icx.py)

        Manage Manage icx configuration.

OPTIONS (= is mandatory):

= credentials
        Credentials for R&S®Web Application Firewall

        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

= description
        Description of the ICX configuration.

        type: str

- legacy_resolve
        Indicates how to resolve false positives generated by the
        policy.
        [Default: False]
        type: bool

= name
        Name of the ICX configuration.

        type: str

- state
        State of the ICX configuration.
        (Choices: present, absent)[Default: present]
        type: str

- template
        The template of the ICX configuration.
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        - name
            Name of the template.
            [Default: (null)]
            type: str

        - uid
            Uid of the template.
            [Default: (null)]
            type: str


AUTHOR: RS team (@rs_team)

EXAMPLES:

- name: Have test ICX configuration

  r_s.waf.icx:

    credentials:
      host: rswaf.local:3001
      password: Denyall@0
      username: superadmin
      verify_ssl: false
    name: test
    description: "ICX configuration description"
    template:
      uid: icxtplowa2010
    legacy_resolve: false

