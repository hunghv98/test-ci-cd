> BLACKLIST    (ansible/ansible_collections/r_s/waf/plugins/modules/blacklist.py)

        Manage Manage blacklist configuration.

OPTIONS (= is mandatory):

= credentials
        Credentials for R&S®Web Application Firewall

        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

= description
        Description of the blacklist configuration.

        type: str

= name
        Name of the blacklist configuration.

        type: str

- state
        State of the blacklist configuration.
        (Choices: present, absent)[Default: present]
        type: str

- static_blacklist
        The static blacklist.
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        - name
            Name of the static blacklist.
            [Default: (null)]
            type: str

        - uid
            Uid of the static blacklist.
            [Default: (null)]
            type: str

- template
        The template of the blacklist configuration.
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        - name
            Name of the template.
            [Default: (null)]
            type: str

        - uid
            Uid of the template.
            [Default: (null)]
            type: str


AUTHOR: RS team

EXAMPLES:

- name: Have test blacklist configuration

  r_s.waf.blacklist:

    credentials:
      host: rswaf.local:3001
      password: Denyall@0
      username: superadmin
      verify_ssl: false
    name: test
    description: "blacklist configuration description"
    template:
      uid: blacklistDefault
    static_blacklist:
      uid: staticblacklistDefault

