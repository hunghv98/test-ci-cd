> ALERTING_DESTINATION    (contrib/ansible/ansible_collections/r_s/waf/plugins/modules/alerting_destination.py)

        Manage alerting destination.

OPTIONS (= is mandatory):

- credentials
        Credentials for R&S®Web Application Firewall
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

= name
        Name of the alerting destination.

        type: str

- state
        State of the alerting destination.
        (Choices: present, absent)[Default: present]
        type: str

- syslog_config
        The syslog configuration
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        - facility
            facility of the syslog configuration.
            [Default: True]
            type: int

        - format
            format of the syslog configuration.
            (Choices: legacyV5, rfc5424, rfc3164)[Default: (null)]
            type: str

        = ip
            ip of the syslog configuration.

            type: str

        = port
            port of the syslog configuration.

            type: int

        = protocol
            protocol of the syslog configuration.

            type: str

        - severity
            severity of the syslog configuration.
            [Default: True]
            type: int

- type
        Type of the alerting destination
        (Choices: smtp, syslog, snmp)[Default: smtp]
        type: str


AUTHOR: RS team

EXAMPLES:

- name: Have test syslog destination

  r_s.waf.alerting_destination:

    credentials:
      host: rswaf.local:3001
      password: Denyall@0
      username: superadmin
      verify_ssl: false
    name: test
    type: syslog
    syslog_config:
      ip: 192.168.0.191
      port: 5008
      protocol: tcp
      severity: 5
      facility: 1
      format: rfc5424

