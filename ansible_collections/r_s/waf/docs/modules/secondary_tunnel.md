> SECONDARY_TUNNEL    (ansible/ansible_collections/r_s/waf/plugins/modules/secondary_tunnel.py)

        Manage secondary tunnels

OPTIONS (= is mandatory):

- active
        Active secondary tunnel.
        [Default: True]
        type: bool

= credentials
        Credentials for R&S®Web Application Firewall

        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

= in_network_interface
        Uid of the network interface.

        type: str

= parent_tunnel
        Uid of the primary tunnel.

        type: str

= reverse_proxy
        Uid of the reverse proxy.

        type: str

- state
        State of the secondary tunnel.
        (Choices: present, absent)[Default: present]
        type: str


AUTHOR: RS team (@rs_team)

EXAMPLES:

- name: secondary tunnel

    r_s.waf.secondary_tunnel:

      credentials:
        host: rswaf.local:3001
        username: superadmin
        password: "Denyall@0"
        verify_ssl: false
      reverse_proxy:  7ca63343ed7f97da38340d64152d89e8
      parent_tunnel: 73a0e2f2d46873749cd9f1bc759bc800
      in_network_interface: ee2bc48cd85d38f954cffddf90747a37
      active:true

