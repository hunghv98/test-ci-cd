> REVERSE_PROXY    (contrib/ansible/ansible_collections/r_s/waf/plugins/modules/reverse_proxy.py)

        Manage reverse proxies

OPTIONS (= is mandatory):

- appliance
        Appliance of the reverse proxy.
        [Default: (null)]
        type: str

        SUBOPTIONS:

        - name
            Appliance Name of the reverse proxy.
            [Default: (null)]
            type: str

        - uid
            Appliance ID of the reverse proxy.
            [Default: (null)]
            type: str

- credentials
        Credentials for R&S®Web Application Firewall
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

- labels
        Labels list.
        [Default: (null)]
        type: list

= name
        Name of the reverse proxy.

        type: str

- state
        State of the reverse proxy.
        (Choices: present, absent)[Default: present]
        type: str


AUTHOR: RS team (@rs_team)

EXAMPLES:

- name: Have reverse proxy

  r_s.waf.reverse_proxy:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    appliance:
      name: app1
    name: rp1

