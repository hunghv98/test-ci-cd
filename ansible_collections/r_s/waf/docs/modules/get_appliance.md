> GET_APPLIANCE    (contrib/ansible/ansible_collections/r_s/waf/plugins/modules/get_appliance.py)

        Get an appliance.

OPTIONS (= is mandatory):

- credentials
        Credentials for R&S®Web Application Firewall
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

= name
        Name of the appliance.

        type: str


AUTHOR: RS team (@rs_team)

EXAMPLES:

- name: get all appliance

      r_s.waf.get_appliance:

        credentials:
          host: rswaf.local:3001
          username: superadmin
          password: "Denyall@1"
          verify_ssl: false
        name: application1

