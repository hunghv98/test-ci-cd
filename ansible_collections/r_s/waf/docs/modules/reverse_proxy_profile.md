> REVERSE_PROXY_PROFILE    (ansible/ansible_collections/r_s/waf/plugins/modules/reverse_proxy_profile.py)

        Manage reverse proxy profiles

OPTIONS (= is mandatory):

= credentials
        Credentials for R&S®Web Application Firewall

        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

- keep_alive
        keep_alive of the reverse proxy profiles.
        [Default: True]
        type: bool

- keep_alive_timeout
        keep_alive_timeout of the reverse proxy profiles.
        [Default: 60]
        type: int

- limit_request_field_size
        limit_request_field_size of the reverse proxy profiles.
        [Default: 10000000]
        type: int

- max_clients
        max_clients of the reverse proxy profiles.
        [Default: 10]
        type: int

- max_keep_alive_requests
        max_keep_alive_requests of the reverse proxy profiles.
        [Default: 5]
        type: int

- max_requests_per_child
        max_requests_per_child of the reverse proxy profiles.
        [Default: 100]
        type: int

- max_spare_threads
        max_spare_threads of the reverse proxy profiles.
        [Default: 1000]
        type: int

- min_spare_threads
        min_spare_threads of the reverse proxy profiles.
        [Default: 500]
        type: int

= name
        Name of the reverse proxy profiles.

        type: str

- proxy_timeout
        proxy_timeout of the reverse proxy profiles.
        [Default: 300]
        type: int

- server_limit
        server_limit of the reverse proxy profiles.
        [Default: 2]
        type: int

- ssl_session_cache_size
        ssl_session_cache_size of the reverse proxy profiles.
        [Default: 300]
        type: int

- ssl_session_cache_timeout
        ssl_session_cache_timeout of the reverse proxy profiles.
        [Default: 256]
        type: int

- start_server
        start_server of the reverse proxy profiles.
        [Default: 1]
        type: int

- state
        State of the reverse proxy profiles.
        (Choices: present, absent)[Default: present]
        type: str

- thread_per_child
        thread_per_child of the reverse proxy profiles.
        [Default: 100]
        type: int

- timeout
        timeout of the reverse proxy profiles.
        [Default: 12400]
        type: int


AUTHOR: RS team

EXAMPLES:

- name: Have reverse proxy profiles

  r_s.waf.reverse_proxy_profile:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    name: test reverse proxy profiles
    start_server: 2
    server_limit: 10
    max_clients: 1000
    max_spare_threads: 500
    min_spare_threads: 100
    thread_per_child: 100
    max_requests_per_child: 10000000
    limit_request_field_size: 12400
    timeout: 300
    proxy_timeout: 60
    keep_alive: true
    keep_alive_timeout: 5
    max_keep_alive_requests: 300
    ssl_session_cache_size: 256
    ssl_session_cache_timeout: 300

