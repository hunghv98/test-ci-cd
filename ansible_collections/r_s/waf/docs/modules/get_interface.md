> GET_INTERFACE    (ansible/ansible_collections/r_s/waf/plugins/modules/get_interface.py)

        Get an interface.

OPTIONS (= is mandatory):

= appliance
        Appliance ID.

        type: str

= credentials
        Credentials for R&S®Web Application Firewall

        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

- name
        Name of the interface.
        [Default: (null)]
        type: str


AUTHOR: RS team (@rs_team)

EXAMPLES:

- name: get eth0

      r_s.waf.get_interface:

        credentials:
          host: rswaf.local:3001
          username: superadmin
          password: "Denyall@1"
          verify_ssl: false
        name: eth0
        appliance: a24db5e577ea16cd02a0cd84883f9253

