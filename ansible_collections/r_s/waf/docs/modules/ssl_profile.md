> SSL_PROFILE    (ansible/ansible_collections/r_s/waf/plugins/modules/ssl_profile.py)

        Manage ssl profiles

OPTIONS (= is mandatory):

= cipher
        A cipher suite

        type: str

= credentials
        Credentials for R&S®Web Application Firewall

        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

= name
        A name of this SSL Profile

        type: str

= protocol
        A protocol list

        type: str

- state
        State of the ssl profile.
        (Choices: present, absent)[Default: present]
        type: str


AUTHOR: RS team (@rs_team)

EXAMPLES:

- name: Have ssl profile

  r_s.waf.ssl_profile:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    name: test
    cipher: "ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384"
    protocol: "TLSv1.2:TLSv1.1:TLSv1:SSLv3"

