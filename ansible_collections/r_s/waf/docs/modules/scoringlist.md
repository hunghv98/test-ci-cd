> SCORINGLIST    (ansible_collections/r_s/waf/plugins/modules/scoringlist.py)

        Manage Manage scoringlist configuration.

OPTIONS (= is mandatory):

= credentials
        Credentials for R&S®Web Application Firewall

        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

= description
        Description of the scoringlist configuration.

        type: str

= name
        Name of the scoringlist configuration.

        type: str

- state
        State of the scoringlist configuration.
        (Choices: present, absent)[Default: present]
        type: str

- static_scoringlist
        The static scoringlist.
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        - name
            Name of the static scoringlist.
            [Default: (null)]
            type: str

        - uid
            Uid of the static scoringlist.
            [Default: (null)]
            type: str

- template
        The template of the scoringlist configuration.
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        - name
            Name of the template.
            [Default: (null)]
            type: str

        - uid
            Uid of the template.
            [Default: (null)]
            type: str


AUTHOR: RS team (@rs_team)

EXAMPLES:

- name: Have test scoringlist configuration

  r_s.waf.scoringlist:

    credentials:
      host: rswaf.local:3001
      password: Denyall@0
      username: superadmin
      verify_ssl: false
    name: test
    description: "scoringlist configuration description"
    template:
      uid: scoringlistDefault
    static_scoringlist:
      uid: staticScoringlistDefault

