> ACCESS_LOG_PROFILE    (contrib/ansible/ansible_collections/r_s/waf/plugins/modules/access_log_profile.py)

        Manage access log profiles.

OPTIONS (= is mandatory):

- credentials
        Credentials for R&S®Web Application Firewall
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

- format
        Format of the access log profile.
        [Default: (null)]
        type: str

= name
        Name of the access log profile.

        type: str

- state
        State of the access log profile.
        (Choices: present, absent)[Default: present]
        type: str

- type
        Type of the access log profile
        (Choices: custom, clf, clfvh, ncsa, newncsa, newclfvh,
        newclf)[Default: custom]
        type: str


AUTHOR: RS team (@rs_team)

EXAMPLES:

- name: have "test" access log profile

  r_s.waf.access_log_profile:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    name: test
    type: custom
    format: test_format

