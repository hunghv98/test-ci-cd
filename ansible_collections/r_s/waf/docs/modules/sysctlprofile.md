> SYSCTLPROFILE    (ansible/ansible_collections/r_s/waf/plugins/modules/sysctlprofile.py)

        Manage sysctl profiles

OPTIONS (= is mandatory):

= credentials
        Credentials for R&S®Web Application Firewall

        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

- kernel_shm_max
        kernel_shm_max of the sysctl profile
        [Default: 2048]
        type: int

= name
        Name of the sysctl profile.

        type: str

- netCoreNetdevMaxBacklog
        netCoreNetdevMaxBacklog of sysctlprofile
        [Default: 2500]
        type: int

- netIpv4TcpAbortOnOverflow
        netIpv4TcpAbortOnOverflow of sysctlprofile
        [Default: 0]
        type: int

- net_core_optmem_max
        net_core_optmem_max of sysctlprofile
        [Default: 20480]
        type: int

- net_core_rmem_default
        net_core_rmem_default of sysctlprofile
        [Default: 212992]
        type: int

- net_core_somaxconn
        net_core_somaxconn of sysctlprofile
        [Default: 1024]
        type: int

- net_core_wmem_default
        net_core_wmem_default of sysctlprofile
        [Default: 212992]
        type: int

- net_ipv4_conf_all_rp_filter
        If you need to set several IPs with the same network range on
        several interfaces. You need to set netIpv4ConfAllRpFilter=2.
        We don't recommend because of security issues.
        [Default: 2]
        type: int

- net_ipv4_tcp_abort_on_overflow
        net_ipv4_tcp_abort_on_overflow of sysctlprofile
        [Default: 5]
        type: int

- net_ipv4_tcp_adv_win_scale
        net_ipv4_tcp_adv_win_scale of sysctlprofile
        [Default: 6]
        type: int

- net_ipv4_tcp_congestion_control
        net_ipv4_tcp_congestion_control of sysctlprofile
        (Choices: cubic, westwood)[Default: cubic]
        type: str

- net_ipv4_tcp_dsack
        net_ipv4_tcp_dsack of sysctlprofile
        [Default: 1]
        type: int

- net_ipv4_tcp_ecn
        net_ipv4_tcp_ecn of sysctlprofilen
        [Default: 2]
        type: int

- net_ipv4_tcp_fack
        net_ipv4_tcp_fack of sysctlprofile
        [Default: 1]
        type: int

- net_ipv4_tcp_fin_timeout
        net_ipv4_tcp_fin_timeout of sysctlprofile
        [Default: 15]
        type: int

- net_ipv4_tcp_keepalive_intvl
        net_ipv4_tcp_keepalive_intvl of sysctlprofile
        [Default: 75]
        type: int

- net_ipv4_tcp_keepalive_probes
        net_ipv4_tcp_keepalive_probes of sysctlprofile
        [Default: 9]
        type: int

- net_ipv4_tcp_keepalive_time
        net_ipv4_tcp_keepalive_time of sysctlprofile
        [Default: 1800]
        type: int

- net_ipv4_tcp_max_orphans
        net_ipv4_tcp_max_orphans of sysctlprofile
        [Default: 131072]
        type: int

- net_ipv4_tcp_max_syn_backlog
        net_ipv4_tcp_max_syn_backlog of sysctlprofile
        [Default: 8192]
        type: int

- net_ipv4_tcp_max_tw_buckets
        net_ipv4_tcp_max_tw_buckets of sysctlprofile
        [Default: 131072]
        type: int

- net_ipv4_tcp_moderate_rcvbuf
        net_ipv4_tcp_moderate_rcvbuf of sysctlprofile
        [Default: 1]
        type: int

- net_ipv4_tcp_rfc1337
        net_ipv4_tcp_rfc1337 of sysctlprofile
        [Default: 0]
        type: int

- net_ipv4_tcp_rmem_default
        net_ipv4_tcp_rmem_default of sysctlprofile
        [Default: 87380]
        type: int

- net_ipv4_tcp_rmem_max
        net_ipv4_tcp_rmem_max of sysctlprofile
        [Default: 6291456]
        type: int

- net_ipv4_tcp_rmem_min
        net_ipv4_tcp_rmem_min of sysctlprofile
        [Default: 4096]
        type: int

- net_ipv4_tcp_sack
        net_ipv4_tcp_sack of sysctlprofile
        [Default: 1]
        type: int

- net_ipv4_tcp_syn_ack_retries
        net_ipv4_tcp_syn_ack_retries of sysctlprofile
        [Default: 3]
        type: int

- net_ipv4_tcp_syncookies
        net_ipv4_tcp_syncookies of sysctlprofile
        [Default: 0]
        type: int

- net_ipv4_tcp_timestamps
        net_ipv4_tcp_timestamps of sysctlprofile
        [Default: 1]
        type: int

- net_ipv4_tcp_tw_recycle
        net_ipv4_tcp_tw_recycle of sysctlprofile
        [Default: 0]
        type: int

- net_ipv4_tcp_tw_reuse
        net_ipv4_tcp_tw_reuse of sysctlprofile
        [Default: 0]
        type: int

- net_ipv4_tcp_windows_scaling
        net_ipv4_tcp_windows_scaling of sysctlprofile
        [Default: 1]
        type: int

- net_ipv4_tcp_wmem_default
        net_ipv4_tcp_wmem_default of sysctlprofile
        [Default: 16384]
        type: int

- net_ipv4_tcp_wmem_max
        net_ipv4_tcp_wmem_max of sysctlprofile
        [Default: 4194304]
        type: int

- net_ipv4_tcp_wmem_min
        net_ipv4_tcp_wmem_min of sysctlprofile
        [Default: 4096]
        type: int

- net_ipv4_tcp_workaround_signed_windows
        net_ipv4_tcp_workaround_signed_windows of sysctlprofile
        [Default: 0]
        type: int

- state
        State of the sysctl profile.
        (Choices: present, absent)[Default: present]
        type: str


AUTHOR: RS team

EXAMPLES:

- name: Have sysctl profile

  r_s.waf.sysctlprofile:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@0"
      verify_ssl: false
    name: "test sysctl"
	kernel_shm_max: 2048
	net_ipv4_conf_all_rp_filter: 2
	net_core_optmem_max: 20480
    net_ipv4_tcp_ecn: 2
    net_ipv4_tcp_congestion_control: "cubic"
    net_ipv4_tcp_fack: 1
    net_ipv4_tcp_sack: 1
    net_ipv4_tcp_dsack: 1
    net_ipv4_tcp_timestamps: 1
    net_ipv4_tcp_windows_scaling: 1
    net_ipv4_tcp_adv_win_scale: 1
    net_ipv4_tcp_workaround_signed_windows: 1
    net_ipv4_tcp_syncookies: 1
    net_ipv4_tcp_fin_timeout: 1
    net_ipv4_tcp_keepalive_time: 1
    net_ipv4_tcp_keepalive_intvl: 1
    net_ipv4_tcp_keepalive_probes: 1
    net_ipv4_tcp_tw_reuse: 1
    net_ipv4_tcp_max_tw_buckets: 1

