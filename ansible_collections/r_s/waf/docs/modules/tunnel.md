> TUNNEL    (contrib/ansible/ansible_collections/r_s/waf/plugins/modules/tunnel.py)

        Manage tunnel.

OPTIONS (= is mandatory):

- advanced
        Advanced configuration of the tunnel
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        - geo_ip_enabled
            Enable geolocation.
            [Default: False]
            type: bool

        - limit_request_body
            Request body size limit
            [Default: 0]
            type: int

        - priority
            Tunnel priority
            [Default: 50]
            type: int

        - workflow_body
            Enable advanced body fetching
            [Default: False]
            type: bool

        - workflow_url_decode_body_plus_as_space
            Enable URL decode + as space.
            [Default: True]
            type: bool

- credentials
        Credentials for R&S®Web Application Firewall
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

- logs
        The log configuraiton of the tunnel
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        - access
            The access logs options
            [Default: (null)]
            type: dict

        - database
            Enable access log database
            [Default: False]
            type: bool

        - debug
            Enable debug logs.
            [Default: (null)]
            type: bool

        - file_format_profile
            The uid of access log profiles of log files
            [Default: False]
            type: str

        - filter
            The uid of log filter options
            [Default: False]
            type: str

        - realtime
            The realtime options
            [Default: (null)]
            type: dict

            SUBOPTIONS:

            - syslog_destination_profiles
                List of the realtime alerting destinations.
                [Default: (null)]
                type: list

                SUBOPTIONS:

                - name
                    Name of the realtime alerting destination.
                    [Default: (null)]
                    type: str

                - uid
                    Uid of the realtime alerting destination.
                    [Default: (null)]
                    type: string

- monitor
        The monitor options of the tunnel
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        - backend
            Backend of monitoring
            [Default: True]
            type: dict

            SUBOPTIONS:

            - enabled
                Enable Backend monitor check
                [Default: True]
                type: bool

            - frequency
                Backend monitor frequancy (min)
                [Default: 1]
                type: int

            - http_host
                Backend monitor host
                [Default: (null)]
                type: str

            - method
                Backend monitor method
                (Choices: head, get)[Default: head]
                type: str

            - return_code
                Backend monitor return code
                [Default: !5**]
                type: str

            - timeout
                Backend monitor timeout (s)
                [Default: 2]
                type: int

            - url
                Backend monitor URL
                [Default: /]
                type: str

        - enabled
            Enable tunnel monitoring
            [Default: True]
            type: bool

= name
        Name of the tunnel.

        type: str

= network
        Network of the tunnel

        type: dict

        SUBOPTIONS:

        = incoming
            Incoming of the tunnel

            type: dict

            SUBOPTIONS:

            = interface
                Object describing of the used interface

                type: dict

            - port
                Incoming port
                [Default: (null)]
                type: int

            - server_alias
                Server alias
                [Default: (null)]
                type: list

            - server_name
                Server name
                [Default: (null)]
                type: str

            - ssl
                The incoming ssl.
                [Default: (null)]
                type: dict

                SUBOPTIONS:

                - certificate
                    The incoming proxy certificate (Server)
                    [Default: (null)]
                    type: dict

                - profile
                    The incoming ssl profile.
                    [Default: (null)]
                    type: dict

                - sni_vhost_check
                    Force SNI verification
                    [Default: (null)]
                    type: bool

                - sslhsts_enable
                    Force HTTP Strict transport security
                    [Default: (null)]
                    type: bool

                - verify_client_certificate
                    The incoming ssl verifyClientCertificate
                    configuration. The presence of
                    ssl.incoming.verifyClientCertificate activates the
                    incoming SSl verify client certificates.
                    [Default: (null)]
                    type: dict

                    SUBOPTIONS:

                    - bundle
                        Bundle of the incoming ssl
                        verifyClientCertificate configuration.
                        [Default: (null)]
                        type: dict

                        SUBOPTIONS:

                        - name
                            Name of the bundle.
                            [Default: (null)]
                            type: str

                        - uid
                            Uid of the bundle.
                            [Default: (null)]
                            type: str

                    - ca
                        CA bundle of the incoming ssl
                        verifyClientCertificate configuration.
                        [Default: (null)]
                        type: dict

                        SUBOPTIONS:

                        - name
                            Name of the ca bundle.
                            [Default: (null)]
                            type: str

                        - uid
                            Uid of the ca bundle.
                            [Default: (null)]
                            type: str

                    - ocsp
                        OCSP bundle of the incoming ssl
                        verifyClientCertificate configuration.
                        [Default: (null)]
                        type: dict

                        SUBOPTIONS:

                        - name
                            Name of the ocsp bundle.
                            [Default: (null)]
                            type: str

                        - uid
                            Uid of the ocsp bundle.
                            [Default: (null)]
                            type: str

                    - ssl_redirect_enable
                        The HTTP redirect on a https
                        [Default: (null)]
                        type: bool

                    - ssl_redirect_port_in
                        The port of clear traffic to be redirected to
                        HTTPS
                        [Default: (null)]
                        type: int

        = outgoing
            Outgoing of the tunnel

            type: dict

            SUBOPTIONS:

            - address
                Backend IP/Host. Mandatory if loadBalancer is not
                used.
                [Default: (null)]
                type: str

            - ajp_enable
                Enable AJP for outgoing connections
                [Default: (null)]
                type: bool

            - port
                Backend port
                [Default: (null)]
                type: int

            = ssl
                The outgoing ssl

                type: dict

- performance
        The perfomance of the tunnel
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        - keep_alive_timeout
            Keepalive timout (s)
            [Default: (null)]
            type: int

        - proxy_timeout
            Proxy timeout(s)
            [Default: 60]
            type: int

        - timeout
            Timeout (s)
            [Default: (null)]
            type: int

        - workflow_preserve_deflate
            Enable Forward gzip encoding
            [Default: True]
            type: bool

= reverse_proxy
        Reverse proxy of the tunnel

        type: dict

- state
        State of the tunnel.
        (Choices: present, absent)[Default: present]
        type: str

= workflow
        The Workflow of the tunnel

        type: dict

- workflow_parameters
        Array of workflow parameters
        [Default: (null)]
        type: dict


AUTHOR: RS team (@rs_team)

EXAMPLES:

- Have tunnel

  r_s.waf.tunnel:

    credentials:
      host: 192.168.254.183:3001
      username: superadmin
      password: "Denyall@1"
      verify_ssl: false
    name: "tun-test"
    reverse_proxy: "d1942d0f00548f6cac6a45708cd7d44f"
    workflow: "WAF ICX Default"
    network:
      incoming:
        interface: "753db29c09364e6b25dc3867a092b65e"
        port: 15000
        server_name: "tun-test"
      outgoing:
        address: "test"
        port: 8080

