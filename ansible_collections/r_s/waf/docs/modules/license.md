> LICENSE    (contrib/ansible/ansible_collections/r_s/waf/plugins/modules/license.py)

        Manage certificates

OPTIONS (= is mandatory):

- credentials
        Credentials for R&S®Web Application Firewall
        [Default: (null)]
        type: dict

        SUBOPTIONS:

        = host
            R&S®Web Application Firewall host

            type: str

        = password
            Administrator password

            type: str

        = username
            Administrator user name

            type: str

        - verify_ssl
            Set to false to disable SSL verification
            [Default: True]
            type: bool

- state
        State of the license.
        (Choices: present, absent)[Default: present]
        type: str

= upload
        The path to the licens file.

        type: path


AUTHOR: RS team (@rs_team)

EXAMPLES:

- name: have license

  r_s.waf.license:

    credentials:
      host: rswaf.local:3001
      username: superadmin
      password: "Denyall@1"
      verify_ssl: false
    upload: ./files/QACluster_VP-VMware-56-4d-d0-f2-76-51-b1-5e-1b-b8-d2-6c-df-7c-62-4e-1547199966.lic

