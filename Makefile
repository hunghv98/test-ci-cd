COLLECTION_PATH = ./ansible_collections/r_s/waf
API_PATH = ./build/client

.PHONY: generate-client
generate-client: $(API_PATH)/README.md

$(API_PATH)/README.md: openapi-generator-cli.jar wafapi.yaml generator_config.json
	java -jar openapi-generator-cli.jar generate -i dawaf_openapi3.json -g python -o "$(API_PATH)"  -c generator_config.json
	black "$(API_PATH)"

.PHONY: install-client
install-client:
	pip install $(API_PATH)

openapi-generator-cli.jar:
	wget https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/4.3.1/openapi-generator-cli-4.3.1.jar -O openapi-generator-cli.jar

.PHONY: test
test:
	cd "$(COLLECTION_PATH)" && ansible-test sanity --docker default --python 3.7

.PHONY: build-collection
build-collection:
	rm -f build/r_s-waf-*.tar.gz
	ansible-galaxy collection build --output-path build/ ansible_collections/r_s/waf --force

.PHONY: install-collection
install-collection: build-collection
	ansible-galaxy collection install build/r_s-waf-*.tar.gz --force
