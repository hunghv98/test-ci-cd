# Build it

Create a venv

    python3 -m venv .venv
    source .venv/bin/activate
    pip install wheel

Install ansible

    pip install ansible

Generate and install api client:

    make generate-client install-client

Install ansible collection:

    make install-collection

Then, run an ansible playbook with:

    ansible-playbook examples/multiple_tunnel.yaml # add -v for more details

# Use ansible collection without installing it

Makes ansible use our current directory to look for collection:

    export ANSIBLE_COLLECTIONS_PATHS=$PWD

Install unit test

    cd ansible_collections/r_s/waf
    pip install -r requirements.txt
    pip install -r units.txt

Run unit test
    cd ansible_collections/r_s/waf
    ansible-test units --local --python 3.6 -vvv