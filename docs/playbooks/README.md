#  ansible

Use this playbook to demo how to configure rs waf automatically 

## Actions:

Actions performed by this role


#### Get all appliances:
* list and check all appliance in the list_boxes (Playbook)
#### Upload license to all boxes:
* Upload license to all boxes (DEMO - step 1) (Playbook)
#### Create/update reverse proxies:
* check then create 4 first reverse proxies on 4 boxes (DEMO - step 2) (Playbook)
#### Create/update the second reverse proxies:
* check then create 4 second reverse proxies on 4 boxes (DEMO - step 2) (Playbook)
#### Create/update certificate:
* check then create 4 certificates (DEMO - step 4) (Playbook)
#### Create/update network device:
* check then get all eth0 of 4 boxes (Playbook)
#### Create/update tunnels:
* Create first Tunnels on each RP with default workflow with parameters (DEMO - step 3) (Playbook)
#### Create/update tunnels with workflow with parameters:
* Create second tunnels on each RP with workflow with parameters (DEMO - step 3) (Playbook)
#### Create/update ssl tunnels:
* Add an application (DEMO - step 4a) (Playbook)
#### Update ssl tunnels:
* Update an application SSL certificate with previous tunnel (DEMO - step 4b) (Playbook)
#### Apply configuration:
* apply all changed reverse proxies and tunnels (Playbook)

## Tags:

* `create_tunnel` - Create tunnels

## Variables:

### Playbook:
* `credentials`: `` - credentials to call RS WAF Json API
* `ssl_profile`: `` - SSL profile object
* `list_boxes`: `` - The list of boxes which will be configured automatically by ansible
* `license_files`: `` - List of the license files
## TODO:

#### Improvement:
* Add more params when intergrating other configurations -  (Playbook)

## Author Information
This playbook  was created by: Rs team

Documentation generated using: [Ansible-autodoc](https://github.com/AndresBott/ansible-autodoc)

